﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ExamenASp_Guimaev.Controllers
{
    public class EnseignantController : Controller
    {
        robodbDataContext db = new robodbDataContext();

        // home de l'enseignant
        public ActionResult IndexEnseignant()
        {
            return View();
        }



        public ActionResult readmessage()
        {
            inbox inb = new inbox();

            var msg = (from x in db.inbox where x.Id_enseignant == Convert.ToInt32(Session["Id_enseignant"]) select x).ToList();
            return View(msg);
        }


        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login", "Home");
        }

    }
}