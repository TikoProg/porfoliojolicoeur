﻿using ExamenASp_Guimaev.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExamenASp_Guimaev.Controllers
{
    public class HomeController : Controller
    {
        robodbDataContext db = new robodbDataContext();

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// login
        /// </summary>
        /// <returns></returns> 
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// validation du multi login
        /// </summary>
        /// <param name="e"></param>
        /// <param name="en"></param>
        /// <param name="ad"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(etudiant e, enseignant en)
        {

            etudiantMode em = new etudiantMode();
            if (em.logE(e) == true)
            {
                var etud = (from et in db.etudiant where et.username == e.username && et.password == e.password select et).FirstOrDefault();

                Session["Id_etudiant"] = etud.Id_etudiant;
                Session["nom"] = etud.nom;
                Session["prenom"] = etud.prenom;

                return RedirectToAction("IndexEtudiant", "Etudiant");
            }
            else
            {
                enseignantMode sm = new enseignantMode();

                if (sm.logENS(en) == true)
                {
                    var ens = (from es in db.enseignant where es.username == en.username && es.password == en.password select es).FirstOrDefault();

                    Session["Id_enseignant"] = ens.Id_enseignant;
                    Session["nom"] = ens.nom;
                    Session["prenom"] = ens.prenom;

                    return RedirectToAction("IndexEnseignant", "Enseignant");
                }
                else
                {
                        return View();
                  
                }
            }
        }




    }
}