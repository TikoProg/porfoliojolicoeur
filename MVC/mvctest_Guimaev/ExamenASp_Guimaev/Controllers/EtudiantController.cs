﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ExamenASp_Guimaev.Controllers
{
    public class EtudiantController : Controller
    {
        robodbDataContext db = new robodbDataContext();

        // home de l'etudiant
        public ActionResult IndexEtudiant()
        {
            return View();
        }


        public ActionResult sendmessage()
        {
            return View();
        }


        [HttpPost]
        public ActionResult sendmessage(inbox inb)
        {
            inb.Id_etudiant=Convert.ToInt32(Session["Id_etudiant"]);

            db.inbox.InsertOnSubmit(inb);
            db.SubmitChanges();
            return View();
        }


        public ActionResult readmessage()
        {
            inbox inb = new inbox();

            var msg = (from x in db.inbox where x.Id_etudiant == Convert.ToInt32(Session["Id_etudiant"]) select x).ToList();
            return View(msg);
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login", "Home");
        }
    }
}