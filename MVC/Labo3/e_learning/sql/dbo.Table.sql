﻿CREATE TABLE [dbo].[admin]
(
	[Id_admin] INT NOT NULL PRIMARY KEY IDENTITY, 
    [nom] NVARCHAR(50) NULL, 
    [prenom] NVARCHAR(50) NULL, 
    [username] NVARCHAR(50) NULL, 
    [password] NVARCHAR(50) NULL, 
    [enseignant] BIT NULL 
)
