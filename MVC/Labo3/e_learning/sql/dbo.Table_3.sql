﻿CREATE TABLE [dbo].[inscription]
(
	[numero] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Id_etudiant] NVARCHAR(50) NULL, 
    [Id_admin] NCHAR(10) NULL, 
    CONSTRAINT [FK_inscription_etudiant] FOREIGN KEY ([Id_etudiant]) REFERENCES [etudiant]([Id_etudiant])
)
