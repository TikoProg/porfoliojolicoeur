﻿CREATE TABLE [dbo].[cours]
(
	[Id_cours] INT NOT NULL PRIMARY KEY IDENTITY, 
    [nom] NVARCHAR(50) NULL, 
    [video] NVARCHAR(50) NULL, 
    [pdf] NVARCHAR(50) NULL, 
    [labo] NVARCHAR(50) NULL, 
    [exercice] NVARCHAR(50) NULL, 
    [solutionnaire] NVARCHAR(50) NULL, 
    [quiz] NVARCHAR(50) NULL
)
