﻿using e_learning.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;

namespace e_learning.Controllers
{
    public class HomeController : Controller
    {

        ElearningDbDataContext db = new ElearningDbDataContext();

        // GET: Home
        public ActionResult Index()
        {

            return View();
        }


        /// <summary>
        /// inscription
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(etudiant e)
        {

            etudiantMode gw = new etudiantMode();
            
            if (gw.insertE(e))
            {
                //envoi du mot de passe et username au client
                sendmail(e);

                //recuperation des information du user
                Session["Id_etudiant"] = e.Id_etudiant;
                Session["nom"] = e.nom;
                Session["prenom"] = e.prenom;
                Session["username"] = e.username;
                Session["password"] = e.password;
                Session["email"] = e.email;

                return RedirectToAction("CompleteRegistration", "Etudiant");
            }
            return View();
        }


        /// <summary>
        /// login
        /// </summary>
        /// <returns></returns> 
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// validation du multi login
        /// </summary>
        /// <param name="e"></param>
        /// <param name="en"></param>
        /// <param name="ad"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(etudiant e, enseignant en, admin ad)
        {

            etudiantMode em = new etudiantMode();
            if (em.logE(e) == true)
            {
                var etud = (from et in db.etudiant where et.username == e.username && et.password == e.password select et).FirstOrDefault();

                Session["Id_etudiant"] = etud.Id_etudiant;
                Session["nom"] = etud.nom;
                Session["prenom"] = etud.prenom;

                return RedirectToAction("IndexEtudiant", "Etudiant");
            }
            else
            {
                enseignantMode sm = new enseignantMode();
                if (sm.logENS(en) == true)
                {
                    return RedirectToAction("IndexEnseignant", "Enseignant");
                }
                else
                {
                    adminMode am = new adminMode();
                    if (am.logADM(ad) == true)
                    {
                        return RedirectToAction("IndexAdmin", "Admin");
                    }
                    else
                    {
                        return View();
                    }
                }
            }


        }


        private bool sendmail(etudiant e)
        {
            string subject = "Mot de passe temporaire";
            string message = "Bienvenue parmi nous."+"\n "+"Voici votre nom d'utilisateur et votre mot de passe temporaire."+"\n"+"Username: "+e.username+"\n" +"Password: "+e.password+"\n"+"Vous pouvez toujours les modifier dans vos parametres. N'hésitez pas à nous solliciter si vous avez besoin de quoi que ce soit,on se fera un plaisir de vous apporter notre aide."+"\n"+ "Encore bienvenu!";
            try
            {
                //recuperation du username et du pw qui sont dans le fichier Web.config
                string usn = System.Configuration.ConfigurationManager.AppSettings["senderUser"].ToString();
                string pw = System.Configuration.ConfigurationManager.AppSettings["senderPw"].ToString();

                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(usn, pw);

                //creer un username et un pw pour l'inscription
                // a partir de
                //mettre le username et le pw dans la base de donnee
                MailMessage mailmsg = new MailMessage(usn, e.email, subject, message);
                mailmsg.BodyEncoding = UTF8Encoding.UTF8;
                //mailmsg.IsBodyHtml = true;

                smtp.Send(mailmsg);

                ViewBag.msg = "Un nom d'utilisateur et un mot de passe temporaire vous a ete envoye! ";
                return true;
            }
            catch (Exception ex)
            {
                ViewBag.msg = ex.Message;
                return false;
            }          

        }

    }
}