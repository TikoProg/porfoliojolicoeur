﻿using e_learning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace e_learning.Controllers
{
    public class EtudiantController : Controller
    {
        ElearningDbDataContext db = new ElearningDbDataContext();

        // GET: Etude
        public ActionResult IndexEtudiant()
        {
            return View();
        }

        /// <summary>
        /// changement pour le mot de passe et le username du nouveau etudiant
        /// </summary>
        /// <returns></returns>
        public ActionResult CompleteRegistration()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CompleteRegistration(etudiant e)
        {
            try
            {
                var compare = (from x in db.etudiant where x.username == Session["username"].ToString() && x.password == Session["password"].ToString() select x).FirstOrDefault();
                if (compare!=null)
                {
                    etudiantMode gw = new etudiantMode();

                    if (gw.insertE(e))
                    {
                        return RedirectToAction("IndexEtudiant");
                    }
                }
            }
            catch (Exception)
            {

                ViewBag.msg = "Utilisateur non existant! :-(";
            }

            return View();
        }

        /// <summary>
        /// inscription de l'etudiant a un cours
        /// </summary>
        /// <returns> une confirmation</returns>
        public ActionResult inscription(int reportID)
        {
            demande_inscription di =new demande_inscription();

            di.id_cours = reportID;
            di.id_etudiant = Convert.ToInt32(Session["Id_etudiant"]);
            di.accepter = false;

            demandeMode dm = new demandeMode();
  
            if (dm.demandeInscription(di))
            {
                ViewBag.msg = "Votre demande a été envoyé et sera traitée sous peu!";
            }
            else
            {
                ViewBag.msg = "Vous etes deja enregistre pour ce cours";
            }

            return View();

        }

        [HttpPost]
        public ActionResult inscription()
        {
            return View();

        }

        public ActionResult showdemande(etudiant e)
        {

            
            var listdemande = from dem in db.demande_inscription where dem.id_etudiant==Convert.ToInt32(Session[0]) && dem.accepter==true select dem;

            ViewBag.title = "Inscription";
            ViewBag.message = "LISTE DE VOS COURS";

            return View(listdemande);
        }
          

        public ActionResult showcours()
        {
            
            var listcours = from crs in db.cours select crs;

            ViewBag.title = "Cours diponible";
            ViewBag.message = "LISTE DES COURS DISPONIBLE";

            return View(listcours);
        }

        public ActionResult sendmsg()
        {

            return View();
        }


        [HttpPost]
        public ActionResult sendmsg(inbox e)
        {
            e.Id_etudiant =Convert.ToInt32(Session["Id_etudiant"]);

            try
            {
                    db.inbox.InsertOnSubmit(e);
                    db.SubmitChanges();

                ViewBag.msg = "Merci! Vos commentaires nous sont importants.";
                return View();
            }
            catch (Exception)
            {
                ViewBag.Message = "Vous devez remplir tous les champs!";
                return View();
            }

        }


        public ActionResult readmessage()
        {

            var msg = (from x in db.inbox where x.Id_etudiant == Convert.ToInt32(Session["Id_etudiant"]) select x).ToList();
            return View(msg);
        }

        public ActionResult about()
        {

            return View();
        }


        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login", "Home");
        }
    }
}