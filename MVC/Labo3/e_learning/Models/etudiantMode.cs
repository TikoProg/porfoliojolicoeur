﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_learning.Models
{
    public class etudiantMode
    {
        ElearningDbDataContext db = new ElearningDbDataContext();

        public bool insertE(etudiant e)
        {

            UserPwtempo(e);

            try
            {
                db.etudiant.InsertOnSubmit(e);
                db.SubmitChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        /// <summary>
        /// validation du login etudaiant
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool logE(etudiant e)
        {
            var stat = db.etudiant.Where(x => x.username == e.username && x.password == e.password).FirstOrDefault();
             
            if (stat != null)
            {

                return true;
            }
            else
            {
                return false;
            }

        }

        //methode pour creer les username et pw :-) The best one buy Guimo
        public void UserPwtempo(etudiant e)
        {
            char[] usrtmp = new char[8];
            char[] pswtmp = new char[8];
            Random rand = new Random();

            string code = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            for (int i = 0; i < 8; i++)
            {
                usrtmp[i] = code[rand.Next(62)];
                pswtmp[i] = code[rand.Next(62)];

                e.username += usrtmp[i].ToString();
                e.password += pswtmp[i].ToString();
            }


        }


    }
}