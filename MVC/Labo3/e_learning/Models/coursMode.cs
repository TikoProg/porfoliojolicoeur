﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_learning.Models
{
    public class coursMode
    {
        private ElearningDbDataContext db = new ElearningDbDataContext();

        public bool addcours(cours c)
        {
            try
            {
                db.cours.InsertOnSubmit(c);
                db.SubmitChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool selectcours(cours c)
        {

                var liscour = db.cours.Select(x=>x.Id_cours==c.Id_cours).ToList();
                if (liscour.Count>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
        }

    }
}