﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_learning.Models
{
    public class inboxMode
    {
        private ElearningDbDataContext db = new ElearningDbDataContext();

        public bool sendmessage(inbox ib)
        {
            var listenom = (from ens in db.enseignant
                                orderby ens.nom
                                select new { ID = ens.Id_enseignant, Nom = ens.nom, Prenom =ens.prenom }).ToList();

            ib.Id_enseignant = Convert.ToInt32(listenom[0].ID);

            try
            {
                db.inbox.InsertOnSubmit(ib);
                db.SubmitChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}