﻿namespace System_Banquier_Guima
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Driecteurs = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBoxDirec = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.listBoxDirec = new System.Windows.Forms.ListBox();
            this.txtSalaireD = new System.Windows.Forms.TextBox();
            this.txtPreNomD = new System.Windows.Forms.TextBox();
            this.txtNomD = new System.Windows.Forms.TextBox();
            this.butCreatDirecteurs = new System.Windows.Forms.Button();
            this.Banques = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pictureBoxBank = new System.Windows.Forms.PictureBox();
            this.txtCapital = new System.Windows.Forms.TextBox();
            this.comboBoxDirec = new System.Windows.Forms.ComboBox();
            this.listBoxBank = new System.Windows.Forms.ListBox();
            this.txtAdresseBank = new System.Windows.Forms.TextBox();
            this.txtNomBank = new System.Windows.Forms.TextBox();
            this.butCreatBank = new System.Windows.Forms.Button();
            this.Agences = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.butModifierAgence = new System.Windows.Forms.Button();
            this.comboBoxChoixBank = new System.Windows.Forms.ComboBox();
            this.listBoxAgence = new System.Windows.Forms.ListBox();
            this.txtAdressAgence = new System.Windows.Forms.TextBox();
            this.txtNomAgence = new System.Windows.Forms.TextBox();
            this.butCreatAgence = new System.Windows.Forms.Button();
            this.Employés = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.comboBoxNomAgence = new System.Windows.Forms.ComboBox();
            this.butModifierEmp = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listBoxEmp = new System.Windows.Forms.ListBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.txtSalairEmp = new System.Windows.Forms.TextBox();
            this.txtPrenomEMp = new System.Windows.Forms.TextBox();
            this.txtNomEMp = new System.Windows.Forms.TextBox();
            this.butCreatEmp = new System.Windows.Forms.Button();
            this.Clients = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkBoxNonRé = new System.Windows.Forms.CheckBox();
            this.checkBoxRé = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.butModifierClient = new System.Windows.Forms.Button();
            this.comboBoxEMP = new System.Windows.Forms.ComboBox();
            this.listBoxClient = new System.Windows.Forms.ListBox();
            this.txtAdresseClient = new System.Windows.Forms.TextBox();
            this.txtPrenomClient = new System.Windows.Forms.TextBox();
            this.txtNomClient = new System.Windows.Forms.TextBox();
            this.butCreatClient = new System.Windows.Forms.Button();
            this.Operations = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.listBoxOPÉ = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.txtMontant = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.radioButEpargne = new System.Windows.Forms.RadioButton();
            this.radioButCheque = new System.Windows.Forms.RadioButton();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.butModifierBank = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.Driecteurs.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDirec)).BeginInit();
            this.Banques.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBank)).BeginInit();
            this.Agences.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.Employés.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.Clients.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.Operations.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Driecteurs);
            this.tabControl1.Controls.Add(this.Banques);
            this.tabControl1.Controls.Add(this.Agences);
            this.tabControl1.Controls.Add(this.Employés);
            this.tabControl1.Controls.Add(this.Clients);
            this.tabControl1.Controls.Add(this.Operations);
            this.tabControl1.Location = new System.Drawing.Point(0, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(846, 325);
            this.tabControl1.TabIndex = 6;
            // 
            // Driecteurs
            // 
            this.Driecteurs.Controls.Add(this.groupBox1);
            this.Driecteurs.Location = new System.Drawing.Point(4, 22);
            this.Driecteurs.Name = "Driecteurs";
            this.Driecteurs.Size = new System.Drawing.Size(838, 299);
            this.Driecteurs.TabIndex = 3;
            this.Driecteurs.Text = "Driecteurs";
            this.Driecteurs.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.groupBox1.Controls.Add(this.pictureBoxDirec);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.listBoxDirec);
            this.groupBox1.Controls.Add(this.txtSalaireD);
            this.groupBox1.Controls.Add(this.txtPreNomD);
            this.groupBox1.Controls.Add(this.txtNomD);
            this.groupBox1.Controls.Add(this.butCreatDirecteurs);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(832, 292);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Directeurs";
            // 
            // pictureBoxDirec
            // 
            this.pictureBoxDirec.Location = new System.Drawing.Point(223, 36);
            this.pictureBoxDirec.Name = "pictureBoxDirec";
            this.pictureBoxDirec.Size = new System.Drawing.Size(214, 186);
            this.pictureBoxDirec.TabIndex = 13;
            this.pictureBoxDirec.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Salaire:";
            // 
            // listBoxDirec
            // 
            this.listBoxDirec.FormattingEnabled = true;
            this.listBoxDirec.Location = new System.Drawing.Point(473, 36);
            this.listBoxDirec.Name = "listBoxDirec";
            this.listBoxDirec.Size = new System.Drawing.Size(347, 186);
            this.listBoxDirec.TabIndex = 11;
            // 
            // txtSalaireD
            // 
            this.txtSalaireD.Location = new System.Drawing.Point(54, 120);
            this.txtSalaireD.Name = "txtSalaireD";
            this.txtSalaireD.Size = new System.Drawing.Size(50, 20);
            this.txtSalaireD.TabIndex = 3;
            this.txtSalaireD.Text = "0.00";
            // 
            // txtPreNomD
            // 
            this.txtPreNomD.Location = new System.Drawing.Point(5, 63);
            this.txtPreNomD.Name = "txtPreNomD";
            this.txtPreNomD.Size = new System.Drawing.Size(132, 20);
            this.txtPreNomD.TabIndex = 2;
            this.txtPreNomD.Text = "Prenom";
            // 
            // txtNomD
            // 
            this.txtNomD.Location = new System.Drawing.Point(5, 36);
            this.txtNomD.Name = "txtNomD";
            this.txtNomD.Size = new System.Drawing.Size(132, 20);
            this.txtNomD.TabIndex = 1;
            this.txtNomD.TabStop = false;
            this.txtNomD.Text = "Nom";
            // 
            // butCreatDirecteurs
            // 
            this.butCreatDirecteurs.Location = new System.Drawing.Point(6, 199);
            this.butCreatDirecteurs.Name = "butCreatDirecteurs";
            this.butCreatDirecteurs.Size = new System.Drawing.Size(75, 23);
            this.butCreatDirecteurs.TabIndex = 0;
            this.butCreatDirecteurs.Text = "Enregistrer";
            this.butCreatDirecteurs.UseVisualStyleBackColor = true;
            this.butCreatDirecteurs.Click += new System.EventHandler(this.butCreatDirecteurs_Click);
            // 
            // Banques
            // 
            this.Banques.Controls.Add(this.groupBox3);
            this.Banques.Location = new System.Drawing.Point(4, 22);
            this.Banques.Name = "Banques";
            this.Banques.Padding = new System.Windows.Forms.Padding(3);
            this.Banques.Size = new System.Drawing.Size(838, 299);
            this.Banques.TabIndex = 1;
            this.Banques.Text = "Banques";
            this.Banques.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.DarkCyan;
            this.groupBox3.Controls.Add(this.butModifierBank);
            this.groupBox3.Controls.Add(this.pictureBoxBank);
            this.groupBox3.Controls.Add(this.txtCapital);
            this.groupBox3.Controls.Add(this.comboBoxDirec);
            this.groupBox3.Controls.Add(this.listBoxBank);
            this.groupBox3.Controls.Add(this.txtAdresseBank);
            this.groupBox3.Controls.Add(this.txtNomBank);
            this.groupBox3.Controls.Add(this.butCreatBank);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(832, 295);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Banques";
            // 
            // pictureBoxBank
            // 
            this.pictureBoxBank.Location = new System.Drawing.Point(221, 32);
            this.pictureBoxBank.Name = "pictureBoxBank";
            this.pictureBoxBank.Size = new System.Drawing.Size(215, 186);
            this.pictureBoxBank.TabIndex = 15;
            this.pictureBoxBank.TabStop = false;
            // 
            // txtCapital
            // 
            this.txtCapital.Location = new System.Drawing.Point(6, 85);
            this.txtCapital.Name = "txtCapital";
            this.txtCapital.Size = new System.Drawing.Size(125, 20);
            this.txtCapital.TabIndex = 14;
            this.txtCapital.Text = "Capital";
            // 
            // comboBoxDirec
            // 
            this.comboBoxDirec.FormattingEnabled = true;
            this.comboBoxDirec.Location = new System.Drawing.Point(5, 111);
            this.comboBoxDirec.Name = "comboBoxDirec";
            this.comboBoxDirec.Size = new System.Drawing.Size(126, 21);
            this.comboBoxDirec.TabIndex = 13;
            this.comboBoxDirec.Text = "Directeur";
            // 
            // listBoxBank
            // 
            this.listBoxBank.FormattingEnabled = true;
            this.listBoxBank.Location = new System.Drawing.Point(468, 32);
            this.listBoxBank.Name = "listBoxBank";
            this.listBoxBank.Size = new System.Drawing.Size(351, 186);
            this.listBoxBank.TabIndex = 12;
            // 
            // txtAdresseBank
            // 
            this.txtAdresseBank.Location = new System.Drawing.Point(6, 59);
            this.txtAdresseBank.Name = "txtAdresseBank";
            this.txtAdresseBank.Size = new System.Drawing.Size(187, 20);
            this.txtAdresseBank.TabIndex = 3;
            this.txtAdresseBank.Text = "Adresse";
            // 
            // txtNomBank
            // 
            this.txtNomBank.Location = new System.Drawing.Point(6, 32);
            this.txtNomBank.Name = "txtNomBank";
            this.txtNomBank.Size = new System.Drawing.Size(125, 20);
            this.txtNomBank.TabIndex = 1;
            this.txtNomBank.Text = "Nom";
            // 
            // butCreatBank
            // 
            this.butCreatBank.Location = new System.Drawing.Point(6, 195);
            this.butCreatBank.Name = "butCreatBank";
            this.butCreatBank.Size = new System.Drawing.Size(75, 23);
            this.butCreatBank.TabIndex = 0;
            this.butCreatBank.Text = "Enregistrer";
            this.butCreatBank.UseVisualStyleBackColor = true;
            this.butCreatBank.Click += new System.EventHandler(this.butCreatBank_Click_1);
            // 
            // Agences
            // 
            this.Agences.Controls.Add(this.groupBox2);
            this.Agences.Location = new System.Drawing.Point(4, 22);
            this.Agences.Name = "Agences";
            this.Agences.Padding = new System.Windows.Forms.Padding(3);
            this.Agences.Size = new System.Drawing.Size(838, 299);
            this.Agences.TabIndex = 0;
            this.Agences.Text = "Agences";
            this.Agences.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Teal;
            this.groupBox2.Controls.Add(this.butModifierAgence);
            this.groupBox2.Controls.Add(this.comboBoxChoixBank);
            this.groupBox2.Controls.Add(this.listBoxAgence);
            this.groupBox2.Controls.Add(this.txtAdressAgence);
            this.groupBox2.Controls.Add(this.txtNomAgence);
            this.groupBox2.Controls.Add(this.butCreatAgence);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(832, 295);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Agences";
            // 
            // butModifierAgence
            // 
            this.butModifierAgence.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.butModifierAgence.Location = new System.Drawing.Point(115, 182);
            this.butModifierAgence.Name = "butModifierAgence";
            this.butModifierAgence.Size = new System.Drawing.Size(75, 23);
            this.butModifierAgence.TabIndex = 14;
            this.butModifierAgence.Text = "Modifier";
            this.butModifierAgence.UseVisualStyleBackColor = false;
            this.butModifierAgence.Click += new System.EventHandler(this.butModifierAgence_Click);
            // 
            // comboBoxChoixBank
            // 
            this.comboBoxChoixBank.FormattingEnabled = true;
            this.comboBoxChoixBank.Location = new System.Drawing.Point(7, 86);
            this.comboBoxChoixBank.Name = "comboBoxChoixBank";
            this.comboBoxChoixBank.Size = new System.Drawing.Size(121, 21);
            this.comboBoxChoixBank.TabIndex = 12;
            this.comboBoxChoixBank.Text = "Choisir la banque";
            // 
            // listBoxAgence
            // 
            this.listBoxAgence.FormattingEnabled = true;
            this.listBoxAgence.Location = new System.Drawing.Point(310, 19);
            this.listBoxAgence.Name = "listBoxAgence";
            this.listBoxAgence.Size = new System.Drawing.Size(354, 186);
            this.listBoxAgence.TabIndex = 11;
            // 
            // txtAdressAgence
            // 
            this.txtAdressAgence.Location = new System.Drawing.Point(6, 59);
            this.txtAdressAgence.Name = "txtAdressAgence";
            this.txtAdressAgence.Size = new System.Drawing.Size(184, 20);
            this.txtAdressAgence.TabIndex = 2;
            this.txtAdressAgence.Text = "Adresse";
            // 
            // txtNomAgence
            // 
            this.txtNomAgence.Location = new System.Drawing.Point(6, 32);
            this.txtNomAgence.Name = "txtNomAgence";
            this.txtNomAgence.Size = new System.Drawing.Size(127, 20);
            this.txtNomAgence.TabIndex = 1;
            this.txtNomAgence.Text = "Nom";
            // 
            // butCreatAgence
            // 
            this.butCreatAgence.Location = new System.Drawing.Point(6, 182);
            this.butCreatAgence.Name = "butCreatAgence";
            this.butCreatAgence.Size = new System.Drawing.Size(75, 23);
            this.butCreatAgence.TabIndex = 0;
            this.butCreatAgence.Text = "Enregistrer";
            this.butCreatAgence.UseVisualStyleBackColor = true;
            this.butCreatAgence.Click += new System.EventHandler(this.butCreatAgence_Click_1);
            // 
            // Employés
            // 
            this.Employés.Controls.Add(this.groupBox5);
            this.Employés.Location = new System.Drawing.Point(4, 22);
            this.Employés.Name = "Employés";
            this.Employés.Size = new System.Drawing.Size(838, 299);
            this.Employés.TabIndex = 4;
            this.Employés.Text = "Employés";
            this.Employés.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.groupBox5.Controls.Add(this.comboBoxNomAgence);
            this.groupBox5.Controls.Add(this.butModifierEmp);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.listBoxEmp);
            this.groupBox5.Controls.Add(this.dateTimePicker1);
            this.groupBox5.Controls.Add(this.txtSalairEmp);
            this.groupBox5.Controls.Add(this.txtPrenomEMp);
            this.groupBox5.Controls.Add(this.txtNomEMp);
            this.groupBox5.Controls.Add(this.butCreatEmp);
            this.groupBox5.Location = new System.Drawing.Point(3, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(832, 291);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Employés";
            // 
            // comboBoxNomAgence
            // 
            this.comboBoxNomAgence.FormattingEnabled = true;
            this.comboBoxNomAgence.Location = new System.Drawing.Point(5, 88);
            this.comboBoxNomAgence.Name = "comboBoxNomAgence";
            this.comboBoxNomAgence.Size = new System.Drawing.Size(100, 21);
            this.comboBoxNomAgence.TabIndex = 15;
            this.comboBoxNomAgence.Text = "Agence";
            // 
            // butModifierEmp
            // 
            this.butModifierEmp.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.butModifierEmp.Location = new System.Drawing.Point(107, 198);
            this.butModifierEmp.Name = "butModifierEmp";
            this.butModifierEmp.Size = new System.Drawing.Size(75, 23);
            this.butModifierEmp.TabIndex = 14;
            this.butModifierEmp.Text = "Modifier";
            this.butModifierEmp.UseVisualStyleBackColor = false;
            this.butModifierEmp.Click += new System.EventHandler(this.butModifierEmp_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Salaire:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(129, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Date d\'ambauche:";
            // 
            // listBoxEmp
            // 
            this.listBoxEmp.FormattingEnabled = true;
            this.listBoxEmp.Location = new System.Drawing.Point(309, 35);
            this.listBoxEmp.Name = "listBoxEmp";
            this.listBoxEmp.Size = new System.Drawing.Size(366, 186);
            this.listBoxEmp.TabIndex = 11;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(132, 35);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(146, 20);
            this.dateTimePicker1.TabIndex = 4;
            // 
            // txtSalairEmp
            // 
            this.txtSalairEmp.Location = new System.Drawing.Point(54, 120);
            this.txtSalairEmp.Name = "txtSalairEmp";
            this.txtSalairEmp.Size = new System.Drawing.Size(51, 20);
            this.txtSalairEmp.TabIndex = 3;
            this.txtSalairEmp.Text = "0.00";
            // 
            // txtPrenomEMp
            // 
            this.txtPrenomEMp.Location = new System.Drawing.Point(5, 62);
            this.txtPrenomEMp.Name = "txtPrenomEMp";
            this.txtPrenomEMp.Size = new System.Drawing.Size(100, 20);
            this.txtPrenomEMp.TabIndex = 2;
            this.txtPrenomEMp.Text = "Prenom";
            // 
            // txtNomEMp
            // 
            this.txtNomEMp.Location = new System.Drawing.Point(5, 35);
            this.txtNomEMp.Name = "txtNomEMp";
            this.txtNomEMp.Size = new System.Drawing.Size(100, 20);
            this.txtNomEMp.TabIndex = 1;
            this.txtNomEMp.Text = "Nom";
            // 
            // butCreatEmp
            // 
            this.butCreatEmp.Location = new System.Drawing.Point(5, 198);
            this.butCreatEmp.Name = "butCreatEmp";
            this.butCreatEmp.Size = new System.Drawing.Size(75, 23);
            this.butCreatEmp.TabIndex = 0;
            this.butCreatEmp.Text = "Enregistrer";
            this.butCreatEmp.UseVisualStyleBackColor = true;
            this.butCreatEmp.Click += new System.EventHandler(this.butCreatEmp_Click_1);
            // 
            // Clients
            // 
            this.Clients.Controls.Add(this.groupBox4);
            this.Clients.Location = new System.Drawing.Point(4, 22);
            this.Clients.Name = "Clients";
            this.Clients.Size = new System.Drawing.Size(838, 299);
            this.Clients.TabIndex = 2;
            this.Clients.Text = "Clients";
            this.Clients.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.groupBox4.Controls.Add(this.checkBoxNonRé);
            this.groupBox4.Controls.Add(this.checkBoxRé);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.butModifierClient);
            this.groupBox4.Controls.Add(this.comboBoxEMP);
            this.groupBox4.Controls.Add(this.listBoxClient);
            this.groupBox4.Controls.Add(this.txtAdresseClient);
            this.groupBox4.Controls.Add(this.txtPrenomClient);
            this.groupBox4.Controls.Add(this.txtNomClient);
            this.groupBox4.Controls.Add(this.butCreatClient);
            this.groupBox4.Location = new System.Drawing.Point(3, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(829, 291);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Clients";
            // 
            // checkBoxNonRé
            // 
            this.checkBoxNonRé.AutoSize = true;
            this.checkBoxNonRé.BackColor = System.Drawing.Color.LightSkyBlue;
            this.checkBoxNonRé.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxNonRé.Location = new System.Drawing.Point(230, 60);
            this.checkBoxNonRé.Name = "checkBoxNonRé";
            this.checkBoxNonRé.Size = new System.Drawing.Size(110, 17);
            this.checkBoxNonRé.TabIndex = 16;
            this.checkBoxNonRé.Text = "Non Rémunéré";
            this.checkBoxNonRé.UseVisualStyleBackColor = false;
            // 
            // checkBoxRé
            // 
            this.checkBoxRé.AutoSize = true;
            this.checkBoxRé.BackColor = System.Drawing.Color.LightSkyBlue;
            this.checkBoxRé.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxRé.Location = new System.Drawing.Point(230, 33);
            this.checkBoxRé.Name = "checkBoxRé";
            this.checkBoxRé.Size = new System.Drawing.Size(83, 17);
            this.checkBoxRé.TabIndex = 15;
            this.checkBoxRé.Text = "Rémunéré";
            this.checkBoxRé.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(227, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Types de comptes";
            // 
            // butModifierClient
            // 
            this.butModifierClient.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.butModifierClient.Location = new System.Drawing.Point(126, 193);
            this.butModifierClient.Name = "butModifierClient";
            this.butModifierClient.Size = new System.Drawing.Size(75, 23);
            this.butModifierClient.TabIndex = 13;
            this.butModifierClient.Text = "Modifier";
            this.butModifierClient.UseVisualStyleBackColor = false;
            this.butModifierClient.Click += new System.EventHandler(this.butModifierClient_Click);
            // 
            // comboBoxEMP
            // 
            this.comboBoxEMP.FormattingEnabled = true;
            this.comboBoxEMP.Location = new System.Drawing.Point(6, 110);
            this.comboBoxEMP.Name = "comboBoxEMP";
            this.comboBoxEMP.Size = new System.Drawing.Size(121, 21);
            this.comboBoxEMP.TabIndex = 12;
            this.comboBoxEMP.Text = "ID de l\'employé";
            // 
            // listBoxClient
            // 
            this.listBoxClient.FormattingEnabled = true;
            this.listBoxClient.Location = new System.Drawing.Point(375, 30);
            this.listBoxClient.Name = "listBoxClient";
            this.listBoxClient.Size = new System.Drawing.Size(335, 186);
            this.listBoxClient.TabIndex = 11;
            // 
            // txtAdresseClient
            // 
            this.txtAdresseClient.Location = new System.Drawing.Point(6, 84);
            this.txtAdresseClient.Name = "txtAdresseClient";
            this.txtAdresseClient.Size = new System.Drawing.Size(195, 20);
            this.txtAdresseClient.TabIndex = 4;
            this.txtAdresseClient.Text = "Adresse";
            // 
            // txtPrenomClient
            // 
            this.txtPrenomClient.Location = new System.Drawing.Point(6, 57);
            this.txtPrenomClient.Name = "txtPrenomClient";
            this.txtPrenomClient.Size = new System.Drawing.Size(134, 20);
            this.txtPrenomClient.TabIndex = 2;
            this.txtPrenomClient.Text = "Prenom";
            // 
            // txtNomClient
            // 
            this.txtNomClient.Location = new System.Drawing.Point(6, 30);
            this.txtNomClient.Name = "txtNomClient";
            this.txtNomClient.Size = new System.Drawing.Size(134, 20);
            this.txtNomClient.TabIndex = 1;
            this.txtNomClient.Text = "Nom";
            // 
            // butCreatClient
            // 
            this.butCreatClient.Location = new System.Drawing.Point(6, 193);
            this.butCreatClient.Name = "butCreatClient";
            this.butCreatClient.Size = new System.Drawing.Size(75, 23);
            this.butCreatClient.TabIndex = 0;
            this.butCreatClient.Text = "Enregistrer";
            this.butCreatClient.UseVisualStyleBackColor = true;
            this.butCreatClient.Click += new System.EventHandler(this.butCreatClient_Click_1);
            // 
            // Operations
            // 
            this.Operations.Controls.Add(this.groupBox6);
            this.Operations.Location = new System.Drawing.Point(4, 22);
            this.Operations.Name = "Operations";
            this.Operations.Size = new System.Drawing.Size(838, 299);
            this.Operations.TabIndex = 5;
            this.Operations.Text = "Opérations Banquaires";
            this.Operations.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.groupBox6.Controls.Add(this.button1);
            this.groupBox6.Controls.Add(this.listBoxOPÉ);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Controls.Add(this.txtMontant);
            this.groupBox6.Controls.Add(this.textBox19);
            this.groupBox6.Controls.Add(this.radioButEpargne);
            this.groupBox6.Controls.Add(this.radioButCheque);
            this.groupBox6.Location = new System.Drawing.Point(3, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(835, 292);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Opération Client";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 263);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Effectuer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.butEffectuer_Click);
            // 
            // listBoxOPÉ
            // 
            this.listBoxOPÉ.FormattingEnabled = true;
            this.listBoxOPÉ.Location = new System.Drawing.Point(263, 39);
            this.listBoxOPÉ.Name = "listBoxOPÉ";
            this.listBoxOPÉ.Size = new System.Drawing.Size(355, 186);
            this.listBoxOPÉ.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 212);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Montant:";
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.groupBox7.Controls.Add(this.radioButton3);
            this.groupBox7.Controls.Add(this.radioButton4);
            this.groupBox7.Location = new System.Drawing.Point(12, 82);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(159, 49);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "groupBox7";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.BackColor = System.Drawing.Color.LightSteelBlue;
            this.radioButton3.Location = new System.Drawing.Point(6, 19);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(54, 17);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Dépot";
            this.radioButton3.UseVisualStyleBackColor = false;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.BackColor = System.Drawing.Color.LightSteelBlue;
            this.radioButton4.Location = new System.Drawing.Point(88, 19);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(56, 17);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Retrait";
            this.radioButton4.UseVisualStyleBackColor = false;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // txtMontant
            // 
            this.txtMontant.Location = new System.Drawing.Point(64, 205);
            this.txtMontant.Name = "txtMontant";
            this.txtMontant.Size = new System.Drawing.Size(107, 20);
            this.txtMontant.TabIndex = 7;
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(12, 39);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(159, 20);
            this.textBox19.TabIndex = 6;
            this.textBox19.Text = "Nom";
            // 
            // radioButEpargne
            // 
            this.radioButEpargne.AutoSize = true;
            this.radioButEpargne.BackColor = System.Drawing.Color.OrangeRed;
            this.radioButEpargne.Location = new System.Drawing.Point(91, 148);
            this.radioButEpargne.Name = "radioButEpargne";
            this.radioButEpargne.Size = new System.Drawing.Size(65, 17);
            this.radioButEpargne.TabIndex = 1;
            this.radioButEpargne.TabStop = true;
            this.radioButEpargne.Text = "Épargne";
            this.radioButEpargne.UseVisualStyleBackColor = false;
            this.radioButEpargne.Visible = false;
            // 
            // radioButCheque
            // 
            this.radioButCheque.AutoSize = true;
            this.radioButCheque.BackColor = System.Drawing.Color.Orange;
            this.radioButCheque.Location = new System.Drawing.Point(12, 148);
            this.radioButCheque.Name = "radioButCheque";
            this.radioButCheque.Size = new System.Drawing.Size(62, 17);
            this.radioButCheque.TabIndex = 0;
            this.radioButCheque.TabStop = true;
            this.radioButCheque.Text = "Chèque\r\n";
            this.radioButCheque.UseVisualStyleBackColor = false;
            this.radioButCheque.Visible = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // butModifierBank
            // 
            this.butModifierBank.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.butModifierBank.Location = new System.Drawing.Point(105, 195);
            this.butModifierBank.Name = "butModifierBank";
            this.butModifierBank.Size = new System.Drawing.Size(75, 23);
            this.butModifierBank.TabIndex = 16;
            this.butModifierBank.Text = "Modifier";
            this.butModifierBank.UseVisualStyleBackColor = false;
            this.butModifierBank.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(848, 328);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.Driecteurs.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDirec)).EndInit();
            this.Banques.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBank)).EndInit();
            this.Agences.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.Employés.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.Clients.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.Operations.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Agences;
        private System.Windows.Forms.TabPage Banques;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtAdressAgence;
        private System.Windows.Forms.TextBox txtNomAgence;
        private System.Windows.Forms.Button butCreatAgence;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtAdresseBank;
        private System.Windows.Forms.TextBox txtNomBank;
        private System.Windows.Forms.Button butCreatBank;
        private System.Windows.Forms.TabPage Clients;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtAdresseClient;
        private System.Windows.Forms.TextBox txtPrenomClient;
        private System.Windows.Forms.TextBox txtNomClient;
        private System.Windows.Forms.Button butCreatClient;
        private System.Windows.Forms.TabPage Driecteurs;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtSalaireD;
        private System.Windows.Forms.TextBox txtPreNomD;
        private System.Windows.Forms.TextBox txtNomD;
        private System.Windows.Forms.Button butCreatDirecteurs;
        private System.Windows.Forms.TabPage Employés;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox txtSalairEmp;
        private System.Windows.Forms.TextBox txtPrenomEMp;
        private System.Windows.Forms.TextBox txtNomEMp;
        private System.Windows.Forms.Button butCreatEmp;
        private System.Windows.Forms.TabPage Operations;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.TextBox txtMontant;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.RadioButton radioButEpargne;
        private System.Windows.Forms.RadioButton radioButCheque;
        private System.Windows.Forms.ListBox listBoxOPÉ;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBoxAgence;
        private System.Windows.Forms.ListBox listBoxBank;
        private System.Windows.Forms.ListBox listBoxClient;
        private System.Windows.Forms.ListBox listBoxDirec;
        private System.Windows.Forms.ListBox listBoxEmp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxDirec;
        private System.Windows.Forms.ComboBox comboBoxEMP;
        private System.Windows.Forms.ComboBox comboBoxChoixBank;
        private System.Windows.Forms.TextBox txtCapital;
        private System.Windows.Forms.PictureBox pictureBoxBank;
        private System.Windows.Forms.PictureBox pictureBoxDirec;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button butModifierClient;
        private System.Windows.Forms.Button butModifierAgence;
        private System.Windows.Forms.Button butModifierEmp;
        private System.Windows.Forms.ComboBox comboBoxNomAgence;
        private System.Windows.Forms.CheckBox checkBoxNonRé;
        private System.Windows.Forms.CheckBox checkBoxRé;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button butModifierBank;
    }
}

