﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System_Banquier_Guima
{
   public class Compte
    {
        private float solde;
        private string id;
        public static int cnt;

        public Compte()
        {
            this.solde = 0;
            this.id = cnt.ToString();
            cnt++;
        }

        public Compte(float solde,string id)
        {
            this.solde = solde;
            this.id = id;
        }

        public float SOLDE
        {
            get { return this.solde; }
            set { this.solde = value; }
        }

        public string ID
        {
            get { return this.id; }
            set { this.id = value; }
        }


    }
}
