﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System_Banquier_Guima
{
   public class Employé:Personne
    {
        private string nomAgence;
        private string dateEntrer ;
        private decimal salaire ;

        public Employé() : base()
        {
            this.nomAgence="";
            this.salaire = 0;
            this.dateEntrer="";
            cnt++;
        }   

        public Employé(string id, string nom, string prenom, string nomAgence, decimal salaire, string dateEntrer) : base(id, nom, prenom)
        {
            this.nomAgence = nomAgence;
            this.salaire = salaire;
            this.dateEntrer = dateEntrer;
            
        }

      

        public decimal SALAIRE
        {
            get { return this.salaire; }
            set { this.salaire = value; }
        }

        public string DATE
        {
            get { return this.dateEntrer; }
            set { this.dateEntrer = value; }
        }
        public string NOMAGENCE
        {
            get { return this.nomAgence; }
            set { this.nomAgence = value; }
        }

    }
}
