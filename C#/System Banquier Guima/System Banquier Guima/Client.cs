﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System_Banquier_Guima
{
    public class Client:Personne
    {
        private string IdEmp;
        private string adresse;
        private decimal sold;
        
       

        public Client():base()
        {
            this.adresse = "adresse";
            this.sold = 0;
            cnt++;
           
        }

        public Client(string id,string nom, string prenom, string adresse, string IdEmp) : base(id,nom, prenom)
        {
            this.adresse = adresse;
            this.sold = 0;
            
        }

        public string ADRESSE
        {
            get { return this.adresse; }
            set { this.adresse = value; }
        }
        public string IDEMP
        {
            get { return this.IdEmp; }
            set { this.IdEmp = value; }
        }

        public decimal SOLD
        {
            get { return this.sold; }
            set { this.sold = value; }
        }


        public void AddMoney(decimal x, decimal y)
        {
            decimal total = x + y;
            this.sold = total;
            
        }

        public void TakeMoney(decimal x, decimal y)
        {
            decimal total = x - y;
            if(x==0)
            {
                total += Convert.ToDecimal(0.12) * total;
                this.sold = total;
            }
            
        }





    }
}
