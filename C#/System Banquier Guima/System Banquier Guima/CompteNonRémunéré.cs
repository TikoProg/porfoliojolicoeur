﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System_Banquier_Guima
{
    public class CompteNonRémunéré:Compte
    {
        public CompteNonRémunéré() : base()
        {

        }

        public CompteNonRémunéré(string id, float solde) : base(solde,id)
        {
            
        }
    }
   
}