﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System_Banquier_Guima
{
    class Agence
    {
        private string id = cnt.ToString();
        private string nom;
        private string adresse;
        private string bank;
        public static int cnt;

        public Agence()
        {
            this.id = "id";
            this.nom = "nom";
            this.adresse = "adresse";
            this.bank = "bank";
            cnt++;
        }

        public Agence(string id, string nom, string adresse,string bank)
        {
            this.id = id;
            this.nom = nom;
            this.adresse = adresse;
            this.bank = bank;
            cnt++;
        }
        public string ID
        {
            get { return this.id; }
        }

        public string NOM
        {
            get { return this.nom; }
            set { this.nom = value; }
        }

        public string ADRESSE
        {
            get { return this.adresse; }
            set { this.adresse = value; }
        }

        public string BANK
        {
            get { return this.bank; }
            set { this.bank = value; }
        }

               
    }

}
