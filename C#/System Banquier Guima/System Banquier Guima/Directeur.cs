﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System_Banquier_Guima
{
   public class Directeur:Personne
    {
        private decimal salaire = 0;
        public string photo;
      
        public  Directeur() : base()
        {
            this.salaire = 0;
            this.photo = "";
            cnt++;           
        }

        public Directeur(string id, string nom, string prenom, decimal salaire, string photo) : base(id, nom, prenom)
        {
            this.salaire = salaire;
            this.photo = photo;
            
        }

    
   
        public decimal SALAIRE
        {
            get { return this.salaire; }
            set { this.salaire = value; }
        }

        public string PHOTO
        {
            get { return this.photo; }
            
        }

    }
}
