﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System_Banquier_Guima
{
    public class Personne
    {
       private string id;
       private string nom;
       private string prenom;
       public static int cnt;
       

        public Personne()
        {
            this.id = "ID: " + cnt.ToString();
            this.nom = "nom";
            this.prenom = "prenom";
            cnt++;
        }

        public Personne(string id,string nom, string prenom)
        {
            this.id = id;
            this.nom = nom;
            this.prenom = prenom;
            cnt++;
        }
        public  string ID
        {
            get { return this.id; }
            
        }

        public  string NOM
        {
            get { return this.nom; }
            set { this.nom = value; }
        }
        public  string PRENOM
        {
            get { return this.prenom; }
            set { this.prenom = value; }
        }
      
    }
}