﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System_Banquier_Guima
{
   public class Banque
    {
       private string id;
       private string nom;
       private string adresse;
       private string directeur;
       private double capital;
       public string photo;
       public static int cnt;

        public Banque()
        {
            this.id = cnt.ToString();
            this.nom = "nom";
            this.adresse = "adresse";
            this.directeur = "directeur";
            this.capital = 0.0;
            this.photo = "";
            cnt++;
        }
        public Banque(string id,string nom,string adresse,double capital,string directeur, string photo)
        {
            this.id = id;
            this.nom = nom;
            this.adresse = adresse;
            this.directeur = directeur;
            this.capital = capital;
            this.photo = photo;
            cnt++;
        }

        public string ID
        {
            get { return this.id; }

        }

        public string NOM
        {
            get { return this.nom; }
            set { this.nom = value; }
        }
        public string ADRESSE
        {
            get { return this.adresse; }
            set { this.adresse = value; }
        }
        public string DIRECTEUR
        {
            get { return this.directeur; }
            set { this.directeur = value; }
        }

        public double CAPITAL
        {
            get { return this.capital; }
            set { this.capital = value; }
        }

        public string PHOTO
        {
            get { return this.photo; }

        }

   
    }
}
