﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace System_Banquier_Guima
{
    public partial class Form1 : Form
    {
        private ArrayList ListDirecteur;
        private ArrayList ListBank;
        private ArrayList ListClient;
        private ArrayList ListEmp;
        private ArrayList ListAgence;

        public Form1()
        {
            ListBank = new ArrayList();
            ListDirecteur = new ArrayList();
            ListClient = new ArrayList();
            ListEmp = new ArrayList();
            ListAgence = new ArrayList();

            InitializeComponent();
            
        }
                   
            
        private void butCreatAgence_Click_1(object sender, EventArgs e)
        {
            Agence agn1 = new Agence(Agence.cnt.ToString(), txtNomAgence.Text, txtAdressAgence.Text, comboBoxChoixBank.Text);
            listBoxAgence.Items.Add( agn1.ID + " "+ agn1.NOM + " " + agn1.ADRESSE + " " + agn1.BANK);
            
            //je remplis ma banque
            ListBank.Add(agn1);
            comboBoxNomAgence.Items.Add( agn1.ID +" " + agn1.NOM );


        }

        private void butCreatEmp_Click_1(object sender, EventArgs e)
        {
            Employé emp1 = new Employé(Personne.cnt.ToString(), txtNomEMp.Text, txtPrenomEMp.Text,comboBoxNomAgence.Text, Convert.ToInt16(txtSalairEmp.Text), dateTimePicker1.Text);
            listBoxEmp.Items.Add(" "+ emp1.ID + ": " + emp1.NOM+" " + emp1.PRENOM + " " + emp1.DATE + " " + emp1.SALAIRE + "$");

            ListAgence.Add(emp1);
            //j'affiche les employes 

          
            comboBoxEMP.Items.Add(emp1.ID + " " + emp1.NOM + " " + emp1.PRENOM);

            
        }



        private void butCreatDirecteurs_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();

            //je cree un directeur temporaire
            Directeur dr1 = new Directeur(Personne.cnt.ToString(), txtNomD.Text, txtPreNomD.Text, Convert.ToInt32(txtSalaireD.Text),openFileDialog1.FileName);
            
            listBoxDirec.Items.Add(" " + dr1.ID + " " + dr1.NOM + " " + dr1.PRENOM + " " + dr1.SALAIRE + "$");

            pictureBoxDirec.Image=Image.FromFile(dr1.photo);
            pictureBoxDirec.SizeMode = PictureBoxSizeMode.StretchImage;
           
                    
            //je rempli ma liste de directeur
            ListDirecteur.Add(dr1);

            //j'affiche mes directeurs                    
            comboBoxDirec.Items.Add(" "+dr1.ID+ " " + dr1.NOM + " " +dr1.PRENOM);

        }



        private void butCreatClient_Click_1(object sender, EventArgs e)
        {
            Client c1 = new Client(Personne.cnt.ToString(), txtNomClient.Text, txtPrenomClient.Text, txtAdresseClient.Text, comboBoxEMP.Text);

            if (checkBoxRé.Checked)
            {
                
            }
            else if (checkBoxNonRé.Checked)
            {

            }

            ListEmp.Add(c1);
            
            listBoxClient.Items.Add(" " + c1.ID + " " + c1.NOM + " " + c1.PRENOM + " " + c1.SOLD + "$" + " " + c1.IDEMP);
            
        }

     

        private void butCreatBank_Click_1(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();

            //je rempli ma liste de banques
            Banque bnk1 = new Banque(Banque.cnt.ToString(), txtNomBank.Text, txtAdresseBank.Text, Convert.ToUInt16(txtCapital.Text), comboBoxDirec.Text,openFileDialog1.FileName);

            pictureBoxBank.Image = Image.FromFile(bnk1.photo);
            pictureBoxBank.SizeMode = PictureBoxSizeMode.StretchImage;

            ListDirecteur.Add(bnk1);
                        
            listBoxBank.Items.Add(" " + bnk1.ID + " " + bnk1.NOM+ " " + bnk1.ADRESSE + " " + bnk1.CAPITAL + "$" + " " + bnk1.DIRECTEUR+ " " );
            
            comboBoxChoixBank.Items.Add(bnk1.ID + " " + bnk1.NOM);

        }

        private void butEffectuer_Click(object sender, EventArgs e)
        {
            
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            radioButCheque.Visible = true;
            radioButEpargne.Visible = true;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            radioButCheque.Visible = true;
            radioButEpargne.Visible = true;
        }

        private void butModifierClient_Click(object sender, EventArgs e)
        {
            int pos = listBoxClient.SelectedIndex;
            ListClient.RemoveAt(pos);
            ListClient.TrimToSize();
            listBoxClient.Items.Clear();
        }

        private void butModifierEmp_Click(object sender, EventArgs e)
        {
            int pos = listBoxEmp.SelectedIndex;
            ListEmp.RemoveAt(pos);
            ListEmp.TrimToSize();
            listBoxEmp.Items.Clear();

        }

        private void butModifierAgence_Click(object sender, EventArgs e)
        {
            int pos = listBoxAgence.SelectedIndex;
            ListAgence.RemoveAt(pos);
            ListAgence.TrimToSize();
            listBoxAgence.Items.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
           
        }
    }
}
