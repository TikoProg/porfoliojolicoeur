﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Table_tournante_Guimaev
{
    public class Evaluation
    {
        private int no;
        private DateTime date_évaluation;
        private string id_client;
        private string id_entreprise;
        private string appréciation_générale;
        private string appréciation_table;
        private string appréciation_service;

        public static int cnt;

        public Evaluation()
        { }

        public Evaluation(int no,string id_client,string id_entreprise, DateTime date_évaluation, string appréciation_générale, string appréciation_service, string appréciation_table)
        {
            this.no = no;
            this.id_client = id_client;
            this.id_entreprise = id_entreprise;
            this.appréciation_générale = appréciation_générale;
            this.appréciation_table = appréciation_table;
            this.appréciation_service = appréciation_service;
            cnt++;
        }

        public int NO
        {
            get { return this.no ; }
            set { this.no = value; }
        }

        public string ID_CLIENT
        {
            get { return this.id_client; }
            set { this.id_client = value; }
        }

        public string ID_ENTREPRISE
        {
            get { return this.id_entreprise; }
            set { this.id_entreprise = value; }
        }

        public DateTime DATE
        {
            get { return this.date_évaluation ; }
            set { this.date_évaluation = value; }
        }

        public string APPGE
        {
            get { return this.appréciation_générale ; }
            set { this.appréciation_générale = value; }
        }

        public string APPSE
        {
            get { return this.appréciation_service ; }
            set { this.appréciation_service = value; }
        }

        public string APPTA
        {
            get { return this.appréciation_table ; }
            set { this.appréciation_table = value; }
        }
    }
}