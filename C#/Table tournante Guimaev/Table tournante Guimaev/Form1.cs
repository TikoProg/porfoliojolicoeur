﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Table_tournante_Guimaev
{
    public partial class Form1 : Form
    {
        private ArrayList listeCommande, listeClient, listeEntreprise, listePièce, listeTable, listeEvaluation, listeEmployé, listeFournisseur, listeAchat, listeVente, listeFabrication;
        private OleDbConnection conn;
        private OleDbCommand cmd;
        private OleDbCommand cmdCalcul;
        private OleDbDataReader reader;
        private DataTable table;
        private OleDbDataAdapter adapter;
        private bool ON = false;

        public Form1()
        {
            InitializeComponent();
            listeCommande = new ArrayList();
            listeClient = new ArrayList();
            listeEntreprise = new ArrayList();
            listePièce = new ArrayList();
            listeTable = new ArrayList();
            listeEvaluation = new ArrayList();
            listeEmployé = new ArrayList();
            listeFournisseur = new ArrayList();
            listeAchat = new ArrayList();
            listeVente = new ArrayList();
            listeFabrication = new ArrayList();



        }


        private void Form1_Load(object sender, EventArgs e)
        {

            try
            {
                String url = Application.StartupPath + @"\PRO-JECT_DB.accdb";
                conn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + url);
               
                
            }
            catch (Exception ex)
            {
                lblConnected.Text = ex.Message;
            }
            finally
            {
                lblConnected.Text = "Connexion réussie...";
            }
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                if (lblCONN.Text == "Clients")
                {
                    txtID.Text = row.Cells["id_client"].Value.ToString();
                    txtNom.Text = row.Cells["nom"].Value.ToString();
                    txtPrenom.Text = row.Cells["prénom"].Value.ToString();
                    txtNoCivique.Text = row.Cells["numéroCivique"].Value.ToString();
                    txtRue.Text = row.Cells["rue"].Value.ToString();
                    txtApp.Text = row.Cells["numéroAppartement"].Value.ToString();
                    txtVille.Text = row.Cells["ville"].Value.ToString();
                    txtProvince.Text = row.Cells["province"].Value.ToString();
                    txtCodePostal.Text = row.Cells["codePostal"].Value.ToString();
                    txtTelephone.Text = row.Cells["téléphone"].Value.ToString();
                    txtCell.Text = row.Cells["cellulaire"].Value.ToString();
                    txtCourriel.Text = row.Cells["courriel"].Value.ToString();

                }
                else if (lblCONN.Text == "Commandes")
                {
                    label93.Text = "Type :";
                    txtModèle.Text = row.Cells["type_commande"].Value.ToString();

                    txtNoCom.Text = row.Cells["no_commande"].Value.ToString();
                    txtNoTable.Text = row.Cells["no_table"].Value.ToString();
                    txtQuantiteCom.Text = row.Cells["quantité"].Value.ToString();
                    dateTimePicker1.Value = Convert.ToDateTime(row.Cells["date_commande"].Value);

                }
                else if (lblCONN.Text == "Employés")
                {
                    txtID.Text = row.Cells["id_employé"].Value.ToString();
                    txtNom.Text = row.Cells["nom"].Value.ToString();
                    txtPrenom.Text = row.Cells["prénom"].Value.ToString();
                    txtNoCivique.Text = row.Cells["numéroCivique"].Value.ToString();
                    txtRue.Text = row.Cells["rue"].Value.ToString();
                    txtApp.Text = row.Cells["numéroAppartement"].Value.ToString();
                    txtVille.Text = row.Cells["ville"].Value.ToString();
                    txtProvince.Text = row.Cells["province"].Value.ToString();
                    txtCodePostal.Text = row.Cells["codePostal"].Value.ToString();
                    txtTelephone.Text = row.Cells["téléphone"].Value.ToString();
                    txtCell.Text = row.Cells["cellulaire"].Value.ToString();
                    txtCourriel.Text = row.Cells["courriel"].Value.ToString();
                }
                else if (lblCONN.Text == "Entreprises")
                {
                    txtID_Entreprise.Text = row.Cells["id_entreprise"].Value.ToString();
                    txtID_Entreprise.Text = row.Cells["id_entreprise"].Value.ToString();
                    txtNom_Entreprise.Text = row.Cells["nom_entreprise"].Value.ToString();
                    txtNom_Contact.Text = row.Cells["Nom_contact"].Value.ToString();
                    txtPrenom_Contact.Text = row.Cells["prénom_contact"].Value.ToString();
                    txtNoCivique.Text = row.Cells["numéroCivique"].Value.ToString();
                    txtRue.Text = row.Cells["rue"].Value.ToString();
                    txtVille.Text = row.Cells["ville"].Value.ToString();
                    txtProvince.Text = row.Cells["province"].Value.ToString();
                    txtCodePostal.Text = row.Cells["codePostal"].Value.ToString();
                    txtTelephone.Text = row.Cells["téléphone"].Value.ToString();
                    txtCell.Text = row.Cells["cellulaire_contact"].Value.ToString();
                    txtCourriel.Text = row.Cells["courriel"].Value.ToString();
                    txtFax.Text = row.Cells["fax"].Value.ToString();
                }
               
                else if (lblCONN.Text == "Fournisseurs")
                {
                    txtID_Entreprise.Text = row.Cells["id_fournisseur"].Value.ToString();
                    txtNom_Entreprise.Text = row.Cells["nom"].Value.ToString();
                    txtNoCivique.Text = row.Cells["numéroCivique"].Value.ToString();
                    txtRue.Text = row.Cells["rue"].Value.ToString();
                    txtVille.Text = row.Cells["ville"].Value.ToString();
                    txtProvince.Text = row.Cells["province"].Value.ToString();
                    txtCodePostal.Text = row.Cells["codePostal"].Value.ToString();
                    txtTelephone.Text = row.Cells["téléphone"].Value.ToString();
                    txtCourriel.Text = row.Cells["courriel"].Value.ToString();
                }
                else if (lblCONN.Text == "Tables tournantes")
                {
                    txtModèle.Text = row.Cells["modèle"].Value.ToString();
                    txtPrix.Text = row.Cells["prix"].Value.ToString();
                    txtNoTable.Text = row.Cells["no_table"].Value.ToString();
                }



            }
        }

        private void dataGridViewATELIER_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridViewATELIER.Rows[e.RowIndex];


                if (lblCONN.Text == "Tables fabriquées")
                {
                    dateTimeFabrication.Value = Convert.ToDateTime(row.Cells["date_fabrication"].Value);
                    txtQuantiteCom.Text = row.Cells["no_pièce"].Value.ToString();
                    txtModèle.Text = row.Cells["id_fournisseur"].Value.ToString();
                    txtNoCom.Text = row.Cells["no_commande"].Value.ToString();
                    txtNoTAB.Text = row.Cells["no_table"].Value.ToString();
                    txtValeurPiece.Text = row.Cells["valeur_pièce"].Value.ToString();
                }
                else if (lblCONN.Text == "Pièces")
                {

                }
                else if (lblCONN.Text == "Vente")
                {

                }
                else if (lblCONN.Text == "Évaluations")
                {
                    txtEvaluation.Text = row.Cells["no_évaluation"].Value.ToString();
                    dateTime.Value = Convert.ToDateTime(row.Cells["date_évaluation"].Value);
                    txtID_ClientAppré.Text = row.Cells["id_client"].Value.ToString();
                    txtAppréGénéral.Text = row.Cells["appréciationGénérale"].Value.ToString();
                    txtAppréService.Text = row.Cells["appréciationService"].Value.ToString();
                    txtAppréTable.Text = row.Cells["appréciationTable"].Value.ToString();
                }
            }
        }



        private void butClients_Click(object sender, EventArgs e)
        {
            lblCONN.Text = butClients.Text;
            ON = true;

            if (!ON == false)
            {
                try
                {
                    conn.Open();
                    cmd = new OleDbCommand("Client", conn);
                    cmd.CommandType = CommandType.TableDirect;
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listeClient.Add(new Client(reader["id_client"].ToString(), reader["nom"].ToString(), reader["prénom"].ToString(), (int)reader["numérocivique"], reader["rue"].ToString(), reader["numéroAppartement"].ToString(), reader["ville"].ToString(), reader["province"].ToString(), reader["codePostal"].ToString(), reader["téléphone"].ToString(), reader["cellulaire"].ToString(), reader["courriel"].ToString()));

                    }
                    
                }
                catch (Exception ex)
                {
                    lblConnected.Text = ex.Message;
                }
                finally
                {
                    conn.Close();
                    adapter = new OleDbDataAdapter(cmd);
                    table = new DataTable();
                    adapter.FillSchema(table, SchemaType.Mapped);
                    adapter.Fill(table);

                    conn.Close();
                    dataGridView1.DataSource = table;
                    ON = false;
                }
            }
        }

        private void butEmployés_Click(object sender, EventArgs e)
        {
            lblCONN.Text = butEmployés.Text;
            ON = true;

            if (!ON == false)
            {

                try
                {
                    conn.Open();
                    cmd = new OleDbCommand("Employé", conn);
                    cmd.CommandType = CommandType.TableDirect;
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listeEmployé.Add(new Employé(reader["id_employé"].ToString(), reader["nom"].ToString(), reader["prénom"].ToString(), (int)reader["numérocivique"], reader["rue"].ToString(), reader["numéroAppartement"].ToString(), reader["ville"].ToString(), reader["province"].ToString(), reader["codePostal"].ToString(), reader["téléphone"].ToString(), reader["cellulaire"].ToString(), reader["courriel"].ToString()));

                    }
                    
                }
                catch (Exception ex)
                {
                    lblConnected.Text = ex.Message;
                }
                finally
                {
                    conn.Close();
                    adapter = new OleDbDataAdapter(cmd);
                    table = new DataTable();
                    adapter.FillSchema(table, SchemaType.Mapped);
                    adapter.Fill(table);

                    conn.Close();
                    dataGridView1.DataSource = table;
                    ON = false;
                }
            }

        }

        private void butFournisseurs_Click(object sender, EventArgs e)
        {
            lblCONN.Text = butFournisseurs.Text;
            ON = true;

            if (!ON == false)
            {

                try
                {
                    conn.Open();
                    cmd = new OleDbCommand("Fournisseur", conn);
                    cmd.CommandType = CommandType.TableDirect;
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listeFournisseur.Add(new Fournisseur(reader["id_fournisseur"].ToString(), reader["nom"].ToString(), (int)reader["numérocivique"], reader["rue"].ToString(), reader["ville"].ToString(), reader["province"].ToString(), reader["codePostal"].ToString(), reader["téléphone"].ToString(), reader["courriel"].ToString()));

                    }
                    
                }
                catch (Exception ex)
                {
                    lblConnected.Text = ex.Message;
                }
                finally
                {
                    conn.Close();
                    adapter = new OleDbDataAdapter(cmd);
                    table = new DataTable();
                    adapter.FillSchema(table, SchemaType.Mapped);
                    adapter.Fill(table);

                    conn.Close();
                    dataGridView1.DataSource = table;
                    ON = false;
                }
            }
        }

        private void butEntreprise_Click(object sender, EventArgs e)
        {
            lblCONN.Text = butEntreprise.Text;
            groupBoxCommande.Enabled = false;
            groupBoxClient.Enabled = false;
            ON = true;

            if (!ON == false)
            {
                try
                {
                    conn.Open();
                    cmd = new OleDbCommand("Entreprise", conn);
                    cmd.CommandType = CommandType.TableDirect;
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listeEntreprise.Add(new Entreprise(reader["id_entreprise"].ToString(), reader["nom_entreprise"].ToString(), reader["nom_contact"].ToString(), reader["prénom_contact"].ToString(), (int)reader["numérocivique"], reader["rue"].ToString(), reader["ville"].ToString(), reader["province"].ToString(), reader["codePostal"].ToString(), reader["téléphone"].ToString(), reader["cellulaire_contact"].ToString(), reader["fax"].ToString(), reader["courriel"].ToString()));

                    }

                }
                catch (Exception ex)
                {
                    lblConnected.Text = ex.Message;
                }
                finally
                {
                    conn.Close();
                    adapter = new OleDbDataAdapter(cmd);
                    table = new DataTable();
                    adapter.FillSchema(table, SchemaType.Mapped);
                    adapter.Fill(table);

                    conn.Close();
                    dataGridView1.DataSource = table;
                    ON = false;
                }
            }
        }

        private void butTable_Click(object sender, EventArgs e)
        {
            lblCONN.Text = butTable.Text;
            ON = true;

            if (!ON == false)
            {

                try
                {
                    conn.Open();
                    cmd = new OleDbCommand("Table_tournante", conn);
                    cmd.CommandType = CommandType.TableDirect;
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listeTable.Add(new Table_tournante((int)reader["no_table"], reader["modèle"].ToString(), reader["description"].ToString(), (int)reader["prix"]));

                    }
                  
                }
                catch (Exception ex)
                {
                    lblConnected.Text = ex.Message;
                }
                finally
                {
                    conn.Close();
                    adapter = new OleDbDataAdapter(cmd);
                    table = new DataTable();
                    adapter.FillSchema(table, SchemaType.Mapped);
                    adapter.Fill(table);

                    conn.Close();
                    dataGridView1.DataSource = table;
                    ON = false;
                }
            }
        }

        private void butCommandes_Click(object sender, EventArgs e)
        {
            lblCONN.Text = butCommandes.Text;
            ON = true;

            if (!ON == false)
            {
                try
                {
                    conn.Open();
                    cmd = new OleDbCommand("Commande", conn);
                    cmd.CommandType = CommandType.TableDirect;
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listeCommande.Add(new Commande((int)reader["no_commande"], reader["id_client"].ToString(), reader["id_entreprise"].ToString(), Convert.ToDateTime(reader["date_commande"]), (int)reader["no_table"], reader["type_commande"].ToString(), (int)reader["quantité"]));

                    }
                   
                }
                catch (Exception ex)
                {
                    lblConnected.Text = ex.Message;
                }
                finally
                {
                    conn.Close();
                    adapter = new OleDbDataAdapter(cmd);
                    table = new DataTable();
                    adapter.FillSchema(table, SchemaType.Mapped);
                    adapter.Fill(table);

                    conn.Close();
                    dataGridView1.DataSource = table;



                    ON = false;
                }
            }

        }

        private void butVentes_Click(object sender, EventArgs e)
        {
            lblCONN.Text = butVente.Text;
            ON = true;

            if (!ON == false)
            {
                try
                {
                    conn.Open();
                    cmd = new OleDbCommand("Vente", conn);
                    cmd.CommandType = CommandType.TableDirect;
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listeVente.Add(new Vente((int)reader["no_vente"], reader["id_employé"].ToString(), (int)reader["no_commande"], (int)reader["no_table"], (DateTime)reader["date_vente"], (int)reader["quantité"], (double)reader["prix"]));
                    }
                 
                }
                catch (Exception ex)
                {
                    lblConnected.Text = ex.Message;
                }
                finally
                {
                    conn.Close();
                    adapter = new OleDbDataAdapter(cmd);
                    table = new DataTable();
                    adapter.FillSchema(table, SchemaType.Mapped);
                    adapter.Fill(table);

                    conn.Close();
                    dataGridView1.DataSource = table;

                    double sum = 00.00;
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        double s1 = Convert.ToDouble(dataGridView1.Rows[i].Cells[5].Value) * Convert.ToDouble(dataGridView1.Rows[i].Cells[6].Value);
                        sum += s1;
                    }
                    lblSum.Text = sum.ToString() + "$";
                    lblSumTaxe.Text = (sum + (sum * 0.05 + sum * 0.09975)).ToString().ToString() + "$";


                    ON = false;
                }
            }
        }


        
        private void butPiece_Click(object sender, EventArgs e)
        {
            lblAFF.Text = butPiece.Text;
            ON = true;

            if (!ON == false)
            {
                try
                {
                    conn.Open();
                    cmd = new OleDbCommand("Pièce", conn);
                    cmd.CommandType = CommandType.TableDirect;
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listePièce.Add(new Pièce((int)reader["no_pièce"], reader["description"].ToString(), reader["commentaire"].ToString(), (double)reader["valeur"]));

                    }
                    
                }
                catch (Exception ex)
                {
                    lblConnected.Text = ex.Message;
                }
                finally
                {
                    conn.Close();
                    adapter = new OleDbDataAdapter(cmd);
                    table = new DataTable();
                    adapter.FillSchema(table, SchemaType.Mapped);
                    adapter.Fill(table);

                    conn.Close();
                    dataGridViewATELIER.DataSource = table;
                    ON = false;
                }
            }
        }

        private void butACHAT_Click_1(object sender, EventArgs e)
        {
            lblAFF.Text = butACHAT.Text;
            ON = true;

            if (!ON == false)
            {

                try
                {
                    conn.Open();
                    cmd = new OleDbCommand("Achat_Pièce", conn);
                    cmd.CommandType = CommandType.TableDirect;
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listePièce.Add(new Achat_Pièce((int)reader["no_achat"], (int)reader["no_pièce"], reader["no_fournisseur"].ToString(), (DateTime)reader["date_achat"], (int)reader["quantité"], (double)reader["valeur"]));

                    }
                
                }
                catch (Exception ex)
                {
                    lblConnected.Text = ex.Message;
                }
                finally
                {
                    conn.Close();
                    adapter = new OleDbDataAdapter(cmd);
                    table = new DataTable();
                    adapter.FillSchema(table, SchemaType.Mapped);
                    adapter.Fill(table);

                    conn.Close();
                    dataGridViewATELIER.DataSource = table;
                    ON = false;
                }
            }
        }

        private void butFabrication_Click(object sender, EventArgs e)
        {
            lblAFF.Text = butFabrication.Text;
            ON = true;

            if (!ON == false)
            {
                try
                {
                    conn.Open();
                    cmd = new OleDbCommand("Fabrication", conn);
                    cmd.CommandType = CommandType.TableDirect;
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listeFabrication.Add(new Fabrication((int)reader["no_fabrication"], (int)reader["no_commande"], reader["id_employé"].ToString(), (DateTime)reader["date_fabrication"], (int)reader["no_table"], (int)reader["no_pièce"], (double)reader["valeur"]));
                    }

                }
                catch (Exception ex)
                {
                    lblConnected.Text = ex.Message;
                }
                finally
                {
                    conn.Close();
                    adapter = new OleDbDataAdapter(cmd);
                    table = new DataTable();
                    adapter.FillSchema(table, SchemaType.Mapped);
                    adapter.Fill(table);

                    conn.Close();
                    dataGridViewATELIER.DataSource = table;
                    ON = false;
                }
            }
        }



        private void butInsert_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePickerFabrication_ValueChanged(object sender, EventArgs e)
        {

        }

        private void butÉvaluations_Click_1(object sender, EventArgs e)
        {

            lblAFF.Text = butÉvaluations.Text;
            ON = true;

            if (!ON == false)
            {

                try
                {
                    conn.Open();
                    cmd = new OleDbCommand("Évaluation", conn);
                    cmd.CommandType = CommandType.TableDirect;
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listeEvaluation.Add(new Evaluation((int)reader["no_évaluation"], reader["id_client"].ToString(), reader["id_entreprise"].ToString(), (DateTime)reader["date_évaluation"], reader["appréciationGénérale"].ToString(), reader["appréciationTable"].ToString(), reader["appréciationService"].ToString()));

                    }

                }
                catch (Exception ex)
                {
                    lblConnected.Text = ex.Message;
                }
                finally
                {
                    conn.Close();
                    adapter = new OleDbDataAdapter(cmd);
                    table = new DataTable();
                    adapter.FillSchema(table, SchemaType.Mapped);
                    adapter.Fill(table);

                    conn.Close();
                    dataGridViewATELIER.DataSource = table;
                    ON = false;
                }
            }
        }


        private void butModifier_Click(object sender, EventArgs e)
        {

        }

        private void butEffacer_Click(object sender, EventArgs e)
        {

        }

        private void Search_Button_Click(object sender, EventArgs e)
        {
            if (lblCONN.Text == "Commandes")
            {
                Search_Commande();
            }
            else if (lblCONN.Text == "Entreprises")
            {
                search_Entreprise();
            }
            else if (lblCONN.Text == "Clients")
            {
                Search_Client();
            }

        }



        private void butInsert_1_Click(object sender, EventArgs e)
        {

        }

        private void butModifier_1_Click(object sender, EventArgs e)
        {

        }

        private void butEfacer_1_Click(object sender, EventArgs e)
        {

        }

        private void Search_Button2_Click(object sender, EventArgs e)
        {

        }




        //**************Fonctions**************

        private void Search_Commande()
        {
            ON = true;

            if (!ON == false)
            {
                try
                {
                    conn.Open();
                    cmd = new OleDbCommand(" select no_commande, date_fabrication, type_commande, no_table, no_pièce, prix_table from Commande where no_commande =(" + textBoxSearch.Text + ") ", conn);
                    adapter = new OleDbDataAdapter(cmd);
                    table = new DataTable();
                    adapter.FillSchema(table, SchemaType.Mapped);
                    adapter.Fill(table);
                    dataGridView1.DataSource = table;

                }
                catch (Exception ex)
                {
                    lblConnected.Text = ex.Message;
                }
                finally
                {
                    conn.Close();
                    
                    ON = false;
                }
            }
        }

        private void search_Entreprise()
        {
            ON = true;

            if (!ON == false)
            {
                try
                {
                    conn.Open();
                    cmd = new OleDbCommand(" select id_entreprise, nom_entreprise, nom_contact, prénom_contact, numéroCivique, rue, ville, province, codePostal from Entreprise where id_entreprise = ('" + textBoxSearch.Text + "') ", conn);
                    adapter = new OleDbDataAdapter(cmd);
                    table = new DataTable();
                    adapter.FillSchema(table, SchemaType.Mapped);
                    adapter.Fill(table);
                    dataGridView1.DataSource = table;
                }
                catch (Exception ex)
                {
                    lblConnected.Text = ex.Message;
                }
                finally
                {

                    conn.Close();
                    ON = false;
                }
            }
        }

        private void Search_Client()
        {
            ON = true;

            if (!ON == false)
            {
                try
                {
                    conn.Open();
                    cmd = new OleDbCommand(" select id_client, nom_contact, prénom, numérocivique, rue, numéroAppartement, ville, province, codePostal from Client where id_client = ('" + textBoxSearch.Text + "') ", conn);
                    adapter = new OleDbDataAdapter(cmd);
                    table = new DataTable();
                    adapter.FillSchema(table, SchemaType.Mapped);
                    adapter.Fill(table);
                    dataGridView1.DataSource = table;
                }
                catch (Exception ex)
                {
                    lblConnected.Text = ex.Message;
                }
                finally
                {
                    conn.Close();
                    ON = false;
                }
            }
        }

    }
}

               



//txtID_Entreprise.Text = row.Cells["id_entreprise"].Value.ToString();
//txtID_Entreprise.Text = row.Cells["nom_entreprise"].Value.ToString();
//txtNom_Contact.Text = row.Cells["Nom_contact"].Value.ToString();
//txtPrenom_Contact.Text = row.Cells["prénom_contact"].Value.ToString();

//txtID.Text = row.Cells["id_client"].Value.ToString();
//txtNom.Text = row.Cells["nom_contact"].Value.ToString();
//txtPrenom.Text = row.Cells["prénom"].Value.ToString();
//txtNoCivique.Text = row.Cells["numéroCivique"].Value.ToString();
//txtRue.Text = row.Cells["rue"].Value.ToString();
//txtApp.Text = row.Cells["numéroAppartement"].Value.ToString();
//txtVille.Text = row.Cells["ville"].Value.ToString();
//txtProvince.Text = row.Cells["province"].Value.ToString();
//txtCodePostal.Text = row.Cells["codePostal"].Value.ToString();
//txtTelephone.Text = row.Cells["téléphone"].Value.ToString();
//txtCell.Text = row.Cells["cellulaire_contact"].Value.ToString();
//txtCourriel.Text = row.Cells["courriel"].Value.ToString();
//txtFax.Text = row.Cells["fax"].Value.ToString();

//txtQuantiteCom.Text = row.Cells["no_pièce"].Value.ToString();
//txtModèle.Text = row.Cells["id_fournisseur"].Value.ToString();
//txtNoCom.Text = row.Cells["no_commande"].Value.ToString();
//txtNoTable.Text = row.Cells["no_table"].Value.ToString();
//dateTimePicker1.Value = Convert.ToDateTime(row.Cells["date_fabrication"].Value);
//dateTimePicker2.Value = Convert.ToDateTime(row.Cells["date_fabrication"].Value);


//txtEvaluation.Text = row.Cells["no_évaluation"].Value.ToString();
//dateTimePicker3.Value = Convert.ToDateTime(row.Cells["date_évaluation"].Value);
//txtID_ClientAppré.Text = row.Cells["id_client"].Value.ToString();
//txtApréGénéral.Text = row.Cells["appréciationGénérale"].Value.ToString();
//txtApréService.Text = row.Cells["appréciationService"].Value.ToString();
//txtApréTable.Text = row.Cells["appréciationTable"].Value.ToString();