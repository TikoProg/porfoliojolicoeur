﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Table_tournante_Guimaev
{
    public class Vente
    {
        private int no_vente;
        private int no_table;
        private int no_commande;
        private string id_employé;
        private DateTime date_vente;
        private int quantité;
        private double prix;

        public static int cnt;

        public Vente()
        { }

        public Vente(int no_vente, string id_employé, int no_commande, int no_table,  DateTime date_vente, int quantité, double prix)
        {
            this.no_vente = no_vente;
            this.no_table = no_table;
            this.id_employé = id_employé;
            this.no_commande = no_commande;
            this.date_vente = date_vente;
            this.quantité = quantité;
            this.prix = prix;
            cnt++;
        }

        public int NO_VENTE
        {
            get { return this.no_vente; }
            set { this.no_vente = value; }
        }


        public string ID_EMPLOYÉ
        {
            get { return this.id_employé; }
            set { this.id_employé = value; }
        }

        public DateTime DATE
        {
            get { return this.date_vente; }
            set { this.date_vente = value; }
        }

        public int NO_COMMANDE
        {
            get { return this.no_commande; }
            set { this.no_commande = value; }
        }

        public int NO_TABLE
        {
            get { return this.no_table; }
            set { this.no_table = value; }
        }


        public int QUANTITÉ
        {
            get { return this.quantité; }
            set { this.quantité = value; }
        }

        public double PRIX
        {
            get { return this.prix; }
            set { this.prix = value; }
        }
    }
}