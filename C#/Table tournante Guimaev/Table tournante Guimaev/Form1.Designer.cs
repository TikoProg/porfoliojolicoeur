﻿namespace Table_tournante_Guimaev
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label2 = new System.Windows.Forms.Label();
            this.lblConnected = new System.Windows.Forms.Label();
            this.Home = new System.Windows.Forms.TabPage();
            this.butVente = new System.Windows.Forms.Button();
            this.lblCONN = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblSumTaxe = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSum = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBoxClient = new System.Windows.Forms.GroupBox();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtPrenom = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNom_Entreprise = new System.Windows.Forms.TextBox();
            this.txtID_Entreprise = new System.Windows.Forms.TextBox();
            this.txtPrenom_Contact = new System.Windows.Forms.TextBox();
            this.txtNom_Contact = new System.Windows.Forms.TextBox();
            this.butEmployés = new System.Windows.Forms.Button();
            this.butEntreprise = new System.Windows.Forms.Button();
            this.butClients = new System.Windows.Forms.Button();
            this.butFournisseurs = new System.Windows.Forms.Button();
            this.butTable = new System.Windows.Forms.Button();
            this.butCommandes = new System.Windows.Forms.Button();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.butModifier = new System.Windows.Forms.Button();
            this.butInserer = new System.Windows.Forms.Button();
            this.butEffacer = new System.Windows.Forms.Button();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.txtFax = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.txtCell = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.txtCourriel = new System.Windows.Forms.TextBox();
            this.groupBoxCommande = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPrix = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.txtModèle = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNoTable = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.txtQuantiteCom = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.txtNoCom = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.txtNoCivique = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.txtCodePostal = new System.Windows.Forms.TextBox();
            this.txtRue = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.txtProvince = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.txtVille = new System.Windows.Forms.TextBox();
            this.txtApp = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.groupBoxSearch = new System.Windows.Forms.GroupBox();
            this.lblCONN2 = new System.Windows.Forms.Label();
            this.dateTimePickerRECHERCHE = new System.Windows.Forms.DateTimePicker();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.Search_Button = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.TabControl = new System.Windows.Forms.TabControl();
            this.Atelier = new System.Windows.Forms.TabPage();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.txtAppréTable = new System.Windows.Forms.TextBox();
            this.dateTime = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAppréService = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtEvaluation = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.txtAppréGénéral = new System.Windows.Forms.TextBox();
            this.txtID_ClientAppré = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.butÉvaluations = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblAFF = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.butModifier_1 = new System.Windows.Forms.Button();
            this.butInsert_1 = new System.Windows.Forms.Button();
            this.butEfacer_1 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.txtSearchFabrication = new System.Windows.Forms.TextBox();
            this.Search_Button2 = new System.Windows.Forms.Button();
            this.butACHAT = new System.Windows.Forms.Button();
            this.butFabrication = new System.Windows.Forms.Button();
            this.butPiece = new System.Windows.Forms.Button();
            this.dataGridViewATELIER = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtValeurPiece = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtModele = new System.Windows.Forms.TextBox();
            this.txtNoTAB = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textQUANT = new System.Windows.Forms.TextBox();
            this.textNoComFab = new System.Windows.Forms.TextBox();
            this.butAfficheAss = new System.Windows.Forms.Button();
            this.butAfficheVendeur = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.dateTimeFabrication = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.Facture = new System.Windows.Forms.TabPage();
            this.Home.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBoxClient.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBoxCommande.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBoxSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.TabControl.SuspendLayout();
            this.Atelier.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewATELIER)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label2.Font = new System.Drawing.Font("Showcard Gothic", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(963, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 46);
            this.label2.TabIndex = 1;
            this.label2.Text = "PRO-JECT";
            // 
            // lblConnected
            // 
            this.lblConnected.AutoSize = true;
            this.lblConnected.BackColor = System.Drawing.Color.Transparent;
            this.lblConnected.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConnected.ForeColor = System.Drawing.Color.Red;
            this.lblConnected.Location = new System.Drawing.Point(22, 13);
            this.lblConnected.Name = "lblConnected";
            this.lblConnected.Size = new System.Drawing.Size(0, 13);
            this.lblConnected.TabIndex = 2;
            // 
            // Home
            // 
            this.Home.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Home.Controls.Add(this.butVente);
            this.Home.Controls.Add(this.lblCONN);
            this.Home.Controls.Add(this.panel1);
            this.Home.Controls.Add(this.groupBoxClient);
            this.Home.Controls.Add(this.groupBox1);
            this.Home.Controls.Add(this.butEmployés);
            this.Home.Controls.Add(this.butEntreprise);
            this.Home.Controls.Add(this.butClients);
            this.Home.Controls.Add(this.butFournisseurs);
            this.Home.Controls.Add(this.butTable);
            this.Home.Controls.Add(this.butCommandes);
            this.Home.Controls.Add(this.groupBox32);
            this.Home.Controls.Add(this.groupBox21);
            this.Home.Controls.Add(this.groupBoxCommande);
            this.Home.Controls.Add(this.label68);
            this.Home.Controls.Add(this.groupBox28);
            this.Home.Controls.Add(this.groupBoxSearch);
            this.Home.Controls.Add(this.dataGridView1);
            this.Home.Location = new System.Drawing.Point(4, 27);
            this.Home.Name = "Home";
            this.Home.Padding = new System.Windows.Forms.Padding(3);
            this.Home.Size = new System.Drawing.Size(1144, 478);
            this.Home.TabIndex = 7;
            this.Home.Text = "Home";
            // 
            // butVente
            // 
            this.butVente.BackColor = System.Drawing.Color.Silver;
            this.butVente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butVente.Location = new System.Drawing.Point(450, 8);
            this.butVente.Name = "butVente";
            this.butVente.Size = new System.Drawing.Size(79, 31);
            this.butVente.TabIndex = 79;
            this.butVente.Text = "Ventes";
            this.butVente.UseVisualStyleBackColor = false;
            this.butVente.Click += new System.EventHandler(this.butVentes_Click);
            // 
            // lblCONN
            // 
            this.lblCONN.AutoSize = true;
            this.lblCONN.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblCONN.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCONN.ForeColor = System.Drawing.Color.Lime;
            this.lblCONN.Location = new System.Drawing.Point(1012, 19);
            this.lblCONN.Name = "lblCONN";
            this.lblCONN.Size = new System.Drawing.Size(44, 16);
            this.lblCONN.TabIndex = 78;
            this.lblCONN.Text = "---------";
            this.lblCONN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Controls.Add(this.lblSumTaxe);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblSum);
            this.panel1.Controls.Add(this.label3);
            this.panel1.ForeColor = System.Drawing.Color.Lime;
            this.panel1.Location = new System.Drawing.Point(930, 347);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 62);
            this.panel1.TabIndex = 77;
            // 
            // lblSumTaxe
            // 
            this.lblSumTaxe.AutoSize = true;
            this.lblSumTaxe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSumTaxe.Location = new System.Drawing.Point(75, 35);
            this.lblSumTaxe.Name = "lblSumTaxe";
            this.lblSumTaxe.Size = new System.Drawing.Size(0, 15);
            this.lblSumTaxe.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Total  :";
            // 
            // lblSum
            // 
            this.lblSum.AutoSize = true;
            this.lblSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSum.Location = new System.Drawing.Point(75, 12);
            this.lblSum.Name = "lblSum";
            this.lblSum.Size = new System.Drawing.Size(0, 15);
            this.lblSum.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "+Taxes :";
            // 
            // groupBoxClient
            // 
            this.groupBoxClient.BackColor = System.Drawing.Color.Gray;
            this.groupBoxClient.Controls.Add(this.txtNom);
            this.groupBoxClient.Controls.Add(this.txtID);
            this.groupBoxClient.Controls.Add(this.txtPrenom);
            this.groupBoxClient.Controls.Add(this.label87);
            this.groupBoxClient.Controls.Add(this.label90);
            this.groupBoxClient.Controls.Add(this.label89);
            this.groupBoxClient.Location = new System.Drawing.Point(12, 161);
            this.groupBoxClient.Name = "groupBoxClient";
            this.groupBoxClient.Size = new System.Drawing.Size(517, 44);
            this.groupBoxClient.TabIndex = 76;
            this.groupBoxClient.TabStop = false;
            // 
            // txtNom
            // 
            this.txtNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNom.Location = new System.Drawing.Point(204, 15);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(113, 20);
            this.txtNom.TabIndex = 34;
            // 
            // txtID
            // 
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.Location = new System.Drawing.Point(44, 15);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(76, 20);
            this.txtID.TabIndex = 38;
            // 
            // txtPrenom
            // 
            this.txtPrenom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrenom.Location = new System.Drawing.Point(384, 15);
            this.txtPrenom.Name = "txtPrenom";
            this.txtPrenom.Size = new System.Drawing.Size(124, 20);
            this.txtPrenom.TabIndex = 36;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.BackColor = System.Drawing.Color.Transparent;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(6, 18);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(32, 13);
            this.label87.TabIndex = 37;
            this.label87.Text = " ID :";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(158, 18);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(40, 13);
            this.label90.TabIndex = 31;
            this.label90.Text = "Nom :";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(321, 18);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(57, 13);
            this.label89.TabIndex = 32;
            this.label89.Text = "Prénom :";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Gray;
            this.groupBox1.Controls.Add(this.txtNom_Entreprise);
            this.groupBox1.Controls.Add(this.txtID_Entreprise);
            this.groupBox1.Controls.Add(this.txtPrenom_Contact);
            this.groupBox1.Controls.Add(this.txtNom_Contact);
            this.groupBox1.Location = new System.Drawing.Point(12, 112);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(399, 46);
            this.groupBox1.TabIndex = 75;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ent ID       Nom                    Nom Contact   Prenom Contact  ";
            // 
            // txtNom_Entreprise
            // 
            this.txtNom_Entreprise.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNom_Entreprise.Location = new System.Drawing.Point(74, 16);
            this.txtNom_Entreprise.Name = "txtNom_Entreprise";
            this.txtNom_Entreprise.Size = new System.Drawing.Size(104, 20);
            this.txtNom_Entreprise.TabIndex = 59;
            // 
            // txtID_Entreprise
            // 
            this.txtID_Entreprise.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID_Entreprise.Location = new System.Drawing.Point(8, 16);
            this.txtID_Entreprise.Name = "txtID_Entreprise";
            this.txtID_Entreprise.Size = new System.Drawing.Size(57, 20);
            this.txtID_Entreprise.TabIndex = 58;
            // 
            // txtPrenom_Contact
            // 
            this.txtPrenom_Contact.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrenom_Contact.Location = new System.Drawing.Point(281, 16);
            this.txtPrenom_Contact.Name = "txtPrenom_Contact";
            this.txtPrenom_Contact.Size = new System.Drawing.Size(108, 20);
            this.txtPrenom_Contact.TabIndex = 57;
            // 
            // txtNom_Contact
            // 
            this.txtNom_Contact.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNom_Contact.Location = new System.Drawing.Point(185, 16);
            this.txtNom_Contact.Name = "txtNom_Contact";
            this.txtNom_Contact.Size = new System.Drawing.Size(90, 20);
            this.txtNom_Contact.TabIndex = 56;
            // 
            // butEmployés
            // 
            this.butEmployés.BackColor = System.Drawing.Color.Silver;
            this.butEmployés.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butEmployés.Location = new System.Drawing.Point(175, 8);
            this.butEmployés.Name = "butEmployés";
            this.butEmployés.Size = new System.Drawing.Size(69, 31);
            this.butEmployés.TabIndex = 9;
            this.butEmployés.Text = "Employés";
            this.butEmployés.UseVisualStyleBackColor = false;
            this.butEmployés.Click += new System.EventHandler(this.butEmployés_Click);
            // 
            // butEntreprise
            // 
            this.butEntreprise.BackColor = System.Drawing.Color.Silver;
            this.butEntreprise.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butEntreprise.Location = new System.Drawing.Point(250, 8);
            this.butEntreprise.Name = "butEntreprise";
            this.butEntreprise.Size = new System.Drawing.Size(79, 31);
            this.butEntreprise.TabIndex = 71;
            this.butEntreprise.Text = "Entreprises";
            this.butEntreprise.UseVisualStyleBackColor = false;
            this.butEntreprise.Click += new System.EventHandler(this.butEntreprise_Click);
            // 
            // butClients
            // 
            this.butClients.BackColor = System.Drawing.Color.Silver;
            this.butClients.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butClients.Location = new System.Drawing.Point(12, 8);
            this.butClients.Name = "butClients";
            this.butClients.Size = new System.Drawing.Size(64, 31);
            this.butClients.TabIndex = 70;
            this.butClients.Text = "Clients";
            this.butClients.UseVisualStyleBackColor = false;
            this.butClients.Click += new System.EventHandler(this.butClients_Click);
            // 
            // butFournisseurs
            // 
            this.butFournisseurs.BackColor = System.Drawing.Color.Silver;
            this.butFournisseurs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butFournisseurs.Location = new System.Drawing.Point(336, 8);
            this.butFournisseurs.Name = "butFournisseurs";
            this.butFournisseurs.Size = new System.Drawing.Size(108, 31);
            this.butFournisseurs.TabIndex = 68;
            this.butFournisseurs.Text = "Fournisseurs";
            this.butFournisseurs.UseVisualStyleBackColor = false;
            this.butFournisseurs.Click += new System.EventHandler(this.butFournisseurs_Click);
            // 
            // butTable
            // 
            this.butTable.BackColor = System.Drawing.Color.Silver;
            this.butTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butTable.Location = new System.Drawing.Point(535, 8);
            this.butTable.Name = "butTable";
            this.butTable.Size = new System.Drawing.Size(117, 31);
            this.butTable.TabIndex = 67;
            this.butTable.Text = "Tables tournantes";
            this.butTable.UseVisualStyleBackColor = false;
            this.butTable.Click += new System.EventHandler(this.butTable_Click);
            // 
            // butCommandes
            // 
            this.butCommandes.BackColor = System.Drawing.Color.Silver;
            this.butCommandes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butCommandes.Location = new System.Drawing.Point(81, 8);
            this.butCommandes.Name = "butCommandes";
            this.butCommandes.Size = new System.Drawing.Size(88, 31);
            this.butCommandes.TabIndex = 64;
            this.butCommandes.Text = "Commandes";
            this.butCommandes.UseVisualStyleBackColor = false;
            this.butCommandes.Click += new System.EventHandler(this.butCommandes_Click);
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.butModifier);
            this.groupBox32.Controls.Add(this.butInserer);
            this.groupBox32.Controls.Add(this.butEffacer);
            this.groupBox32.Location = new System.Drawing.Point(417, 42);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(112, 116);
            this.groupBox32.TabIndex = 63;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "Éditer";
            // 
            // butModifier
            // 
            this.butModifier.BackColor = System.Drawing.Color.LightGreen;
            this.butModifier.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butModifier.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.butModifier.Location = new System.Drawing.Point(7, 46);
            this.butModifier.Name = "butModifier";
            this.butModifier.Size = new System.Drawing.Size(99, 33);
            this.butModifier.TabIndex = 12;
            this.butModifier.Text = "Modifier";
            this.butModifier.UseVisualStyleBackColor = false;
            this.butModifier.Click += new System.EventHandler(this.butModifier_Click);
            // 
            // butInserer
            // 
            this.butInserer.BackColor = System.Drawing.Color.LightSkyBlue;
            this.butInserer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butInserer.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.butInserer.Location = new System.Drawing.Point(7, 13);
            this.butInserer.Name = "butInserer";
            this.butInserer.Size = new System.Drawing.Size(99, 35);
            this.butInserer.TabIndex = 10;
            this.butInserer.Text = "Insérer";
            this.butInserer.UseVisualStyleBackColor = false;
            // 
            // butEffacer
            // 
            this.butEffacer.BackColor = System.Drawing.Color.Red;
            this.butEffacer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butEffacer.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.butEffacer.Location = new System.Drawing.Point(7, 77);
            this.butEffacer.Name = "butEffacer";
            this.butEffacer.Size = new System.Drawing.Size(99, 34);
            this.butEffacer.TabIndex = 11;
            this.butEffacer.Text = "Effacer";
            this.butEffacer.UseVisualStyleBackColor = false;
            this.butEffacer.Click += new System.EventHandler(this.butEffacer_Click);
            // 
            // groupBox21
            // 
            this.groupBox21.BackColor = System.Drawing.Color.CornflowerBlue;
            this.groupBox21.Controls.Add(this.txtFax);
            this.groupBox21.Controls.Add(this.label92);
            this.groupBox21.Controls.Add(this.txtCell);
            this.groupBox21.Controls.Add(this.label47);
            this.groupBox21.Controls.Add(this.txtTelephone);
            this.groupBox21.Controls.Add(this.label27);
            this.groupBox21.Controls.Add(this.label44);
            this.groupBox21.Controls.Add(this.txtCourriel);
            this.groupBox21.Location = new System.Drawing.Point(12, 274);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(178, 118);
            this.groupBox21.TabIndex = 62;
            this.groupBox21.TabStop = false;
            // 
            // txtFax
            // 
            this.txtFax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFax.Location = new System.Drawing.Point(47, 59);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(122, 20);
            this.txtFax.TabIndex = 54;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(6, 62);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(35, 13);
            this.label92.TabIndex = 53;
            this.label92.Text = "Fax :";
            // 
            // txtCell
            // 
            this.txtCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCell.Location = new System.Drawing.Point(74, 35);
            this.txtCell.Name = "txtCell";
            this.txtCell.Size = new System.Drawing.Size(95, 20);
            this.txtCell.TabIndex = 52;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(6, 14);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(75, 13);
            this.label47.TabIndex = 47;
            this.label47.Text = "Téléphone :";
            // 
            // txtTelephone
            // 
            this.txtTelephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelephone.Location = new System.Drawing.Point(88, 11);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(81, 20);
            this.txtTelephone.TabIndex = 48;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(6, 38);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(67, 13);
            this.label27.TabIndex = 51;
            this.label27.Text = "Cellulaire :";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(6, 87);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(58, 13);
            this.label44.TabIndex = 49;
            this.label44.Text = "Courriel :";
            // 
            // txtCourriel
            // 
            this.txtCourriel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCourriel.Location = new System.Drawing.Point(66, 84);
            this.txtCourriel.Name = "txtCourriel";
            this.txtCourriel.Size = new System.Drawing.Size(103, 20);
            this.txtCourriel.TabIndex = 50;
            // 
            // groupBoxCommande
            // 
            this.groupBoxCommande.Controls.Add(this.label6);
            this.groupBoxCommande.Controls.Add(this.txtPrix);
            this.groupBoxCommande.Controls.Add(this.label93);
            this.groupBoxCommande.Controls.Add(this.txtModèle);
            this.groupBoxCommande.Controls.Add(this.label4);
            this.groupBoxCommande.Controls.Add(this.txtNoTable);
            this.groupBoxCommande.Controls.Add(this.label91);
            this.groupBoxCommande.Controls.Add(this.label71);
            this.groupBoxCommande.Controls.Add(this.txtQuantiteCom);
            this.groupBoxCommande.Controls.Add(this.dateTimePicker1);
            this.groupBoxCommande.Controls.Add(this.txtNoCom);
            this.groupBoxCommande.Controls.Add(this.label70);
            this.groupBoxCommande.Location = new System.Drawing.Point(196, 274);
            this.groupBoxCommande.Name = "groupBoxCommande";
            this.groupBoxCommande.Size = new System.Drawing.Size(333, 118);
            this.groupBoxCommande.TabIndex = 61;
            this.groupBoxCommande.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(223, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 15);
            this.label6.TabIndex = 69;
            this.label6.Text = "Prix :";
            // 
            // txtPrix
            // 
            this.txtPrix.Location = new System.Drawing.Point(266, 41);
            this.txtPrix.Name = "txtPrix";
            this.txtPrix.Size = new System.Drawing.Size(58, 21);
            this.txtPrix.TabIndex = 68;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(94, 45);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(56, 13);
            this.label93.TabIndex = 61;
            this.label93.Text = "Modèle :";
            // 
            // txtModèle
            // 
            this.txtModèle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModèle.Location = new System.Drawing.Point(149, 42);
            this.txtModèle.Name = "txtModèle";
            this.txtModèle.Size = new System.Drawing.Size(73, 20);
            this.txtModèle.TabIndex = 62;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 64;
            this.label4.Text = "Date  :";
            // 
            // txtNoTable
            // 
            this.txtNoTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoTable.Location = new System.Drawing.Point(50, 42);
            this.txtNoTable.Name = "txtNoTable";
            this.txtNoTable.Size = new System.Drawing.Size(41, 20);
            this.txtNoTable.TabIndex = 58;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(3, 45);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(47, 13);
            this.label91.TabIndex = 55;
            this.label91.Text = "Table :";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(197, 14);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(63, 13);
            this.label71.TabIndex = 56;
            this.label71.Text = "Quantité :";
            // 
            // txtQuantiteCom
            // 
            this.txtQuantiteCom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantiteCom.Location = new System.Drawing.Point(266, 11);
            this.txtQuantiteCom.Name = "txtQuantiteCom";
            this.txtQuantiteCom.Size = new System.Drawing.Size(58, 20);
            this.txtQuantiteCom.TabIndex = 57;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Location = new System.Drawing.Point(71, 76);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(253, 21);
            this.dateTimePicker1.TabIndex = 15;
            this.dateTimePicker1.Value = new System.DateTime(2017, 5, 11, 7, 15, 22, 0);
            // 
            // txtNoCom
            // 
            this.txtNoCom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoCom.Location = new System.Drawing.Point(98, 11);
            this.txtNoCom.Name = "txtNoCom";
            this.txtNoCom.Size = new System.Drawing.Size(35, 20);
            this.txtNoCom.TabIndex = 60;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(3, 14);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(96, 13);
            this.label70.TabIndex = 59;
            this.label70.Text = "No Commande :";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(15, 403);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(0, 15);
            this.label68.TabIndex = 20;
            // 
            // groupBox28
            // 
            this.groupBox28.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.groupBox28.Controls.Add(this.txtNoCivique);
            this.groupBox28.Controls.Add(this.label26);
            this.groupBox28.Controls.Add(this.label88);
            this.groupBox28.Controls.Add(this.txtCodePostal);
            this.groupBox28.Controls.Add(this.txtRue);
            this.groupBox28.Controls.Add(this.label48);
            this.groupBox28.Controls.Add(this.txtProvince);
            this.groupBox28.Controls.Add(this.label49);
            this.groupBox28.Controls.Add(this.label51);
            this.groupBox28.Controls.Add(this.txtVille);
            this.groupBox28.Controls.Add(this.txtApp);
            this.groupBox28.Controls.Add(this.label50);
            this.groupBox28.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox28.Location = new System.Drawing.Point(12, 207);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(517, 61);
            this.groupBox28.TabIndex = 19;
            this.groupBox28.TabStop = false;
            // 
            // txtNoCivique
            // 
            this.txtNoCivique.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoCivique.Location = new System.Drawing.Point(93, 12);
            this.txtNoCivique.Name = "txtNoCivique";
            this.txtNoCivique.Size = new System.Drawing.Size(64, 20);
            this.txtNoCivique.TabIndex = 54;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(7, 15);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(76, 13);
            this.label26.TabIndex = 53;
            this.label26.Text = "No civique :";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(158, 15);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(38, 13);
            this.label88.TabIndex = 33;
            this.label88.Text = "Rue :";
            // 
            // txtCodePostal
            // 
            this.txtCodePostal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodePostal.Location = new System.Drawing.Point(409, 35);
            this.txtCodePostal.Name = "txtCodePostal";
            this.txtCodePostal.Size = new System.Drawing.Size(99, 20);
            this.txtCodePostal.TabIndex = 46;
            // 
            // txtRue
            // 
            this.txtRue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRue.Location = new System.Drawing.Point(204, 12);
            this.txtRue.Name = "txtRue";
            this.txtRue.Size = new System.Drawing.Size(113, 20);
            this.txtRue.TabIndex = 35;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(158, 38);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(65, 13);
            this.label48.TabIndex = 45;
            this.label48.Text = "Province :";
            // 
            // txtProvince
            // 
            this.txtProvince.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProvince.Location = new System.Drawing.Point(225, 35);
            this.txtProvince.Name = "txtProvince";
            this.txtProvince.Size = new System.Drawing.Size(92, 20);
            this.txtProvince.TabIndex = 44;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(320, 38);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(83, 13);
            this.label49.TabIndex = 43;
            this.label49.Text = "Code Postal :";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(320, 15);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(106, 13);
            this.label51.TabIndex = 39;
            this.label51.Text = "No Appartement :";
            // 
            // txtVille
            // 
            this.txtVille.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVille.Location = new System.Drawing.Point(56, 35);
            this.txtVille.Name = "txtVille";
            this.txtVille.Size = new System.Drawing.Size(101, 20);
            this.txtVille.TabIndex = 42;
            // 
            // txtApp
            // 
            this.txtApp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApp.Location = new System.Drawing.Point(432, 12);
            this.txtApp.Name = "txtApp";
            this.txtApp.Size = new System.Drawing.Size(76, 20);
            this.txtApp.TabIndex = 40;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(8, 38);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(39, 13);
            this.label50.TabIndex = 41;
            this.label50.Text = "Ville :";
            // 
            // groupBoxSearch
            // 
            this.groupBoxSearch.BackColor = System.Drawing.Color.DodgerBlue;
            this.groupBoxSearch.Controls.Add(this.lblCONN2);
            this.groupBoxSearch.Controls.Add(this.dateTimePickerRECHERCHE);
            this.groupBoxSearch.Controls.Add(this.textBoxSearch);
            this.groupBoxSearch.Controls.Add(this.Search_Button);
            this.groupBoxSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxSearch.Location = new System.Drawing.Point(12, 48);
            this.groupBoxSearch.Name = "groupBoxSearch";
            this.groupBoxSearch.Size = new System.Drawing.Size(399, 58);
            this.groupBoxSearch.TabIndex = 8;
            this.groupBoxSearch.TabStop = false;
            this.groupBoxSearch.Text = "Rechercher";
            // 
            // lblCONN2
            // 
            this.lblCONN2.AutoSize = true;
            this.lblCONN2.Location = new System.Drawing.Point(389, 26);
            this.lblCONN2.Name = "lblCONN2";
            this.lblCONN2.Size = new System.Drawing.Size(0, 13);
            this.lblCONN2.TabIndex = 3;
            // 
            // dateTimePickerRECHERCHE
            // 
            this.dateTimePickerRECHERCHE.Location = new System.Drawing.Point(6, 22);
            this.dateTimePickerRECHERCHE.Name = "dateTimePickerRECHERCHE";
            this.dateTimePickerRECHERCHE.Size = new System.Drawing.Size(226, 20);
            this.dateTimePickerRECHERCHE.TabIndex = 2;
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSearch.Location = new System.Drawing.Point(238, 22);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(103, 20);
            this.textBoxSearch.TabIndex = 1;
            // 
            // Search_Button
            // 
            this.Search_Button.BackColor = System.Drawing.Color.Transparent;
            this.Search_Button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Search_Button.BackgroundImage")));
            this.Search_Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Search_Button.Location = new System.Drawing.Point(347, 18);
            this.Search_Button.Name = "Search_Button";
            this.Search_Button.Size = new System.Drawing.Size(38, 27);
            this.Search_Button.TabIndex = 0;
            this.Search_Button.UseVisualStyleBackColor = false;
            this.Search_Button.Click += new System.EventHandler(this.Search_Button_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(535, 48);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(595, 291);
            this.dataGridView1.TabIndex = 7;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // TabControl
            // 
            this.TabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.TabControl.Controls.Add(this.Home);
            this.TabControl.Controls.Add(this.Atelier);
            this.TabControl.Controls.Add(this.Facture);
            this.TabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabControl.Location = new System.Drawing.Point(13, 51);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(1152, 509);
            this.TabControl.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.TabControl.TabIndex = 0;
            // 
            // Atelier
            // 
            this.Atelier.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.Atelier.Controls.Add(this.groupBox29);
            this.Atelier.Controls.Add(this.butÉvaluations);
            this.Atelier.Controls.Add(this.panel2);
            this.Atelier.Controls.Add(this.lblAFF);
            this.Atelier.Controls.Add(this.groupBox4);
            this.Atelier.Controls.Add(this.groupBox5);
            this.Atelier.Controls.Add(this.butACHAT);
            this.Atelier.Controls.Add(this.butFabrication);
            this.Atelier.Controls.Add(this.butPiece);
            this.Atelier.Controls.Add(this.dataGridViewATELIER);
            this.Atelier.Controls.Add(this.groupBox3);
            this.Atelier.Location = new System.Drawing.Point(4, 27);
            this.Atelier.Name = "Atelier";
            this.Atelier.Padding = new System.Windows.Forms.Padding(3);
            this.Atelier.Size = new System.Drawing.Size(1144, 478);
            this.Atelier.TabIndex = 9;
            this.Atelier.Text = "Atelier";
            // 
            // groupBox29
            // 
            this.groupBox29.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.groupBox29.Controls.Add(this.txtAppréTable);
            this.groupBox29.Controls.Add(this.dateTime);
            this.groupBox29.Controls.Add(this.label9);
            this.groupBox29.Controls.Add(this.txtAppréService);
            this.groupBox29.Controls.Add(this.label8);
            this.groupBox29.Controls.Add(this.txtEvaluation);
            this.groupBox29.Controls.Add(this.label72);
            this.groupBox29.Controls.Add(this.txtAppréGénéral);
            this.groupBox29.Controls.Add(this.txtID_ClientAppré);
            this.groupBox29.Controls.Add(this.label73);
            this.groupBox29.Controls.Add(this.label74);
            this.groupBox29.Controls.Add(this.label75);
            this.groupBox29.Location = new System.Drawing.Point(12, 273);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(399, 165);
            this.groupBox29.TabIndex = 82;
            this.groupBox29.TabStop = false;
            // 
            // txtAppréTable
            // 
            this.txtAppréTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAppréTable.Location = new System.Drawing.Point(142, 102);
            this.txtAppréTable.Name = "txtAppréTable";
            this.txtAppréTable.Size = new System.Drawing.Size(251, 20);
            this.txtAppréTable.TabIndex = 19;
            // 
            // dateTime
            // 
            this.dateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTime.Location = new System.Drawing.Point(142, 44);
            this.dateTime.Name = "dateTime";
            this.dateTime.Size = new System.Drawing.Size(251, 21);
            this.dateTime.TabIndex = 65;
            this.dateTime.Value = new System.DateTime(2017, 5, 11, 7, 15, 22, 0);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Appréciation table :";
            // 
            // txtAppréService
            // 
            this.txtAppréService.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAppréService.Location = new System.Drawing.Point(142, 133);
            this.txtAppréService.Name = "txtAppréService";
            this.txtAppréService.Size = new System.Drawing.Size(251, 20);
            this.txtAppréService.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(5, 136);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(131, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Appréciation service :";
            // 
            // txtEvaluation
            // 
            this.txtEvaluation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEvaluation.Location = new System.Drawing.Point(142, 14);
            this.txtEvaluation.Name = "txtEvaluation";
            this.txtEvaluation.Size = new System.Drawing.Size(58, 20);
            this.txtEvaluation.TabIndex = 14;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(5, 17);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(118, 13);
            this.label72.TabIndex = 13;
            this.label72.Text = "No de l\'évaluation :";
            // 
            // txtAppréGénéral
            // 
            this.txtAppréGénéral.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAppréGénéral.Location = new System.Drawing.Point(142, 73);
            this.txtAppréGénéral.Name = "txtAppréGénéral";
            this.txtAppréGénéral.Size = new System.Drawing.Size(251, 20);
            this.txtAppréGénéral.TabIndex = 4;

            // 
            // txtID_ClientAppré
            // 
            this.txtID_ClientAppré.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID_ClientAppré.Location = new System.Drawing.Point(311, 14);
            this.txtID_ClientAppré.Name = "txtID_ClientAppré";
            this.txtID_ClientAppré.Size = new System.Drawing.Size(82, 20);
            this.txtID_ClientAppré.TabIndex = 3;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(5, 76);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(139, 13);
            this.label73.TabIndex = 2;
            this.label73.Text = "Appréciation générale :";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(5, 45);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(129, 13);
            this.label74.TabIndex = 1;
            this.label74.Text = "Date de l\'évaluation :";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(241, 17);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(64, 13);
            this.label75.TabIndex = 0;
            this.label75.Text = "Client ID :";
            // 
            // butÉvaluations
            // 
            this.butÉvaluations.BackColor = System.Drawing.Color.Silver;
            this.butÉvaluations.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butÉvaluations.Location = new System.Drawing.Point(283, 8);
            this.butÉvaluations.Name = "butÉvaluations";
            this.butÉvaluations.Size = new System.Drawing.Size(81, 31);
            this.butÉvaluations.TabIndex = 81;
            this.butÉvaluations.Text = "Évaluations";
            this.butÉvaluations.UseVisualStyleBackColor = false;
            this.butÉvaluations.Click += new System.EventHandler(this.butÉvaluations_Click_1);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label19);
            this.panel2.ForeColor = System.Drawing.Color.Lime;
            this.panel2.Location = new System.Drawing.Point(930, 347);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 62);
            this.panel2.TabIndex = 80;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(75, 35);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 15);
            this.label16.TabIndex = 8;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(17, 11);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 15);
            this.label17.TabIndex = 3;
            this.label17.Text = "Total  :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(75, 12);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(0, 15);
            this.label18.TabIndex = 7;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(9, 34);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(61, 15);
            this.label19.TabIndex = 4;
            this.label19.Text = "+Taxes :";
            // 
            // lblAFF
            // 
            this.lblAFF.AutoSize = true;
            this.lblAFF.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblAFF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAFF.ForeColor = System.Drawing.Color.Lime;
            this.lblAFF.Location = new System.Drawing.Point(1014, 19);
            this.lblAFF.Name = "lblAFF";
            this.lblAFF.Size = new System.Drawing.Size(44, 16);
            this.lblAFF.TabIndex = 79;
            this.lblAFF.Text = "---------";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.butModifier_1);
            this.groupBox4.Controls.Add(this.butInsert_1);
            this.groupBox4.Controls.Add(this.butEfacer_1);
            this.groupBox4.Location = new System.Drawing.Point(417, 40);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(112, 116);
            this.groupBox4.TabIndex = 78;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Éditer";
            // 
            // butModifier_1
            // 
            this.butModifier_1.BackColor = System.Drawing.Color.LightGreen;
            this.butModifier_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butModifier_1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.butModifier_1.Location = new System.Drawing.Point(7, 46);
            this.butModifier_1.Name = "butModifier_1";
            this.butModifier_1.Size = new System.Drawing.Size(99, 33);
            this.butModifier_1.TabIndex = 12;
            this.butModifier_1.Text = "Modifier";
            this.butModifier_1.UseVisualStyleBackColor = false;
            this.butModifier_1.Click += new System.EventHandler(this.butModifier_1_Click);
            // 
            // butInsert_1
            // 
            this.butInsert_1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.butInsert_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butInsert_1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.butInsert_1.Location = new System.Drawing.Point(7, 13);
            this.butInsert_1.Name = "butInsert_1";
            this.butInsert_1.Size = new System.Drawing.Size(99, 35);
            this.butInsert_1.TabIndex = 10;
            this.butInsert_1.Text = "Insérer";
            this.butInsert_1.UseVisualStyleBackColor = false;
            this.butInsert_1.Click += new System.EventHandler(this.butInsert_1_Click);
            // 
            // butEfacer_1
            // 
            this.butEfacer_1.BackColor = System.Drawing.Color.Red;
            this.butEfacer_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butEfacer_1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.butEfacer_1.Location = new System.Drawing.Point(7, 77);
            this.butEfacer_1.Name = "butEfacer_1";
            this.butEfacer_1.Size = new System.Drawing.Size(99, 34);
            this.butEfacer_1.TabIndex = 11;
            this.butEfacer_1.Text = "Effacer";
            this.butEfacer_1.UseVisualStyleBackColor = false;
            this.butEfacer_1.Click += new System.EventHandler(this.butEfacer_1_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.DodgerBlue;
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.dateTimePicker4);
            this.groupBox5.Controls.Add(this.txtSearchFabrication);
            this.groupBox5.Controls.Add(this.Search_Button2);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(12, 48);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(399, 71);
            this.groupBox5.TabIndex = 77;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Rechercher";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(389, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(0, 13);
            this.label14.TabIndex = 3;
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Location = new System.Drawing.Point(6, 22);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(226, 20);
            this.dateTimePicker4.TabIndex = 2;
            // 
            // txtSearchFabrication
            // 
            this.txtSearchFabrication.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchFabrication.Location = new System.Drawing.Point(238, 22);
            this.txtSearchFabrication.Name = "txtSearchFabrication";
            this.txtSearchFabrication.Size = new System.Drawing.Size(103, 20);
            this.txtSearchFabrication.TabIndex = 1;
            // 
            // Search_Button2
            // 
            this.Search_Button2.BackColor = System.Drawing.Color.Transparent;
            this.Search_Button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Search_Button2.BackgroundImage")));
            this.Search_Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Search_Button2.Location = new System.Drawing.Point(347, 18);
            this.Search_Button2.Name = "Search_Button2";
            this.Search_Button2.Size = new System.Drawing.Size(38, 27);
            this.Search_Button2.TabIndex = 0;
            this.Search_Button2.UseVisualStyleBackColor = false;
            this.Search_Button2.Click += new System.EventHandler(this.Search_Button2_Click);
            // 
            // butACHAT
            // 
            this.butACHAT.BackColor = System.Drawing.Color.Silver;
            this.butACHAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butACHAT.Location = new System.Drawing.Point(211, 8);
            this.butACHAT.Name = "butACHAT";
            this.butACHAT.Size = new System.Drawing.Size(66, 31);
            this.butACHAT.TabIndex = 74;
            this.butACHAT.Text = "Achat";
            this.butACHAT.UseVisualStyleBackColor = false;
            this.butACHAT.Click += new System.EventHandler(this.butACHAT_Click_1);
            // 
            // butFabrication
            // 
            this.butFabrication.BackColor = System.Drawing.Color.Silver;
            this.butFabrication.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butFabrication.Location = new System.Drawing.Point(12, 8);
            this.butFabrication.Name = "butFabrication";
            this.butFabrication.Size = new System.Drawing.Size(132, 31);
            this.butFabrication.TabIndex = 75;
            this.butFabrication.Text = "Tables fabriquées";
            this.butFabrication.UseVisualStyleBackColor = false;
            this.butFabrication.Click += new System.EventHandler(this.butFabrication_Click);
            // 
            // butPiece
            // 
            this.butPiece.BackColor = System.Drawing.Color.Silver;
            this.butPiece.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butPiece.Location = new System.Drawing.Point(150, 8);
            this.butPiece.Name = "butPiece";
            this.butPiece.Size = new System.Drawing.Size(54, 31);
            this.butPiece.TabIndex = 76;
            this.butPiece.Text = "Pièces";
            this.butPiece.UseVisualStyleBackColor = false;
            this.butPiece.Click += new System.EventHandler(this.butPiece_Click);
            // 
            // dataGridViewATELIER
            // 
            this.dataGridViewATELIER.BackgroundColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dataGridViewATELIER.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewATELIER.Location = new System.Drawing.Point(535, 48);
            this.dataGridViewATELIER.Name = "dataGridViewATELIER";
            this.dataGridViewATELIER.Size = new System.Drawing.Size(595, 291);
            this.dataGridViewATELIER.TabIndex = 73;
            this.dataGridViewATELIER.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewATELIER_CellContentClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtValeurPiece);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.txtModele);
            this.groupBox3.Controls.Add(this.txtNoTAB);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.textQUANT);
            this.groupBox3.Controls.Add(this.textNoComFab);
            this.groupBox3.Controls.Add(this.butAfficheAss);
            this.groupBox3.Controls.Add(this.butAfficheVendeur);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.dateTimeFabrication);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(14, 121);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(397, 146);
            this.groupBox3.TabIndex = 72;
            this.groupBox3.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(295, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 15);
            this.label7.TabIndex = 81;
            this.label7.Text = "Coût :";
            // 
            // txtValeurPiece
            // 
            this.txtValeurPiece.Location = new System.Drawing.Point(339, 48);
            this.txtValeurPiece.Name = "txtValeurPiece";
            this.txtValeurPiece.Size = new System.Drawing.Size(53, 21);
            this.txtValeurPiece.TabIndex = 80;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(122, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 78;
            this.label10.Text = "Modèle :";
            // 
            // txtModele
            // 
            this.txtModele.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModele.Location = new System.Drawing.Point(179, 19);
            this.txtModele.Name = "txtModele";
            this.txtModele.Size = new System.Drawing.Size(99, 20);
            this.txtModele.TabIndex = 79;
            // 
            // txtNoTAB
            // 
            this.txtNoTAB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoTAB.Location = new System.Drawing.Point(57, 20);
            this.txtNoTAB.Name = "txtNoTAB";
            this.txtNoTAB.Size = new System.Drawing.Size(53, 20);
            this.txtNoTAB.TabIndex = 75;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 72;
            this.label11.Text = "Table :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(178, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 13);
            this.label12.TabIndex = 73;
            this.label12.Text = "Quantité :";
            // 
            // textQUANT
            // 
            this.textQUANT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textQUANT.Location = new System.Drawing.Point(244, 49);
            this.textQUANT.Name = "textQUANT";
            this.textQUANT.Size = new System.Drawing.Size(48, 20);
            this.textQUANT.TabIndex = 74;
            // 
            // textNoComFab
            // 
            this.textNoComFab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNoComFab.Location = new System.Drawing.Point(123, 50);
            this.textNoComFab.Name = "textNoComFab";
            this.textNoComFab.Size = new System.Drawing.Size(54, 20);
            this.textNoComFab.TabIndex = 77;
            // 
            // butAfficheAss
            // 
            this.butAfficheAss.BackColor = System.Drawing.Color.Gray;
            this.butAfficheAss.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.butAfficheAss.Location = new System.Drawing.Point(4, 108);
            this.butAfficheAss.Name = "butAfficheAss";
            this.butAfficheAss.Size = new System.Drawing.Size(111, 31);
            this.butAfficheAss.TabIndex = 70;
            this.butAfficheAss.Text = "Assembleur(se)";
            this.butAfficheAss.UseVisualStyleBackColor = false;
            // 
            // butAfficheVendeur
            // 
            this.butAfficheVendeur.BackColor = System.Drawing.Color.Gray;
            this.butAfficheVendeur.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butAfficheVendeur.Location = new System.Drawing.Point(122, 108);
            this.butAfficheVendeur.Name = "butAfficheVendeur";
            this.butAfficheVendeur.Size = new System.Drawing.Size(110, 31);
            this.butAfficheVendeur.TabIndex = 71;
            this.butAfficheVendeur.Text = "Vendeur(se)";
            this.butAfficheVendeur.UseVisualStyleBackColor = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 13);
            this.label13.TabIndex = 76;
            this.label13.Text = "No Commande :";
            // 
            // dateTimeFabrication
            // 
            this.dateTimeFabrication.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeFabrication.Location = new System.Drawing.Point(124, 78);
            this.dateTimeFabrication.Name = "dateTimeFabrication";
            this.dateTimeFabrication.Size = new System.Drawing.Size(252, 21);
            this.dateTimeFabrication.TabIndex = 68;
            this.dateTimeFabrication.Value = new System.DateTime(2017, 5, 11, 7, 15, 22, 0);
            this.dateTimeFabrication.ValueChanged += new System.EventHandler(this.dateTimePickerFabrication_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(4, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 13);
            this.label5.TabIndex = 69;
            this.label5.Text = "Date fabrication :";
            // 
            // Facture
            // 
            this.Facture.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Facture.Location = new System.Drawing.Point(4, 27);
            this.Facture.Name = "Facture";
            this.Facture.Padding = new System.Windows.Forms.Padding(3);
            this.Facture.Size = new System.Drawing.Size(1144, 573);
            this.Facture.TabIndex = 8;
            this.Facture.Text = "Facture";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1177, 594);
            this.Controls.Add(this.lblConnected);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TabControl);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Home.ResumeLayout(false);
            this.Home.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBoxClient.ResumeLayout(false);
            this.groupBoxClient.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox32.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBoxCommande.ResumeLayout(false);
            this.groupBoxCommande.PerformLayout();
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBoxSearch.ResumeLayout(false);
            this.groupBoxSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.TabControl.ResumeLayout(false);
            this.Atelier.ResumeLayout(false);
            this.Atelier.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewATELIER)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblConnected;
        private System.Windows.Forms.TabPage Home;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.Button butModifier;
        private System.Windows.Forms.Button butEffacer;
        private System.Windows.Forms.Button butInserer;
        private System.Windows.Forms.GroupBox groupBoxSearch;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.Button Search_Button;
        private System.Windows.Forms.Button butEmployés;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.TextBox txtNoCivique;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtCell;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtPrenom;
        private System.Windows.Forms.TextBox txtCourriel;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.TextBox txtTelephone;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.TextBox txtCodePostal;
        private System.Windows.Forms.TextBox txtRue;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.TextBox txtProvince;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox txtVille;
        private System.Windows.Forms.TextBox txtApp;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox txtNoCom;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox txtNoTable;
        private System.Windows.Forms.TextBox txtQuantiteCom;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Button butClients;
        private System.Windows.Forms.Button butFournisseurs;
        private System.Windows.Forms.Button butTable;
        private System.Windows.Forms.Button butCommandes;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.TextBox txtFax;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.GroupBox groupBoxCommande;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox txtModèle;
        private System.Windows.Forms.Button butEntreprise;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label lblSumTaxe;
        private System.Windows.Forms.Label lblSum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage Facture;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateTimePickerRECHERCHE;
        private System.Windows.Forms.Label lblCONN2;
        private System.Windows.Forms.GroupBox groupBoxClient;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtNom_Entreprise;
        private System.Windows.Forms.TextBox txtID_Entreprise;
        private System.Windows.Forms.TextBox txtPrenom_Contact;
        private System.Windows.Forms.TextBox txtNom_Contact;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblCONN;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPrix;
        private System.Windows.Forms.TabPage Atelier;
        private System.Windows.Forms.Button butAfficheAss;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button butAfficheVendeur;
        private System.Windows.Forms.DateTimePicker dateTimeFabrication;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button butModifier_1;
        private System.Windows.Forms.Button butInsert_1;
        private System.Windows.Forms.Button butEfacer_1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.TextBox txtSearchFabrication;
        private System.Windows.Forms.Button Search_Button2;
        private System.Windows.Forms.Button butACHAT;
        private System.Windows.Forms.Button butFabrication;
        private System.Windows.Forms.Button butPiece;
        private System.Windows.Forms.DataGridView dataGridViewATELIER;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtValeurPiece;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtModele;
        private System.Windows.Forms.TextBox txtNoTAB;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textQUANT;
        private System.Windows.Forms.TextBox textNoComFab;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblAFF;
        private System.Windows.Forms.Button butVente;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.TextBox txtAppréTable;
        private System.Windows.Forms.DateTimePicker dateTime;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAppréService;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtEvaluation;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox txtAppréGénéral;
        private System.Windows.Forms.TextBox txtID_ClientAppré;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Button butÉvaluations;
    }
}

