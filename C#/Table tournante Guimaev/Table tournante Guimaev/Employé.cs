﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Table_tournante_Guimaev
{
    public class Employé
    {

        private string app;
        private string prenom;
        private string id;
        private string nom;
        private int numero_civique;
        private string rue;
        private string ville;
        private string province;
        private string code_postal;
        private string telephone;
        private string courriel;
        private string cellulaire;

        public static int cnt;

        public Employé()
        { }

        public Employé(string id, string nom, string prenom,int numero_civique, string rue, string app, string ville, string province, string code_postal, string telephone, string cellulaire, string courriel) 
        {
            this.id = id;
            this.nom = nom;
            this.numero_civique = numero_civique;
            this.rue = rue;
            this.ville = ville;
            this.province = province;
            this.code_postal = code_postal;
            this.telephone = telephone;
            this.courriel = courriel;
            this.cellulaire = cellulaire;
            this.prenom = prenom;
            this.app = app;
            cnt++;

        }



        public string ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public string NOM
        {
            get { return this.nom; }
            set { this.nom = value; }

        }

        public  string PRENOM
        {
            get { return this.prenom ; }
            set { this.prenom = value; }

        }

        public int NUMERO_CIVIQUE
        {
            get { return this.numero_civique; }
            set { this.numero_civique = value; }
        }

        public string RUE
        {
            get { return this.rue; }
            set { this.rue = value; }
        }

        public string PROVINCE
        {
            get { return this.province; }
            set { this.province = value; }
        }

        public string CODE_POSTAL
        {
            get { return this.code_postal; }
            set { this.code_postal = value; }
        }

        public string APP
        {
            get { return this.app ; }
            set { this.app = value; }
        }

        public string VILLE
        {
            get { return this.ville; }
            set { this.ville = value; }
        }

        public string TELEPHONE
        {
            get { return this.telephone; }
            set { this.telephone = value; }
        }

        public string CELLULAIRE
        {
            get { return this.cellulaire; }
            set { this.cellulaire = value; }
        }

        public string COURRIEL
        {
            get { return this.courriel; }
            set { this.courriel = value; }
        }

    }
}