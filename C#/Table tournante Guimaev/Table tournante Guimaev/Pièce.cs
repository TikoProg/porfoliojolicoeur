﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Table_tournante_Guimaev
{
    public class Pièce
    {

        private int no;
        private string modèle;
        private string description;
        private double prix;
        public static int cnt;    

        public Pièce()
        { }

        public Pièce(int no, string modèle, string description, double prix) 
        {
            this.no = no;
            this.modèle = modèle;
            this.description = description;
            this.prix = prix;
            cnt++;
        }

        public int NO
        {
            get { return this.no ; }
            set { this.no = value; }
        }

        public string MOD
        {
            get { return this.modèle ; }
            set { this.modèle = value; }
        }

        public string DESC
        {
            get { return this.description ; }
            set { this.description = value; }
        }

        public double PRIX
        {
            get { return this.prix ; }
            set { this.prix = value; }
        }


    }
}