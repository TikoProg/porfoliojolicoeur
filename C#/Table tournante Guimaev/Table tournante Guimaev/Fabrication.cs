﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Table_tournante_Guimaev
{
    public class Fabrication
    {
        private int no_fabrication;
        private int no_table;
        private int no_commande;
        private string id_employé;
        private DateTime date_fabrication;
        private int no_pièce;
        private double valeur;

        public static int cnt;

        public Fabrication()
        { }

        public Fabrication(int no_fabrication, int no_commande, string id_employé,  DateTime date_fabrication, int no_table, int no_pièce, double valeur)
        {
            this.no_fabrication = no_fabrication;
            this.no_table = no_table;
            this.id_employé = id_employé;
            this.no_commande = no_commande;
            this.date_fabrication = date_fabrication;
            this.no_pièce = no_pièce;
            this.valeur = valeur;
            cnt++;

        }

        public int NO_FABRICATION
        {
            get { return this.no_fabrication; }
            set { this.no_fabrication = value; }
        }


        public string ID_EMPLOYÉ
        {
            get { return this.id_employé; }
            set { this.id_employé = value; }
        }

        public DateTime DATE
        {
            get { return this.date_fabrication; }
            set { this.date_fabrication = value; }
        }

        public int NO_COMMANDE
        {
            get { return this.no_commande; }
            set { this.no_commande = value; }
        }

        public int NO_TABLE
        {
            get { return this.no_table; }
            set { this.no_table = value; }
        }


        public int NO_PIÈCE
        {
            get { return this.no_pièce; }
            set { this.no_pièce = value; }
        }

        public double VALEUR
        {
            get { return this.valeur; }
            set { this.valeur = value; }
        }
    }
}