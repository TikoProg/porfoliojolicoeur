﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Table_tournante_Guimaev
{
    public class Fournisseur
    {
        protected string id;
        protected string nom;
        protected int numero_civique;
        protected string rue;
        protected string ville;
        protected string province;
        protected string code_postal;
        protected string telephone;
        protected string courriel;

        public static int cnt;

        public Fournisseur()
        {
            

        }

        public Fournisseur(string id, string nom, int numero_civique, string rue, string ville, string province, string code_postal, string telephone, string courriel)
        {
            this.id = id;
            this.nom = nom;
            this.numero_civique = numero_civique;
            this.rue = rue;
            this.ville = ville;
            this.province = province;
            this.code_postal = code_postal;
            this.telephone = telephone;
            this.courriel = courriel;
            cnt++;
        }

        public  string ID
        {
            get { return this.id ; }
            set { this.id = value; }
        }

        public  string NOM
        {
            get { return this.nom ; }
            set { this.nom = value; }

        }

      
        public  int NUMERO_CIVIQUE
        {
            get { return this.numero_civique ; }
            set { this.numero_civique = value; }
        }

        public  string RUE
        {
            get { return this.rue ; }
            set { this.rue = value; }
        }

        public  string VILLE
        {
            get { return this.ville ; }
            set { this.ville = value; }
        }

        public  string PROVINCE
        {
            get { return this.province ; }
            set { this.province = value; }
        }

        public  string CODE_POSTAL
        {
            get { return this.code_postal ; }
            set { this.code_postal = value; }
        }

        public  string TELEPHONE
        {
            get { return this.telephone ; }
            set { this.telephone = value; }
        }

       
        public string COURRIEL
        {
            get { return this.courriel ; }
            set { this.courriel = value; }
        }
    }
}