﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Table_tournante_Guimaev
{
    public class Achat_Pièce
    {
        private int no_achat;
        private int no_pièce;
        private string id_fournisseur;
        private DateTime date_achat;
        private int quantité;
        private double valeur;

        public static int cnt;

        public Achat_Pièce()
        { }

        public Achat_Pièce(int no_achat, int no_pièce, string id_fournisseur, DateTime date_achat, int quantité, double valeur)
        {
            this.no_achat = no_achat;
            this.no_pièce = no_pièce;
            this.id_fournisseur = id_fournisseur;
            this.date_achat = date_achat;
            this.quantité = quantité;
            this.valeur = valeur;
            cnt++;
        }

        public int NO_ACHAT
        {
            get { return this.no_achat; }
            set { this.no_achat = value; }
        }

        public int NO_PIÈCE
        {
            get { return this.no_pièce; }
            set { this.no_pièce = value; }
        }

        public string FOURNISSEUR
        {
            get { return this.id_fournisseur; }
            set { this.id_fournisseur = value; }
        }

        public DateTime DATE_ACHAT
        {
            get { return this.date_achat; }
            set { this.date_achat = value; }
        }

        public int QUANTITÉ
        {
            get { return this.quantité; }
            set { this.quantité = value; }
        }

        public double VALEUR
        {
            get { return this.valeur; }
            set { this.valeur = value; }
        }

    }
}