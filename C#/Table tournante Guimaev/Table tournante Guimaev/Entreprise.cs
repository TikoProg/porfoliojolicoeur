﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Table_tournante_Guimaev
{
    public class Entreprise 
    {
        private string prenom_contact;
        private string nom_entreprise;
        private string id;
        private string nom_contact;
        private int numero_civique;
        private string rue;
        private string ville;
        private string province;
        private string code_postal;
        private string telephone;
        private string courriel;
        string fax;
        string cellulaire_contact;

        public static int cnt;

        public Entreprise()
        { }

        public Entreprise( string id, string nom_entreprise, string nom_contact, string prenom_contact, int numero_civique, string rue, string ville, string province, string code_postal, string telephone, string cellulaire, string fax, string courriel) 
        {
            this.cellulaire_contact = cellulaire;
            this.nom_contact = nom_contact;
            this.prenom_contact = prenom_contact;
            this.id = id;
            this.numero_civique = numero_civique;
            this.rue = rue;
            this.ville = ville;
            this.province = province;
            this.code_postal = code_postal;
            this.telephone = telephone;
            this.courriel = courriel;
            this.nom_entreprise = nom_entreprise;
            this.fax = fax;
            cnt++;
        }

        public string ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public string NOM_ENTREPRISE
        {
            get { return this.nom_entreprise ; }
            set { this.nom_entreprise = value; }
        }


        public string NOM_CONTACT
        {
            get { return this.nom_contact; }
            set { this.nom_contact = value; }

        }

        public string PRENOM_CONTACT
        {
            get { return this.prenom_contact; }
            set { this.prenom_contact = value; }

        }

        public int NUMERO_CIVIQUE
        {
            get { return this.numero_civique; }
            set { this.numero_civique = value; }
        }

        public string RUE
        {
            get { return this.rue; }
            set { this.rue = value; }
        }

        public string VILLE
        {
            get { return this.ville; }
            set { this.ville = value; }
        }

        public string PROVINCE
        {
            get { return this.province; }
            set { this.province = value; }
        }

        public string CODE_POSTAL
        {
            get { return this.code_postal; }
            set { this.code_postal = value; }
        }

        public string TELEPHONE
        {
            get { return this.telephone; }
            set { this.telephone = value; }
        }

        public string CELLULAIRE_CONTACT
        {
            get { return this.cellulaire_contact; }
            set { this.cellulaire_contact = value; }
        }



        public string FAX
        {
            get { return this.fax ; }
            set { this.fax = value; }
        }

        public string COURRIEL
        {
            get { return this.courriel; }
            set { this.courriel = value; }
        }


    }

    
}