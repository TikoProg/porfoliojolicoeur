﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Table_tournante_Guimaev
{
    public class Commande
    {
        private int no_commande;
        private int no_table;
        private DateTime date_commande;
        private string type;
        private string id_client;
        private string id_entreprise;
        private int quantité;

        public static int cnt;

        public Commande()
        {
            
        }

        public Commande(int no_commande, string id_client, string id_entreprise,  DateTime date_commande, int no_table, string type, int quantité)
        {
            this.no_table = no_table;
            this.no_commande = no_commande;
            this.id_client = id_client;
            this.id_entreprise = id_entreprise;
            this.type = type;
            this.date_commande = date_commande;
            this.quantité = quantité;
            cnt++;
        }

        public int NO
        {
            get { return this.no_commande ; }
            set { this.no_commande = value; }
        }


        public string ID_CLIENT
        {
            get { return this.id_client ; }
            set { this.id_client = value; }
        }

        public string ID_ENTREPRISE
        {
            get { return this.id_entreprise ; }
            set { this.id_entreprise = value; }
        }

        public DateTime DATE_COMMANDE
        {
            get { return this.date_commande ; }
            set { this.date_commande = value; }
        }

      
        public int NO_TABLE
        {
            get { return this.no_table ; }
            set { this.no_table = value; }
        }


        public string TYPE
        {
            get { return this.type ; }
            set { this.type = value; }
        }

        public int QUANTITE
        {
            get { return this.quantité ; }
            set { this.quantité = value; }
        }

       
    }
}