﻿namespace FichierTexte_Guimaev_Jolicoeur
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.butInsert = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.BoxIDsearch = new System.Windows.Forms.TextBox();
            this.butSearch = new System.Windows.Forms.Button();
            this.label_longuer_text = new System.Windows.Forms.Label();
            this.label_longeur_nom = new System.Windows.Forms.Label();
            this.label_nombre_followers = new System.Windows.Forms.Label();
            this.label_mombre_followings = new System.Windows.Forms.Label();
            this.label_date_extraction = new System.Windows.Forms.Label();
            this.label_date_compte = new System.Windows.Forms.Label();
            this.label_ID = new System.Windows.Forms.Label();
            this.BoxID = new System.Windows.Forms.TextBox();
            this.BoxCreation = new System.Windows.Forms.TextBox();
            this.BoxExtrac = new System.Windows.Forms.TextBox();
            this.BoxFalwing = new System.Windows.Forms.TextBox();
            this.BoxFalwers = new System.Windows.Forms.TextBox();
            this.BoxLongNom = new System.Windows.Forms.TextBox();
            this.BoxLongProfil = new System.Windows.Forms.TextBox();
            this.butLoad = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.loaded_file = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ex_txt = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.butUpdate = new System.Windows.Forms.Button();
            this.butAdd = new System.Windows.Forms.Button();
            this.ListeUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dATECREATIONDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dATEEXTRACDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nBFOLLOWINGSDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nBFOLLOWERSDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lONGNOMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lONGTEXTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ListeUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // butInsert
            // 
            this.butInsert.Font = new System.Drawing.Font("Elephant", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butInsert.Location = new System.Drawing.Point(560, 295);
            this.butInsert.Name = "butInsert";
            this.butInsert.Size = new System.Drawing.Size(115, 23);
            this.butInsert.TabIndex = 1;
            this.butInsert.Text = "Export .xml";
            this.butInsert.UseVisualStyleBackColor = true;
            this.butInsert.Click += new System.EventHandler(this.butExportxml_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label1.Font = new System.Drawing.Font("Elephant", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 297);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 21);
            this.label1.TabIndex = 4;
            this.label1.Text = "User ID";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.dATECREATIONDataGridViewTextBoxColumn,
            this.dATEEXTRACDataGridViewTextBoxColumn,
            this.nBFOLLOWINGSDataGridViewTextBoxColumn,
            this.nBFOLLOWERSDataGridViewTextBoxColumn,
            this.lONGNOMDataGridViewTextBoxColumn,
            this.lONGTEXTDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.ListeUserBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(52, 99);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(743, 176);
            this.dataGridView1.TabIndex = 5;
            // 
            // BoxIDsearch
            // 
            this.BoxIDsearch.Location = new System.Drawing.Point(125, 297);
            this.BoxIDsearch.Name = "BoxIDsearch";
            this.BoxIDsearch.Size = new System.Drawing.Size(122, 20);
            this.BoxIDsearch.TabIndex = 6;
            // 
            // butSearch
            // 
            this.butSearch.BackColor = System.Drawing.Color.Transparent;
            this.butSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("butSearch.BackgroundImage")));
            this.butSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.butSearch.Location = new System.Drawing.Point(252, 294);
            this.butSearch.Name = "butSearch";
            this.butSearch.Size = new System.Drawing.Size(30, 25);
            this.butSearch.TabIndex = 8;
            this.butSearch.UseVisualStyleBackColor = false;
            this.butSearch.Click += new System.EventHandler(this.butSearch_Click);
            // 
            // label_longuer_text
            // 
            this.label_longuer_text.AutoSize = true;
            this.label_longuer_text.Font = new System.Drawing.Font("Elephant", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_longuer_text.Location = new System.Drawing.Point(819, 258);
            this.label_longuer_text.Name = "label_longuer_text";
            this.label_longuer_text.Size = new System.Drawing.Size(136, 17);
            this.label_longuer_text.TabIndex = 41;
            this.label_longuer_text.Text = "Longeur Text Profil";
            // 
            // label_longeur_nom
            // 
            this.label_longeur_nom.AutoSize = true;
            this.label_longeur_nom.Font = new System.Drawing.Font("Elephant", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_longeur_nom.Location = new System.Drawing.Point(819, 232);
            this.label_longeur_nom.Name = "label_longeur_nom";
            this.label_longeur_nom.Size = new System.Drawing.Size(97, 17);
            this.label_longeur_nom.TabIndex = 40;
            this.label_longeur_nom.Text = "Longeur Nom";
            // 
            // label_nombre_followers
            // 
            this.label_nombre_followers.AutoSize = true;
            this.label_nombre_followers.Font = new System.Drawing.Font("Elephant", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_nombre_followers.Location = new System.Drawing.Point(819, 206);
            this.label_nombre_followers.Name = "label_nombre_followers";
            this.label_nombre_followers.Size = new System.Drawing.Size(122, 17);
            this.label_nombre_followers.TabIndex = 39;
            this.label_nombre_followers.Text = "Nombre followers";
            // 
            // label_mombre_followings
            // 
            this.label_mombre_followings.AutoSize = true;
            this.label_mombre_followings.Font = new System.Drawing.Font("Elephant", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_mombre_followings.Location = new System.Drawing.Point(819, 180);
            this.label_mombre_followings.Name = "label_mombre_followings";
            this.label_mombre_followings.Size = new System.Drawing.Size(127, 17);
            this.label_mombre_followings.TabIndex = 38;
            this.label_mombre_followings.Text = "Nombre followings";
            // 
            // label_date_extraction
            // 
            this.label_date_extraction.AutoSize = true;
            this.label_date_extraction.Font = new System.Drawing.Font("Elephant", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_date_extraction.Location = new System.Drawing.Point(819, 154);
            this.label_date_extraction.Name = "label_date_extraction";
            this.label_date_extraction.Size = new System.Drawing.Size(117, 17);
            this.label_date_extraction.TabIndex = 37;
            this.label_date_extraction.Text = "Date d\'extraction";
            // 
            // label_date_compte
            // 
            this.label_date_compte.AutoSize = true;
            this.label_date_compte.Font = new System.Drawing.Font("Elephant", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_date_compte.Location = new System.Drawing.Point(819, 128);
            this.label_date_compte.Name = "label_date_compte";
            this.label_date_compte.Size = new System.Drawing.Size(183, 17);
            this.label_date_compte.TabIndex = 36;
            this.label_date_compte.Text = "Date de creation du compte ";
            // 
            // label_ID
            // 
            this.label_ID.AutoSize = true;
            this.label_ID.Font = new System.Drawing.Font("Elephant", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ID.Location = new System.Drawing.Point(819, 102);
            this.label_ID.Name = "label_ID";
            this.label_ID.Size = new System.Drawing.Size(61, 17);
            this.label_ID.TabIndex = 35;
            this.label_ID.Text = "User ID";
            // 
            // BoxID
            // 
            this.BoxID.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ListeUserBindingSource, "ID", true));
            this.BoxID.Location = new System.Drawing.Point(1007, 99);
            this.BoxID.Name = "BoxID";
            this.BoxID.Size = new System.Drawing.Size(153, 20);
            this.BoxID.TabIndex = 42;
            this.BoxID.Text = "0";
            // 
            // BoxCreation
            // 
            this.BoxCreation.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ListeUserBindingSource, "DATECREATION", true));
            this.BoxCreation.Location = new System.Drawing.Point(1007, 125);
            this.BoxCreation.Name = "BoxCreation";
            this.BoxCreation.Size = new System.Drawing.Size(153, 20);
            this.BoxCreation.TabIndex = 43;
            this.BoxCreation.Text = "yyyy-MM-dd hh:MM:ss";
            // 
            // BoxExtrac
            // 
            this.BoxExtrac.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ListeUserBindingSource, "DATEEXTRAC", true));
            this.BoxExtrac.Location = new System.Drawing.Point(1007, 151);
            this.BoxExtrac.Name = "BoxExtrac";
            this.BoxExtrac.Size = new System.Drawing.Size(153, 20);
            this.BoxExtrac.TabIndex = 44;
            this.BoxExtrac.Text = "yyyy-MM-dd hh:MM:ss";
            // 
            // BoxFalwing
            // 
            this.BoxFalwing.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ListeUserBindingSource, "NBFOLLOWINGS", true));
            this.BoxFalwing.Location = new System.Drawing.Point(1007, 177);
            this.BoxFalwing.Name = "BoxFalwing";
            this.BoxFalwing.Size = new System.Drawing.Size(153, 20);
            this.BoxFalwing.TabIndex = 45;
            this.BoxFalwing.Text = "0";
            // 
            // BoxFalwers
            // 
            this.BoxFalwers.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ListeUserBindingSource, "NBFOLLOWERS", true));
            this.BoxFalwers.Location = new System.Drawing.Point(1007, 203);
            this.BoxFalwers.Name = "BoxFalwers";
            this.BoxFalwers.Size = new System.Drawing.Size(153, 20);
            this.BoxFalwers.TabIndex = 46;
            this.BoxFalwers.Text = "0";
            // 
            // BoxLongNom
            // 
            this.BoxLongNom.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ListeUserBindingSource, "LONGNOM", true));
            this.BoxLongNom.Location = new System.Drawing.Point(1007, 229);
            this.BoxLongNom.Name = "BoxLongNom";
            this.BoxLongNom.Size = new System.Drawing.Size(153, 20);
            this.BoxLongNom.TabIndex = 47;
            this.BoxLongNom.Text = "0";
            // 
            // BoxLongProfil
            // 
            this.BoxLongProfil.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ListeUserBindingSource, "LONGTEXT", true));
            this.BoxLongProfil.Location = new System.Drawing.Point(1007, 255);
            this.BoxLongProfil.Name = "BoxLongProfil";
            this.BoxLongProfil.Size = new System.Drawing.Size(153, 20);
            this.BoxLongProfil.TabIndex = 48;
            this.BoxLongProfil.Text = "0";
            // 
            // butLoad
            // 
            this.butLoad.Font = new System.Drawing.Font("Elephant", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butLoad.Location = new System.Drawing.Point(52, 12);
            this.butLoad.Name = "butLoad";
            this.butLoad.Size = new System.Drawing.Size(108, 23);
            this.butLoad.TabIndex = 49;
            this.butLoad.Text = "Load File ";
            this.butLoad.UseVisualStyleBackColor = true;
            this.butLoad.Click += new System.EventHandler(this.butLoad_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Elephant", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(53, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 16);
            this.label2.TabIndex = 50;
            this.label2.Text = "File :";
            // 
            // loaded_file
            // 
            this.loaded_file.AutoSize = true;
            this.loaded_file.Location = new System.Drawing.Point(95, 55);
            this.loaded_file.Name = "loaded_file";
            this.loaded_file.Size = new System.Drawing.Size(0, 13);
            this.loaded_file.TabIndex = 51;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Elephant", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(53, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 16);
            this.label3.TabIndex = 52;
            this.label3.Text = "Extension :";
            // 
            // ex_txt
            // 
            this.ex_txt.AutoSize = true;
            this.ex_txt.Location = new System.Drawing.Point(135, 68);
            this.ex_txt.Name = "ex_txt";
            this.ex_txt.Size = new System.Drawing.Size(0, 13);
            this.ex_txt.TabIndex = 53;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Elephant", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(681, 295);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(115, 23);
            this.button1.TabIndex = 54;
            this.button1.Text = "Export .txt";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.butExportTxt_Click);
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bindingNavigator1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bindingNavigator1.BindingSource = this.ListeUserBindingSource;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigator1.Dock = System.Windows.Forms.DockStyle.None;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem});
            this.bindingNavigator1.Location = new System.Drawing.Point(52, 336);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(257, 25);
            this.bindingNavigator1.Stretch = true;
            this.bindingNavigator1.TabIndex = 55;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Ajouter nouveau";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(37, 22);
            this.bindingNavigatorCountItem.Text = "de {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Nombre total d\'éléments";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Supprimer";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Placer en premier";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Déplacer vers le haut";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Position actuelle";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Déplacer vers le bas";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Placer en dernier";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // butUpdate
            // 
            this.butUpdate.Location = new System.Drawing.Point(1007, 293);
            this.butUpdate.Name = "butUpdate";
            this.butUpdate.Size = new System.Drawing.Size(75, 23);
            this.butUpdate.TabIndex = 56;
            this.butUpdate.Text = "Update User";
            this.butUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butUpdate.UseVisualStyleBackColor = true;
            this.butUpdate.Click += new System.EventHandler(this.butUpdate_Click);
            // 
            // butAdd
            // 
            this.butAdd.Location = new System.Drawing.Point(1088, 293);
            this.butAdd.Name = "butAdd";
            this.butAdd.Size = new System.Drawing.Size(72, 23);
            this.butAdd.TabIndex = 57;
            this.butAdd.Text = "Add User";
            this.butAdd.UseVisualStyleBackColor = true;
            this.butAdd.Click += new System.EventHandler(this.butADD_Click);
            // 
            // ListeUserBindingSource
            // 
            this.ListeUserBindingSource.AllowNew = false;
            this.ListeUserBindingSource.DataSource = typeof(User);
            this.ListeUserBindingSource.CurrentChanged += new System.EventHandler(this.ListeUserBindingSource_CurrentChanged);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            // 
            // dATECREATIONDataGridViewTextBoxColumn
            // 
            this.dATECREATIONDataGridViewTextBoxColumn.DataPropertyName = "DATECREATION";
            this.dATECREATIONDataGridViewTextBoxColumn.HeaderText = "DATECREATION";
            this.dATECREATIONDataGridViewTextBoxColumn.Name = "dATECREATIONDataGridViewTextBoxColumn";
            // 
            // dATEEXTRACDataGridViewTextBoxColumn
            // 
            this.dATEEXTRACDataGridViewTextBoxColumn.DataPropertyName = "DATEEXTRAC";
            this.dATEEXTRACDataGridViewTextBoxColumn.HeaderText = "DATEEXTRAC";
            this.dATEEXTRACDataGridViewTextBoxColumn.Name = "dATEEXTRACDataGridViewTextBoxColumn";
            // 
            // nBFOLLOWINGSDataGridViewTextBoxColumn
            // 
            this.nBFOLLOWINGSDataGridViewTextBoxColumn.DataPropertyName = "NBFOLLOWINGS";
            this.nBFOLLOWINGSDataGridViewTextBoxColumn.HeaderText = "NBFOLLOWINGS";
            this.nBFOLLOWINGSDataGridViewTextBoxColumn.Name = "nBFOLLOWINGSDataGridViewTextBoxColumn";
            // 
            // nBFOLLOWERSDataGridViewTextBoxColumn
            // 
            this.nBFOLLOWERSDataGridViewTextBoxColumn.DataPropertyName = "NBFOLLOWERS";
            this.nBFOLLOWERSDataGridViewTextBoxColumn.HeaderText = "NBFOLLOWERS";
            this.nBFOLLOWERSDataGridViewTextBoxColumn.Name = "nBFOLLOWERSDataGridViewTextBoxColumn";
            // 
            // lONGNOMDataGridViewTextBoxColumn
            // 
            this.lONGNOMDataGridViewTextBoxColumn.DataPropertyName = "LONGNOM";
            this.lONGNOMDataGridViewTextBoxColumn.HeaderText = "LONGNOM";
            this.lONGNOMDataGridViewTextBoxColumn.Name = "lONGNOMDataGridViewTextBoxColumn";
            // 
            // lONGTEXTDataGridViewTextBoxColumn
            // 
            this.lONGTEXTDataGridViewTextBoxColumn.DataPropertyName = "LONGTEXT";
            this.lONGTEXTDataGridViewTextBoxColumn.HeaderText = "LONGTEXT";
            this.lONGTEXTDataGridViewTextBoxColumn.Name = "lONGTEXTDataGridViewTextBoxColumn";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1172, 387);
            this.Controls.Add(this.butAdd);
            this.Controls.Add(this.butUpdate);
            this.Controls.Add(this.bindingNavigator1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ex_txt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.loaded_file);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.butLoad);
            this.Controls.Add(this.BoxLongProfil);
            this.Controls.Add(this.BoxLongNom);
            this.Controls.Add(this.BoxFalwers);
            this.Controls.Add(this.BoxFalwing);
            this.Controls.Add(this.BoxExtrac);
            this.Controls.Add(this.BoxCreation);
            this.Controls.Add(this.BoxID);
            this.Controls.Add(this.label_longuer_text);
            this.Controls.Add(this.label_longeur_nom);
            this.Controls.Add(this.label_nombre_followers);
            this.Controls.Add(this.label_mombre_followings);
            this.Controls.Add(this.label_date_extraction);
            this.Controls.Add(this.label_date_compte);
            this.Controls.Add(this.label_ID);
            this.Controls.Add(this.butSearch);
            this.Controls.Add(this.BoxIDsearch);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.butInsert);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ListeUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button butInsert;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox BoxIDsearch;
        private System.Windows.Forms.Button butSearch;
        private System.Windows.Forms.Label label_longuer_text;
        private System.Windows.Forms.Label label_longeur_nom;
        private System.Windows.Forms.Label label_nombre_followers;
        private System.Windows.Forms.Label label_mombre_followings;
        private System.Windows.Forms.Label label_date_extraction;
        private System.Windows.Forms.Label label_date_compte;
        private System.Windows.Forms.Label label_ID;
        private System.Windows.Forms.TextBox BoxID;
        private System.Windows.Forms.TextBox BoxCreation;
        private System.Windows.Forms.TextBox BoxExtrac;
        private System.Windows.Forms.TextBox BoxFalwing;
        private System.Windows.Forms.TextBox BoxFalwers;
        private System.Windows.Forms.TextBox BoxLongNom;
        private System.Windows.Forms.TextBox BoxLongProfil;
        private System.Windows.Forms.Button butLoad;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label loaded_file;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label ex_txt;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource ListeUserBindingSource;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.Button butUpdate;
        private System.Windows.Forms.Button butAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dATECREATIONDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dATEEXTRACDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nBFOLLOWINGSDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nBFOLLOWERSDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lONGNOMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lONGTEXTDataGridViewTextBoxColumn;
    }
}

