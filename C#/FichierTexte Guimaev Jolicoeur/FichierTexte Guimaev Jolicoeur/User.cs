﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FichierTexte_Guimaev_Jolicoeur
{
    public class User
    {
        public int id;
        public string dateCreation;
        public string dateExtrac;
        public int nbFalwing;
        public int nbFalwers;
        public int longNom;
        public int longTexte;

        public static int cnt;

      
        public User(int id, string dateCreation, string dateExtrac, int nbFalwing, int nbFalwers, int longNom, int longTexte)
        {
            this.id = id;
            this.dateCreation = dateCreation;
            this.dateExtrac = dateExtrac;
            this.nbFalwers = nbFalwers;
            this.nbFalwing = nbFalwing;
            this.longNom = longNom;
            this.longTexte = longTexte;

            cnt++;
        }

        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public string DATECREATION
        {
            get { return this.dateCreation; }
            set { this.dateCreation = value; }
        }

        public string DATEEXTRAC
        {
            get { return this.dateExtrac; }
            set { this.dateExtrac = value; }
        }

        public int NBFOLLOWINGS
        {
            get { return this.nbFalwing; }
            set { this.nbFalwing = value; }
        }
       

        public int NBFOLLOWERS
        {
            get { return this.nbFalwers; }
            set { this.nbFalwers = value; }
        }
       

        public int LONGNOM
        {
            get { return this.longNom; }
            set { this.longNom = value; }
        }
      

        public int LONGTEXT
        {
            get { return this.longTexte; }
            set { this.longTexte = value; }
        }

    }
}
