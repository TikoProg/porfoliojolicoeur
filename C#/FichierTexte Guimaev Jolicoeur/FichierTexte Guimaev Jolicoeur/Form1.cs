﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections; //pour les arrays
using System.IO; //pour les streamreaders 
using System.Xml;

namespace FichierTexte_Guimaev_Jolicoeur
{
    public partial class Form1 : Form
    {
        private ArrayList listeUser;
        private ArrayList SearchUser;
        OpenFileDialog newFile = new OpenFileDialog();
        private XmlDocument doc = new XmlDocument();
        private bool GO = false;
        private int count = 1;

        public Form1()
        {
            listeUser = new ArrayList();
            InitializeComponent();


        }




        private void Form1_Load(object sender, EventArgs e)
        {

        }



        private void Lecture()//fonction du lecture des fichier
        {
            try
            {
                string ligne;
                string[] champ; //chauqe info dans une ligne du fichier text
                StreamReader LecteurDeFichier = new StreamReader(newFile.FileName);

                if (ex_txt.Text == ".txt")
                {
                    while ((ligne = LecteurDeFichier.ReadLine()) != null) //pendant qu'il ya lecture du fichier
                    {
                        champ = ligne.Split('\t');//chaque champ separe par une tabulation

                        User information = new User(Convert.ToInt32(champ[0]), champ[1], champ[2], Convert.ToInt32(champ[3]), Convert.ToInt32(champ[4]), Convert.ToInt16(champ[5]), Convert.ToInt16(champ[6]));
                        listeUser.Add(information); //ajoute les infos pour lobjet User 
                    }

                    LecteurDeFichier.Close();
                    ListeUserBindingSource.DataSource = listeUser;
                    dataGridView1.DataSource = listeUser;
                    /*listBoxA.Items.Add(listeUser.Count.ToString());*/ //recupere le nombre d element du array et le converti en char    
                }

                // verification si fichier a XML format
                if (ex_txt.Text == ".xml")
                {
                    XmlDocument LecteurXml = new XmlDocument();
                    LecteurXml.Load(newFile.FileName);
                    XmlNodeList xml_liste = LecteurXml.DocumentElement.SelectNodes("User");

                    foreach (XmlNode element in xml_liste)
                    {

                        User newinformation = new User(Convert.ToInt32(element.SelectSingleNode("ID").InnerText), element.SelectSingleNode("CreationDate").InnerText, element.SelectSingleNode("ExtractionDate").InnerText, Convert.ToInt32(element.SelectSingleNode("Followings").InnerText), Convert.ToInt32(element.SelectSingleNode("Followers").InnerText), Convert.ToInt32(element.SelectSingleNode("NameLenght").InnerText), Convert.ToInt32(element.SelectSingleNode("ProfileLenght").InnerText));
                        listeUser.Add(newinformation);
                    }

                    LecteurXml = null;///???
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    ListeUserBindingSource.DataSource = listeUser;
                    dataGridView1.DataSource = listeUser;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        private void butExportxml_Click(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists("Users_liste.xml"))
                {
                    //permet de reorganiser les elements du fichier
                    XmlWriterSettings set = new XmlWriterSettings();
                    set.Indent = true;

                    //cree un fichier "users_liste.xml"
                    XmlWriter exportX = XmlWriter.Create(Application.StartupPath + @"/users_liste.xml", set);

                    exportX.WriteStartDocument();

                    exportX.WriteStartElement("Users");

                    foreach (User element in listeUser)
                    {
                        exportX.WriteStartElement("User");
                        exportX.WriteElementString("ID", element.id.ToString());
                        exportX.WriteElementString("CreationDate", element.dateCreation.ToString());
                        exportX.WriteElementString("ExtractionDate", element.dateExtrac.ToString());
                        exportX.WriteElementString("Followings", element.nbFalwing.ToString());
                        exportX.WriteElementString("Followers", element.nbFalwers.ToString());
                        exportX.WriteElementString("NameLenght", element.longNom.ToString());
                        exportX.WriteElementString("ProfileLenght", element.longTexte.ToString());
                        exportX.WriteEndElement();
                    }

                    exportX.WriteEndElement();

                    exportX.WriteEndDocument();
                    exportX.Close();
                }
                else
                {
                    if (!File.Exists("Usera_liste.xml"))
                    {

                        XmlDocument doc = new XmlDocument();
                        doc.Load("Users_liste.xml");
                        doc.DocumentElement.RemoveAll();   //  supprimer tout attribute - pour ecrire nouveaux info

                        foreach (User element in listeUser)
                        {

                            XmlElement user = doc.CreateElement("User");
                            XmlElement counter = doc.CreateElement("counter"); ///?????
                            XmlElement id = doc.CreateElement("ID");
                            XmlElement dateCreation = doc.CreateElement("DateCreationCompte");
                            XmlElement dateExtrac = doc.CreateElement("DateExtractionInfo");
                            XmlElement nbfalwings = doc.CreateElement("NombreFollowings");
                            XmlElement nbfalwers = doc.CreateElement("NombreFollowers");
                            XmlElement longNom = doc.CreateElement("LongueurNom");
                            XmlElement longTexte = doc.CreateElement("LongueurProfile");

                            XmlText countertext = doc.CreateTextNode(count.ToString());
                            XmlText idtext = doc.CreateTextNode(element.ID.ToString());
                            XmlText DateCreationtext = doc.CreateTextNode(element.DATECREATION.ToString());
                            XmlText DateExtractext = doc.CreateTextNode(element.DATEEXTRAC.ToString());
                            XmlText NbFollowingstext = doc.CreateTextNode(element.NBFOLLOWINGS.ToString());
                            XmlText NbFollowerstext = doc.CreateTextNode(element.NBFOLLOWERS.ToString());
                            XmlText LongNomtext = doc.CreateTextNode(element.LONGNOM.ToString());
                            XmlText LongProfiletext = doc.CreateTextNode(element.LONGTEXT.ToString());

                            user.AppendChild(counter);
                            user.AppendChild(id);
                            user.AppendChild(dateCreation);
                            user.AppendChild(dateExtrac);
                            user.AppendChild(nbfalwings);
                            user.AppendChild(nbfalwers);
                            user.AppendChild(longNom);
                            user.AppendChild(longTexte);

                            counter.AppendChild(countertext);
                            id.AppendChild(idtext);
                            dateCreation.AppendChild(DateCreationtext);
                            dateExtrac.AppendChild(DateExtractext);
                            nbfalwings.AppendChild(NbFollowingstext);
                            nbfalwers.AppendChild(NbFollowerstext);
                            longNom.AppendChild(LongNomtext);
                            longTexte.AppendChild(LongProfiletext);

                            doc.DocumentElement.AppendChild(user);
                            count++;
                        }

                        GC.Collect();
                        GC.WaitForPendingFinalizers();

                        using (XmlWriter Writer = XmlWriter.Create("Users_liste.xml"))
                        {
                            doc.Save(Writer);

                        }
                        count = 1;
                    }
                }
                MessageBox.Show("Users_liste.xml a ete cree");
            }


            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

        }


        private void butSearch_Click(object sender, EventArgs e)
        {

            GO = false;
            SearchUser = new ArrayList();
            foreach (User element in listeUser)//si le nom cherche est dans liste user
            {
                if (BoxIDsearch.Text == (element.ID).ToString())
                {
                    SearchUser.Add(element);
                    GO = true;
                }

            }
            if (GO == false)
            { MessageBox.Show("Aucun user ID ne correspondent"); }
            else
            { ListeUserBindingSource.DataSource = SearchUser; }



        }

        private void butLoad_Click(object sender, EventArgs e)
        {

            if (newFile.ShowDialog() == DialogResult.OK)
            {
                loaded_file.Text = newFile.FileName;
                ex_txt.Text = Path.GetExtension(newFile.FileName);
                Lecture();

            }

        }

        private void butExportTxt_Click(object sender, EventArgs e)
        {
            try
            {
                string path = Application.StartupPath + @"/users_info.txt";

                if (!File.Exists(path))
                {
                    File.Create(path).Dispose();
                    using (TextWriter exportTxt = new StreamWriter(path))
                    {
                        foreach (User element in listeUser)
                        {
                            exportTxt.Write(element.ID.ToString() + "\t" + element.DATECREATION.ToString() + " " + element.DATEEXTRAC.ToString() + " " + element.NBFOLLOWINGS.ToString() + "\t" + element.NBFOLLOWERS.ToString() + "\t" + element.LONGNOM.ToString() + "\t" + element.LONGTEXT.ToString() + "\n");
                        }

                        exportTxt.Close();
                    }
                }
                else
                {
                    using (TextWriter exportTxt = new StreamWriter(path))
                    {
                        foreach (User element in listeUser)
                        {

                            exportTxt.Write(element.ID.ToString() + "\t" + element.DATECREATION.ToString() + " " + element.DATEEXTRAC.ToString() + " " + element.NBFOLLOWINGS.ToString() + "\t" + element.NBFOLLOWERS.ToString() + "\t" + element.LONGNOM.ToString() + "\t" + element.LONGTEXT.ToString() + "\n");

                        }

                        exportTxt.Close();

                    }

                }
                MessageBox.Show("users_info.txt a ete cree");
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

        }

        private void ListeUserBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void butUpdate_Click(object sender, EventArgs e)
        {
            int x = dataGridView1.CurrentCell.RowIndex;
            int i = 0;

            foreach (User element in listeUser)
            {

                if ((x == i) && (i != listeUser.Count))
                {
                    element.ID = Convert.ToInt32(BoxID.Text);
                    element.DATECREATION = BoxCreation.Text;
                    element.DATEEXTRAC = BoxExtrac.Text;
                    element.NBFOLLOWINGS = Convert.ToInt32(BoxFalwing.Text);
                    element.NBFOLLOWERS = Convert.ToInt32(BoxFalwers.Text);
                    element.LONGNOM = Convert.ToInt32(BoxLongNom.Text);
                    element.LONGTEXT = Convert.ToInt32(BoxLongProfil.Text);

                }
                i++;

            }
        }

        private void butADD_Click(object sender, EventArgs e)
        {
            try
            {
                User newAddedUser = new User(Convert.ToInt32(BoxID.Text), BoxCreation.Text, BoxExtrac.Text, Convert.ToInt32(BoxFalwing.Text), Convert.ToInt32(BoxFalwers.Text), Convert.ToInt32(BoxLongNom.Text), Convert.ToInt32(BoxLongProfil.Text));
                ListeUserBindingSource.Add(newAddedUser);

                BoxID.DataBindings.Add("Text", ListeUserBindingSource, "ID");
                BoxCreation.DataBindings.Add("Text", ListeUserBindingSource, "DateCreationCompte");
                BoxExtrac.DataBindings.Add("Text", ListeUserBindingSource, "DateExtractionInfo");
                BoxFalwing.DataBindings.Add("Text", ListeUserBindingSource, "NombreFollowings");
                BoxFalwers.DataBindings.Add("Text", ListeUserBindingSource, "NombreFollowers");
                BoxLongNom.DataBindings.Add("Text", ListeUserBindingSource, "LongueurNom");
                BoxLongProfil.DataBindings.Add("Text", ListeUserBindingSource, "LongueurProfile");
                BoxID.ReadOnly = true;
                BoxCreation.ReadOnly = true;
                BoxExtrac.ReadOnly = true;
                BoxFalwing.ReadOnly = true;
                BoxFalwers.ReadOnly = true;
                BoxLongNom.ReadOnly = true;
                BoxLongProfil.ReadOnly = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}



//permet d attindre manuellement le chemin d acces au fichiertext ==> Application.StartupPath + @"/users_info.txt             
//StreamReader Lecteur = new StreamReader(Application.StartupPath + @"/users_info.txt");