﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeedDating
{
    public class Profile
    {
        public int id;
        public string Nom;
        public string Prenom;
        public int Age;
        public string Langage;

        public static int cnt;

        public Profile(int id, string Nom, string Prenom, int Age, string Langage)
        {
            this.id = id;
            this.Nom = Nom;
            this.Prenom = Prenom;
            this.Age = Age;
            this.Langage = Langage;

            cnt++;
        }

        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public string NOM
        {
            get { return this.Nom; }
            set { this.Nom = value; }
        }

        public string PRENOM
        {
            get { return this.Prenom; }
            set { this.Prenom = value; }
        }

        public int AGE
        {
            get { return this.Age; }
            set { this.Age = value; }
        }


        public string LANGAGE
        {
            get { return this.Langage; }
            set { this.Langage = value; }
        }
    }
}
