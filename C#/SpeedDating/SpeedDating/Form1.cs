﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpeedDating
{
    public partial class Form1 : Form
    {
        private ArrayList SearchProfile;
        private ArrayList ListProfile;
        private OleDbConnection conn;
        private OleDbDataReader reader;
        private OleDbCommand cmd;


        public Form1()
        {
            InitializeComponent();
            ListProfile = new ArrayList();
            SearchProfile = new ArrayList();
            cmd = new OleDbCommand();
            conn = new OleDbConnection();
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ListProfile = new ArrayList();

            try
            {
                String url = Application.StartupPath + @"\SpeedDating.accdb";
                conn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + url);
                conn.Open();

                cmd = new OleDbCommand("Profile", conn);
                cmd.CommandType = CommandType.TableDirect;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListProfile.Add(new Profile(Convert.ToInt16(reader["ID"]), reader["Nom"].ToString(), reader["Prenom"].ToString(), Convert.ToInt16(reader["Age"]), reader["Langage"].ToString()));
                }
                bindingSource1.DataSource = ListProfile;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();

            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
           
            try
            {
                foreach (Profile element in ListProfile)
                {
                    if (boxSearch.Text == (element.Langage).ToString())
                    {
                        SearchProfile.Add(element);

                    }

                    
                }
                bindingSource1.DataSource = SearchProfile;
                SearchProfile = new ArrayList();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                cmd = new OleDbCommand("insert into Profile(ID,Nom,Prenom,Age,Langage) values(@n,@p,@a,@e,@f)", conn);
                cmd.Parameters.AddWithValue("@n", boxID.Text);
                cmd.Parameters.AddWithValue("@p", boxNom.Text);
                cmd.Parameters.AddWithValue("@a", boxPrenom.Text);
                cmd.Parameters.AddWithValue("@e", boxAge.Text);
                cmd.Parameters.AddWithValue("@f", boxLangage.Text);
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();

            }
        }

        private void butUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                cmd = new OleDbCommand("Update Profile set Nom = @p,Prenom = @a, Age = @e ,Langage = @f ,ID = @n where ID = @n", conn);
                cmd.Parameters.AddWithValue("@n", Convert.ToInt16(boxID.Text));
                cmd.Parameters.AddWithValue("@p", boxNom.Text);
                cmd.Parameters.AddWithValue("@a", boxPrenom.Text);
                cmd.Parameters.AddWithValue("@e", Convert.ToInt16(boxAge.Text));
                cmd.Parameters.AddWithValue("@f", boxLangage.Text);
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            try
            {
                foreach (Profile element in ListProfile)
                {
                    

                    if (boxME.Text == (element.Nom).ToString())
                    {
                        SearchProfile.Add(element);

                    }

                    
                }
                bindingSource1.DataSource = SearchProfile;
                SearchProfile = new ArrayList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
