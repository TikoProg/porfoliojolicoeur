﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public  int nbmois;
        //string[] nom = new string[12]{ "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre" };
        private mois[] listepourlesmois;

        private struct mois
        {
            private string periode;
            private int pre;
            private int tmax;
            private int tmini;
            private double tmoy;
            

            public mois(string periode, int pre, int tmax, int tmini)
            {

                this.pre = pre;
                this.periode = periode;


                if (tmax < -50)
                {
                    this.tmax = -50;
                }
                else if (tmax > 50)
                {
                    this.tmax = 50;
                }
                else
                {
                    this.tmax = tmax;
                }

                if (tmini < -50)
                { this.tmini = -50; }
                else if (tmini > 50)
                { this.tmini = 50; }
                else
                { this.tmini = tmini; }


                this.tmoy =(tmax+tmini)/2;
                

            }

            //parametres(propriété) de mes textboxes 

            public int PRE
            {
                get { return this.pre; }
                set
                {
                    if (value < 0)
                    {
                        this.pre = 0;
                    }
                    else
                    {
                        this.pre = value;
                    }
                }
            }
            public int TMAX
            {
                get { return this.tmax; }
                set
                {
                    if (value < -50)
                    {
                        this.tmax = -50;
                    }
                    else if (value > 50)
                    {
                        this.tmax = 50;
                    }
                    else
                    {
                        this.tmax = value;
                    }
                }
            }
            public int TMINI
            {
                get { return this.tmini; }
                set
                {
                    if (value < -50)
                    {
                        this.tmini = -50;
                    }
                    else if (value > 50)
                    {
                        this.tmini = 50;
                    }
                    else
                    {
                        this.tmini = value;
                    }
                }
            }
            public string PERIODE
            {
                get { return this.periode; }
                set { this.periode = value; }
            }

                public double TMOY
            {
                get { return ((this.tmini + this.tmax) / 2); }

            }

        }


        public Form1()
        {
            InitializeComponent();
            listepourlesmois = new mois[12];
           
        }
    

        private void butenter_Click_1(object sender, EventArgs e)
        {


            //mois m1 = new mois (Convert.ToInt16(txtpre.Text), Convert.ToInt16(txtmax.Text), Convert.ToInt16(txtmin.Text));
            //listepourlesmois[nbmois] = m1;
            //nbmois += 1;
            //lblnbmois.Text = nbmois.ToString() + (" Mois");
            
            //label5.Text =Convert.ToString (nom[nbmois-1]);
           
            if (nbmois < 12)
            {
                mois m1 = new mois(comboBoxmois.Text,  Convert.ToInt16(txtpre.Text), Convert.ToInt16(txtmax.Text), Convert.ToInt16(txtmin.Text));
                listepourlesmois[nbmois] = m1;
                nbmois++;
                lblnbmois.Text = nbmois.ToString() + (" Mois");

                label5.Text = comboBoxmois .Text;
                butenter.Visible = true;
                butstats.Visible = false;
                lblnbmois.Visible = true;
                label5.Visible = true;
                comboBoxmois .Items.Remove(comboBoxmois .SelectedItem);

            }
            else
            {
                butenter.Visible = false;
                butstats.Visible = true;
                lblnbmois.Visible = false;
                label5.Visible = false;
                comboBoxmois.Visible = false;

            }

           


        }

        private void butstats_Click_1(object sender, EventArgs e)
        {
               double pretotalann = 0;
            double moypreann=0.0 ;
            double tempannmax=-50;
            double tempannmin=50;
            double comparaisonMax;
            double comparaisonMin;
            string moisMax= "" ;
            string moisMin= "";
            
            foreach (mois element in listepourlesmois)
            {

                pretotalann += (element.PRE);

                moypreann = (pretotalann / listepourlesmois.Length);

                comparaisonMax = Math.Max(Convert.ToInt16(tempannmax), Convert.ToInt16(element.TMAX));
              
                if (Math.Max(Convert.ToInt16(tempannmax), Convert.ToInt16(element.TMAX)) == tempannmax)
                {
                    moisMax = element.TMAX + " " + moisMax;
                }
                tempannmax = comparaisonMax;





                comparaisonMin = Math.Min( Convert.ToInt16(tempannmin),Convert.ToInt16(element.TMINI));
                tempannmin = comparaisonMin;
                if (comparaisonMin == tempannmin)
                {
                    moisMin = element.TMINI + " " + moisMin;
                }


            }

           
           

        
            boxmois.Items.Add("Pécipitation totale annuelle: " + Convert.ToString(pretotalann));
            boxmois.Items.Add("Moyenne des Pécipitations annuelles: " + Convert.ToString(moypreann ));
          boxmois.Items.Add("Température annuelle maximum: " + Convert.ToString(tempannmax));
            boxmois.Items.Add("Mois Température  Maximum: " +  Convert.ToString(moisMax ));
            boxmois.Items.Add("Température annuelle minimum: " + Convert.ToString(tempannmin));
            boxmois.Items.Add("Mois Température  minimum: " + Convert.ToString(moisMin));
            txtpre.Clear();
                        txtpre.Focus();
                        txtmax.Clear();
                        txtmin.Clear();
      


        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void butclose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
    
}
