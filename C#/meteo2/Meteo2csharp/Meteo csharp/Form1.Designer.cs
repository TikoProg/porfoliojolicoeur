﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.boxmois = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.txtmax = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtmin = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtpre = new System.Windows.Forms.TextBox();
            this.butstats = new System.Windows.Forms.Button();
            this.lblnbmois = new System.Windows.Forms.Label();
            this.butenter = new System.Windows.Forms.Button();
            this.butclose = new System.Windows.Forms.Button();
            this.lablMois = new System.Windows.Forms.Label();
            this.comboBoxmois = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // boxmois
            // 
            this.boxmois.BackColor = System.Drawing.SystemColors.MenuText;
            this.boxmois.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.boxmois.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boxmois.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.boxmois.FormattingEnabled = true;
            this.boxmois.ItemHeight = 16;
            this.boxmois.Location = new System.Drawing.Point(316, 39);
            this.boxmois.Name = "boxmois";
            this.boxmois.Size = new System.Drawing.Size(425, 274);
            this.boxmois.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.comboBoxmois);
            this.panel1.Controls.Add(this.lablMois);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtmax);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtmin);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtpre);
            this.panel1.Controls.Add(this.butstats);
            this.panel1.Controls.Add(this.lblnbmois);
            this.panel1.Controls.Add(this.butenter);
            this.panel1.Location = new System.Drawing.Point(22, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(288, 277);
            this.panel1.TabIndex = 12;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(220, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 13;
            // 
            // txtmax
            // 
            this.txtmax.BackColor = System.Drawing.SystemColors.WindowText;
            this.txtmax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmax.ForeColor = System.Drawing.SystemColors.Window;
            this.txtmax.Location = new System.Drawing.Point(223, 55);
            this.txtmax.Name = "txtmax";
            this.txtmax.Size = new System.Drawing.Size(43, 21);
            this.txtmax.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 15);
            this.label2.TabIndex = 13;
            this.label2.Text = "Température maximum :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 15);
            this.label1.TabIndex = 12;
            this.label1.Text = "Pécipitation mensuelle totale :";
            // 
            // txtmin
            // 
            this.txtmin.BackColor = System.Drawing.SystemColors.WindowText;
            this.txtmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmin.ForeColor = System.Drawing.SystemColors.Window;
            this.txtmin.Location = new System.Drawing.Point(223, 94);
            this.txtmin.Name = "txtmin";
            this.txtmin.Size = new System.Drawing.Size(43, 21);
            this.txtmin.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 15);
            this.label4.TabIndex = 15;
            this.label4.Text = "Température minimum :";
            // 
            // txtpre
            // 
            this.txtpre.BackColor = System.Drawing.SystemColors.WindowText;
            this.txtpre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpre.ForeColor = System.Drawing.SystemColors.Window;
            this.txtpre.Location = new System.Drawing.Point(223, 17);
            this.txtpre.Name = "txtpre";
            this.txtpre.Size = new System.Drawing.Size(43, 21);
            this.txtpre.TabIndex = 17;
            // 
            // butstats
            // 
            this.butstats.BackColor = System.Drawing.Color.ForestGreen;
            this.butstats.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butstats.ForeColor = System.Drawing.SystemColors.Desktop;
            this.butstats.Location = new System.Drawing.Point(16, 218);
            this.butstats.Name = "butstats";
            this.butstats.Size = new System.Drawing.Size(143, 40);
            this.butstats.TabIndex = 22;
            this.butstats.Text = "Statistique";
            this.butstats.UseVisualStyleBackColor = false;
            this.butstats.Visible = false;
            this.butstats.Click += new System.EventHandler(this.butstats_Click_1);
            // 
            // lblnbmois
            // 
            this.lblnbmois.AutoSize = true;
            this.lblnbmois.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnbmois.Location = new System.Drawing.Point(219, 172);
            this.lblnbmois.Name = "lblnbmois";
            this.lblnbmois.Size = new System.Drawing.Size(19, 20);
            this.lblnbmois.TabIndex = 21;
            this.lblnbmois.Text = "0";
            // 
            // butenter
            // 
            this.butenter.BackColor = System.Drawing.Color.CornflowerBlue;
            this.butenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butenter.Location = new System.Drawing.Point(15, 162);
            this.butenter.Name = "butenter";
            this.butenter.Size = new System.Drawing.Size(144, 40);
            this.butenter.TabIndex = 16;
            this.butenter.Text = "Enter";
            this.butenter.UseVisualStyleBackColor = false;
            this.butenter.Click += new System.EventHandler(this.butenter_Click_1);
            // 
            // butclose
            // 
            this.butclose.BackColor = System.Drawing.Color.DarkRed;
            this.butclose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butclose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.butclose.Location = new System.Drawing.Point(632, 323);
            this.butclose.Name = "butclose";
            this.butclose.Size = new System.Drawing.Size(109, 44);
            this.butclose.TabIndex = 13;
            this.butclose.Text = "Close";
            this.butclose.UseVisualStyleBackColor = false;
            this.butclose.Click += new System.EventHandler(this.butclose_Click);
            // 
            // lablMois
            // 
            this.lablMois.AutoSize = true;
            this.lablMois.Location = new System.Drawing.Point(37, 133);
            this.lablMois.Name = "lablMois";
            this.lablMois.Size = new System.Drawing.Size(28, 13);
            this.lablMois.TabIndex = 23;
            this.lablMois.Text = "mois";
            // 
            // comboBoxmois
            // 
            this.comboBoxmois.FormattingEnabled = true;
            this.comboBoxmois.Items.AddRange(new object[] {
            "janvier",
            "fevrier",
            "mars",
            "avril",
            "mai",
            "juin",
            "juillet",
            "aout",
            "septembre",
            "octobre",
            "novembre",
            "decembre"});
            this.comboBoxmois.Location = new System.Drawing.Point(191, 125);
            this.comboBoxmois.Name = "comboBoxmois";
            this.comboBoxmois.Size = new System.Drawing.Size(75, 21);
            this.comboBoxmois.TabIndex = 24;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(762, 379);
            this.Controls.Add(this.butclose);
            this.Controls.Add(this.boxmois);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListBox boxmois;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtmax;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtmin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtpre;
        private System.Windows.Forms.Button butstats;
        private System.Windows.Forms.Label lblnbmois;
        private System.Windows.Forms.Button butenter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button butclose;
        private System.Windows.Forms.ComboBox comboBoxmois;
        private System.Windows.Forms.Label lablMois;
    }
}

