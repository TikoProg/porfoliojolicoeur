﻿namespace The_lion_game
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lapin6 = new System.Windows.Forms.Panel();
            this.elment = new System.Windows.Forms.Panel();
            this.lapin4 = new System.Windows.Forms.Panel();
            this.lapin1 = new System.Windows.Forms.Panel();
            this.lapin2 = new System.Windows.Forms.Panel();
            this.lapin3 = new System.Windows.Forms.Panel();
            this.Carrote = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lion = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lapin6
            // 
            this.lapin6.BackColor = System.Drawing.Color.Transparent;
            this.lapin6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lapin6.BackgroundImage")));
            this.lapin6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lapin6.Location = new System.Drawing.Point(586, 414);
            this.lapin6.Name = "lapin6";
            this.lapin6.Size = new System.Drawing.Size(57, 49);
            this.lapin6.TabIndex = 0;
            this.lapin6.Paint += new System.Windows.Forms.PaintEventHandler(this.lapin6_Paint);
            // 
            // lapin5
            // 
            this.elment.BackColor = System.Drawing.Color.Transparent;
            this.elment.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lapin5.BackgroundImage")));
            this.elment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.elment.Location = new System.Drawing.Point(609, 360);
            this.elment.Name = "lapin5";
            this.elment.Size = new System.Drawing.Size(53, 48);
            this.elment.TabIndex = 1;
            this.elment.Paint += new System.Windows.Forms.PaintEventHandler(this.lapin5_Paint);
            // 
            // lapin4
            // 
            this.lapin4.BackColor = System.Drawing.Color.Transparent;
            this.lapin4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lapin4.BackgroundImage")));
            this.lapin4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lapin4.Location = new System.Drawing.Point(721, 462);
            this.lapin4.Name = "lapin4";
            this.lapin4.Size = new System.Drawing.Size(53, 49);
            this.lapin4.TabIndex = 2;
            this.lapin4.Paint += new System.Windows.Forms.PaintEventHandler(this.lapin4_Paint);
            // 
            // lapin1
            // 
            this.lapin1.BackColor = System.Drawing.Color.Transparent;
            this.lapin1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lapin1.BackgroundImage")));
            this.lapin1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lapin1.Location = new System.Drawing.Point(562, 462);
            this.lapin1.Name = "lapin1";
            this.lapin1.Size = new System.Drawing.Size(55, 49);
            this.lapin1.TabIndex = 3;
            this.lapin1.Paint += new System.Windows.Forms.PaintEventHandler(this.lapin1_Paint);
            // 
            // lapin2
            // 
            this.lapin2.BackColor = System.Drawing.Color.Transparent;
            this.lapin2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lapin2.BackgroundImage")));
            this.lapin2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lapin2.Location = new System.Drawing.Point(609, 507);
            this.lapin2.Name = "lapin2";
            this.lapin2.Size = new System.Drawing.Size(55, 49);
            this.lapin2.TabIndex = 4;
            this.lapin2.Paint += new System.Windows.Forms.PaintEventHandler(this.lapin2_Paint);
            // 
            // lapin3
            // 
            this.lapin3.BackColor = System.Drawing.Color.Transparent;
            this.lapin3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lapin3.BackgroundImage")));
            this.lapin3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lapin3.ForeColor = System.Drawing.Color.Transparent;
            this.lapin3.Location = new System.Drawing.Point(670, 517);
            this.lapin3.Name = "lapin3";
            this.lapin3.Size = new System.Drawing.Size(52, 48);
            this.lapin3.TabIndex = 5;
            this.lapin3.Paint += new System.Windows.Forms.PaintEventHandler(this.lapin3_Paint);
            // 
            // Carrote
            // 
            this.Carrote.BackColor = System.Drawing.Color.Transparent;
            this.Carrote.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Carrote.BackgroundImage")));
            this.Carrote.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Carrote.Location = new System.Drawing.Point(639, 469);
            this.Carrote.Name = "Carrote";
            this.Carrote.Size = new System.Drawing.Size(47, 42);
            this.Carrote.TabIndex = 6;
            this.Carrote.Paint += new System.Windows.Forms.PaintEventHandler(this.panCarrote_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(652, 346);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 138);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // lion
            // 
            this.lion.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.lion.BackColor = System.Drawing.Color.Transparent;
            this.lion.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lion.BackgroundImage")));
            this.lion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lion.Location = new System.Drawing.Point(210, 258);
            this.lion.Margin = new System.Windows.Forms.Padding(0);
            this.lion.Name = "lion";
            this.lion.Size = new System.Drawing.Size(127, 132);
            this.lion.TabIndex = 9;
            this.lion.Paint += new System.Windows.Forms.PaintEventHandler(this.lion_Paint);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(984, 591);
            this.Controls.Add(this.lion);
            this.Controls.Add(this.lapin6);
            this.Controls.Add(this.elment);
            this.Controls.Add(this.Carrote);
            this.Controls.Add(this.lapin3);
            this.Controls.Add(this.lapin2);
            this.Controls.Add(this.lapin1);
            this.Controls.Add(this.lapin4);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel lapin6;
        private System.Windows.Forms.Panel elment;
        private System.Windows.Forms.Panel lapin4;
        private System.Windows.Forms.Panel lapin1;
        private System.Windows.Forms.Panel lapin2;
        private System.Windows.Forms.Panel lapin3;
        private System.Windows.Forms.Panel Carrote;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel lion;
    }
}

