﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_lion_game
{
    public partial class Form1 : Form
    {
        public Timer beatCAR;
        private ArrayList cibleLapin;
        private Random alea;

        public Form1()
        {
            InitializeComponent();
            alea = new Random(DateTime.Now.Millisecond);

            cibleLapin = new ArrayList();

            cibleLapin.Add(lapin1);
            cibleLapin.Add(lapin2);
            cibleLapin.Add(lapin3);
            cibleLapin.Add(lapin4);
            cibleLapin.Add(elment);
            cibleLapin.Add(lapin6);

          


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            beatCAR = new Timer();

            beatCAR.Interval = 1000;
            //tic est l'evenenemt
            beatCAR.Tick += beatCAR_Tick;
            beatCAR.Enabled = true;
            beatCAR.Start();
           
        }
                       

        private void beatCAR_Tick(object sender, EventArgs e)
        {

            Random alea = new Random(DateTime.Now.Millisecond);


            Carrote.Location = new Point(alea.Next(0, 300), +alea.Next(300, 400));
                
           
          

        }


      

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {  
            
            //capture up arrow key    
            if (keyData == Keys.Up )
            {
                lion.Location = new Point(lion.Location.X , lion.Location.Y - 10);
                
            }
            //capture down arrow key    
            if (keyData == Keys.Down )
            {
                lion.Location = new Point(lion.Location.X , lion.Location.Y + 10);
                
            }
            //capture left arrow key    
            if (keyData == Keys.Left)
            {
                lion.Location = new Point(lion.Location.X - 10, lion.Location.Y );
                
            }
            //capture right arrow key    
            if (keyData == Keys.Right )
            {
                lion.Location = new Point(lion.Location.X + 10, lion.Location.Y);
              
            }

              Double distance = 0;

            foreach (Panel element in cibleLapin)
            {
                //Panel ptemp = new Panel();
                //ptemp = (Panel)element;

                distance = Math.Sqrt(Math.Pow((lion.Location.X - element.Location.X), 2) - (Math.Pow((lion.Location.Y -element.Location.Y), 2)));

                if (distance <= 10)
                {
                    element.Dispose();

                }

            }
            return base.ProcessCmdKey(ref msg, keyData);

            //si le lion est en colision avec un des lapin
            
        }


        private void EatRabbit()
        {
         
            
        }

        void moveAlea()
        {
            foreach (Panel element in cibleLapin)
            {

                int step = 5;
                double deltaX = Math.Abs(elment.Location.X - Carrote.Location.X);
                double deltaY = Math.Abs(elment.Location.Y - Carrote.Location.Y);

                double distance = Math.Sqrt(deltaX * deltaX + deltaY * deltaY);

                double angle = radToDeg(Math.Atan(deltaY / deltaX));



                //en haut à droite
                if (elment.Location.X > Carrote.Location.X && elment.Location.Y < Carrote.Location.Y)
                {

                    Carrote.Location = new Point(Convert.ToInt16(Carrote.Location.X + distance / step * Math.Cos(DegToRad(angle))),
                                                     Convert.ToInt16(Carrote.Location.Y - distance / step * Math.Sin(DegToRad(angle))));

                } //en haut à gauche
                else if (elment.Location.X < Carrote.Location.X && elment.Location.Y < Carrote.Location.Y)
                {

                    Carrote.Location = new Point(Convert.ToInt16(Carrote.Location.X - distance / step * Math.Cos(DegToRad(angle))),
                                                     Convert.ToInt16(Carrote.Location.Y - distance / step * Math.Sin(DegToRad(angle))));

                } //en haut 
                else if (elment.Location.X == Carrote.Location.X && elment.Location.Y < Carrote.Location.Y)
                {

                    Carrote.Location = new Point(Carrote.Location.X,
                                                     Convert.ToInt16(Carrote.Location.Y - distance / step * Math.Sin(DegToRad(angle))));

                }//à droite
                else if (elment.Location.X > Carrote.Location.X && elment.Location.Y == Carrote.Location.Y)
                {

                    Carrote.Location = new Point(Convert.ToInt16(Carrote.Location.X + distance / step),
                                                      Carrote.Location.Y);

                }//à gauche
                else if (elment.Location.X < Carrote.Location.X && elment.Location.Y == Carrote.Location.Y)
                {

                    Carrote.Location = new Point(Convert.ToInt16(Carrote.Location.X - distance / step),
                                                      Carrote.Location.Y);

                }//en bas
                else if (elment.Location.X == Carrote.Location.X && elment.Location.Y > Carrote.Location.Y)
                {

                    Carrote.Location = Carrote.Location = new Point(Convert.ToInt16(Carrote.Location.X - distance / step * Math.Cos(DegToRad(angle))),
                                                     Convert.ToInt16(Carrote.Location.Y + distance / step * Math.Sin(DegToRad(angle))));

                }//en bas à gauche
                else if (elment.Location.X < Carrote.Location.X && elment.Location.Y > Carrote.Location.Y)
                {

                    Carrote.Location = new Point(Convert.ToInt16(Carrote.Location.X - distance / step * Math.Cos(DegToRad(angle))),
                                                     Convert.ToInt16(Carrote.Location.Y + distance / step * Math.Sin(DegToRad(angle))));

                } //en bas à droite
                else if (elment.Location.X > Carrote.Location.X && elment.Location.Y > Carrote.Location.Y)
                {

                    Carrote.Location = new Point(Convert.ToInt16(Carrote.Location.X + distance / step * Math.Cos(DegToRad(angle))),
                                                     Convert.ToInt16(Carrote.Location.Y + distance / step * Math.Sin(DegToRad(angle))));

                }

                if (distance < 60)
                {
                    elment.Location = new Point(alea.Next(400), alea.Next(400));
                   
                }
            }
        }

        private double radToDeg(double RadAngle)
        {
            return (RadAngle * 180) / Math.PI;

        }

        private double DegToRad(double DegAngle)
        {
            return (DegAngle * Math.PI) / 180;
        }


        private void lion_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lapin6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lapin5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lapin1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lapin4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lapin3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lapin2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panCarrote_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
