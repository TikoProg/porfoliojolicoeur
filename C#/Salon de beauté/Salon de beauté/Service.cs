﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Service
    {
        private int IDservice;
        private static int cnt;
        private string nom;
        private double prix;

        public Service()
        {
            this.IDservice = 0;
            this.nom = "Nom";
            this.prix = 0;
            cnt++;

        }

        public Service(string nom, double prix)
        {

            this.nom = nom;
            this.prix = prix;
            cnt++;
        }

        public int ID
        {
            get { return this.IDservice; }

        }

        public string NOM
        {
            get { return this.nom; }

        }

        public double PRIX
        {
            get { return this.prix; }

        }

        //public override string ToString()
        //{
        //    return this.nom + this.prix;
        //}


    }
}
