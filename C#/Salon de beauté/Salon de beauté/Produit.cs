﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Produit
    {
        private static int cnt;
        private int IDproduit;
        private string nom;
        private double prix;

        public Produit()
        {
            this.IDproduit=0;
            this.nom="Nom";
            this.prix=0;
            cnt++;

        }
        
        public Produit(string nom,double prix)
        {
            
            this.nom = nom;
            this.prix = prix;
            cnt++;
        }

        public int ID
        {
            get { return this.IDproduit; }
            
        }

        public  string NOM
        {
            get { return this.nom; }
            
        }

        public  double PRIX
        {
            get { return this.prix; }
           
        }

        //public override string ToString()
        //{
        //    return  this.nom  + this.prix;
        //}



    }
}
