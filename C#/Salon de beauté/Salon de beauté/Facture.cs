﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Facture
    {
        private double prixTotale;
        private double rabaisProduit;
        private double rabaisService;
        private double totaleProduit;
        private double totaleService;

        public Facture(double totaleProduit,double rabaisProduit, double totaleService,double rabaisService)
        {
            this.totaleProduit = totaleProduit;
            this.rabaisProduit = rabaisProduit;
            this.totaleService = totaleService;
            this.rabaisService = rabaisService;
           
        }


        private double PRIXTOTALE
        {

            get { return this.prixTotale; }
        }


        private double RABAISPRO
        {
            get { return this.rabaisProduit; }
        }


        private double RABAISSERV
        {
            get { return this.rabaisService; }
        }


        private double TOTALEPRO
        {
            get { return this.totaleProduit; }
        }


        private double TOTALESERV
        {
            get { return this.totaleService; }
        }

    }
}
