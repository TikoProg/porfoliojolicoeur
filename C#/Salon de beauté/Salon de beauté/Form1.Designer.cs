﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.butCreate = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.butAddServ = new System.Windows.Forms.Button();
            this.butAddPro = new System.Windows.Forms.Button();
            this.listService = new System.Windows.Forms.ListBox();
            this.listProduit = new System.Windows.Forms.ListBox();
            this.listClient = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.butTotal = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.AfficheServ = new System.Windows.Forms.ListBox();
            this.AffichePro = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.PowderBlue;
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.butCreate);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(195, 220);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Création de Client";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Premium",
            "Gold",
            "Silver",
            "Aucun"});
            this.comboBox1.Location = new System.Drawing.Point(44, 127);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.Text = "Membership";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(44, 86);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(121, 20);
            this.textBox2.TabIndex = 2;
            this.textBox2.Text = "Prenom";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(44, 42);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "Nom";
            // 
            // butCreate
            // 
            this.butCreate.Location = new System.Drawing.Point(44, 182);
            this.butCreate.Name = "butCreate";
            this.butCreate.Size = new System.Drawing.Size(107, 23);
            this.butCreate.TabIndex = 0;
            this.butCreate.Text = "Create Client";
            this.butCreate.UseVisualStyleBackColor = true;
            this.butCreate.Click += new System.EventHandler(this.butCreate_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.PowderBlue;
            this.groupBox2.Controls.Add(this.butAddServ);
            this.groupBox2.Controls.Add(this.butAddPro);
            this.groupBox2.Controls.Add(this.listService);
            this.groupBox2.Controls.Add(this.listProduit);
            this.groupBox2.Controls.Add(this.listClient);
            this.groupBox2.Location = new System.Drawing.Point(244, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(579, 220);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Achat de produits t de services";
            // 
            // butAddServ
            // 
            this.butAddServ.Location = new System.Drawing.Point(388, 182);
            this.butAddServ.Name = "butAddServ";
            this.butAddServ.Size = new System.Drawing.Size(75, 23);
            this.butAddServ.TabIndex = 4;
            this.butAddServ.Text = "AddService";
            this.butAddServ.UseVisualStyleBackColor = true;
            this.butAddServ.Click += new System.EventHandler(this.butAddServ_Click);
            // 
            // butAddPro
            // 
            this.butAddPro.Location = new System.Drawing.Point(197, 182);
            this.butAddPro.Name = "butAddPro";
            this.butAddPro.Size = new System.Drawing.Size(75, 23);
            this.butAddPro.TabIndex = 3;
            this.butAddPro.Text = "AddProduct";
            this.butAddPro.UseVisualStyleBackColor = true;
            this.butAddPro.Click += new System.EventHandler(this.butAddPro_Click);
            // 
            // listService
            // 
            this.listService.FormattingEnabled = true;
            this.listService.Items.AddRange(new object[] {
            "Manicure",
            "Pedicure",
            "Coiffure ",
            "Coloration"});
            this.listService.Location = new System.Drawing.Point(388, 20);
            this.listService.Name = "listService";
            this.listService.Size = new System.Drawing.Size(185, 147);
            this.listService.TabIndex = 2;
            // 
            // listProduit
            // 
            this.listProduit.FormattingEnabled = true;
            this.listProduit.Items.AddRange(new object[] {
            "Rouge a levre",
            "Mascarade ",
            "Creme pour les mains",
            "Fond de tein"});
            this.listProduit.Location = new System.Drawing.Point(197, 20);
            this.listProduit.Name = "listProduit";
            this.listProduit.Size = new System.Drawing.Size(185, 147);
            this.listProduit.TabIndex = 1;
            // 
            // listClient
            // 
            this.listClient.FormattingEnabled = true;
            this.listClient.Location = new System.Drawing.Point(6, 19);
            this.listClient.Name = "listClient";
            this.listClient.Size = new System.Drawing.Size(185, 147);
            this.listClient.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.PowderBlue;
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.butTotal);
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.AfficheServ);
            this.groupBox3.Controls.Add(this.AffichePro);
            this.groupBox3.Location = new System.Drawing.Point(12, 258);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(614, 299);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Panier d\'achat";
            // 
            // butTotal
            // 
            this.butTotal.Location = new System.Drawing.Point(429, 140);
            this.butTotal.Name = "butTotal";
            this.butTotal.Size = new System.Drawing.Size(160, 26);
            this.butTotal.TabIndex = 4;
            this.butTotal.Text = "Total Price";
            this.butTotal.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(210, 206);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(185, 35);
            this.button3.TabIndex = 3;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(19, 206);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(185, 35);
            this.button2.TabIndex = 2;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // AfficheServ
            // 
            this.AfficheServ.FormattingEnabled = true;
            this.AfficheServ.Location = new System.Drawing.Point(210, 19);
            this.AfficheServ.Name = "AfficheServ";
            this.AfficheServ.Size = new System.Drawing.Size(185, 173);
            this.AfficheServ.TabIndex = 1;
            // 
            // AffichePro
            // 
            this.AffichePro.FormattingEnabled = true;
            this.AffichePro.Location = new System.Drawing.Point(19, 19);
            this.AffichePro.Name = "AffichePro";
            this.AffichePro.Size = new System.Drawing.Size(185, 173);
            this.AffichePro.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(426, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Total Produits:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(426, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Rabais Produits:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(426, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Total Services:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(426, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Rabais Services:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(531, 179);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "label5";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 569);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button butCreate;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button butAddServ;
        private System.Windows.Forms.Button butAddPro;
        private System.Windows.Forms.ListBox listService;
        private System.Windows.Forms.ListBox listProduit;
        private System.Windows.Forms.ListBox listClient;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button butTotal;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox AfficheServ;
        private System.Windows.Forms.ListBox AffichePro;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}

