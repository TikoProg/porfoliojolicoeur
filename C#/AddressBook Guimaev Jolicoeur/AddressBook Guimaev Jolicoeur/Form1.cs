﻿using System;
using System.Collections;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;


namespace AddressBook_Guimaev_Jolicoeur
{
    public partial class Form1 : Form
    {
        private ArrayList SearchContact;
        private ArrayList ListContact;
        private OleDbConnection conn;
        private OleDbDataReader reader;
        private OleDbCommand cmd;

        public Form1()
        {
            InitializeComponent();
            ListContact = new ArrayList();
            SearchContact = new ArrayList();
            cmd = new OleDbCommand();
            conn = new OleDbConnection();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            bindingSource1.MoveNext();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            bindingSource1.MovePrevious();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                foreach(Contact element in ListContact)
                {
                    if ((element.NOM).ToString()==boxSearch.Text.ToString() )
                    {
                        SearchContact.Add(element);
 bindingSource1.DataSource = SearchContact;
                    }

                   
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           

            try
            {
                String url = Application.StartupPath + @"\AddressBook.accdb";
                conn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + url);
                conn.Open();

                cmd = new OleDbCommand("Contact", conn);
                cmd.CommandType = CommandType.TableDirect;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListContact.Add(new Contact(Convert.ToInt16( reader["ID"]), reader["Nom"].ToString(), reader["Prenom"].ToString(), reader["Email"].ToString(), reader["PhoneNumber"].ToString()));
                }
                bindingSource1.DataSource = ListContact;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                boxCount.Text = Convert.ToString(ListContact.Count);
            
                conn.Close();
                
            }
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                cmd = new OleDbCommand("insert into Contact(ID,Nom,Prenom,Email,PhoneNumber) values(@n,@p,@a,@e,@f)", conn);
                cmd.Parameters.AddWithValue("@n", boxID.Text);
                cmd.Parameters.AddWithValue("@p", boxNom.Text);
                cmd.Parameters.AddWithValue("@a", boxPrenom.Text);
                cmd.Parameters.AddWithValue("@e", boxEmail.Text);
                cmd.Parameters.AddWithValue("@f", boxPhone.Text);
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();

            }
        }

        private void boxSearch_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
