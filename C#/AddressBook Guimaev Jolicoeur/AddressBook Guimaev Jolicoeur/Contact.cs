﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddressBook_Guimaev_Jolicoeur
{
   public class Contact
    {
        public int id;
        public string Nom;
        public string Prenom;
        public string Email;
        public string PhoneNumber;

        public static int cnt;

        public Contact(int id, string Nom, string Prenom, string Email, string PhoneNumber)
        {
            this.id = id;
            this.Nom = Nom;
            this.Prenom = Prenom;
            this.Email = Email;
            this.PhoneNumber = PhoneNumber;

            cnt++;
        }

        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public string NOM
        {
            get { return this.Nom; }
            set { this.Nom = value; }
        }

        public string PRENOM
        {
            get { return this.Prenom; }
            set { this.Prenom = value; }
        }

        public string EMAIL
        {
            get { return this.Email; }
            set { this.Email = value; }
        }


        public string PHONENUMBER
        {
            get { return this.PhoneNumber; }
            set { this.PhoneNumber = value; }
        }

    }
}
