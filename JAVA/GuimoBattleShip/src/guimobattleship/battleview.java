/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guimobattleship;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.paint.Color;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 *
 * @author Tikol
 */
public class battleview extends javax.swing.JFrame implements ActionListener {

    private static JButton[][] butarray =new JButton[10][10];
    private static JLabel[][] OceanArray=new JLabel[10][10];
    private static int[] listbateaux = new int[5];
    public static boolean player1;
    private static int taille=10;
    private Thread t;
    backgroundThread bk; 
    /**
     * Creates new form battleview
     */
    public battleview() {
        initComponents();
        initializeButtonGrid();
        initializeOceanGrid();
        initializeship();
        bk = new backgroundThread(); 
    }
    
    //creation de mon gri d'attacque :-)
    private void initializeButtonGrid()
    {
        int nbbutton=1;
        buttongrid.setLayout(new GridLayout(taille,taille));
        for (int i = 0; i < taille; i++) 
        {
            for (int j = 0; j < taille; j++) 
            {
                butarray[i][j]=new JButton(String.valueOf(nbbutton++));                
                butarray[i][j].setForeground(java.awt.Color.LIGHT_GRAY);
                butarray[i][j].addActionListener(this);
                butarray[i][j].setSize(20,20);       
                buttongrid.add(butarray[i][j]);                
            }
            
        }
        validate();
    }
    
    //action de mes buttons
    @Override
    public void actionPerformed(ActionEvent e) 
    {
        for (int i = 0; i < taille; i++) 
        {
            for (int j = 0; j < taille; j++) 
            {
                if (e.getSource()==butarray[i][j]) 
                {
                    bk.dataToSend=true;
                    int nbpos =Integer.parseInt(butarray[i][j].getActionCommand());                  
                    butarray[i][j].setBackground(java.awt.Color.RED);
                    bk.missileOutgoing = nbpos; 
                    t= new Thread(bk);
                    t.start();

//                   msglabel.setText(butarray[i][j].getActionCommand());
//                   msglabel.setText(OceanArray[i][j].getText());
//                   upgrageOceanGrid(nbpos);
                }
                         
            }
            
        }       
    }
    
    //creation de l'ocean... 
    public void initializeOceanGrid()
    {
        int nombre=1;
        oceangrid.setLayout(new GridLayout(taille,taille));
        for (int i = 0; i < taille; i++) 
        {
            for (int j = 0; j < taille; j++) 
            {
                OceanArray[i][j]=new JLabel(String.valueOf(nombre++));                
                OceanArray[i][j].setForeground(java.awt.Color.LIGHT_GRAY);
                OceanArray[i][j].setHorizontalAlignment(javax.swing.JLabel.CENTER);
                OceanArray[i][j].setSize(70,50);       
                oceangrid.add(OceanArray[i][j]);             
            }
            
        }
        validate();        
    }
    
    //j'initialise mon ocean en integrant mes bateaux aleatoirement :-) #SQUAAAAD X
    //PS GUIMO C'EST UN BOSSSS!
    public void initializeship()
    {
   
        Image img = Toolkit.getDefaultToolkit().createImage("images/orig1.gif").getScaledInstance(70,50, WIDTH);
        
        oceangrid.setLayout(new GridLayout(taille,taille));

            for (int j = 0; j < 5; j++) 
            {        
                Random r = new Random();
                int nb1= r.nextInt(9 - 0 + 1) + 0;
                int nb2=r.nextInt(9 - 0 + 1) + 0;
                
                OceanArray[nb1][nb2].setFont(new Font("Serif", Font.PLAIN, 0));
                OceanArray[nb1][nb2].setIcon(new ImageIcon(img));                
                listbateaux[j]=nb1+nb2;
            }

        validate();        
    } 

    //cette fontion prendra en paramètre le missile (ou message de l'adversaire)
    public static void upgrageOceanGrid(int incomming)
    {
        Boolean missHit = false;
        int cnt=0;
        Image img0 = Toolkit.getDefaultToolkit().createImage("images/explosion.gif").getScaledInstance(70,50, WIDTH);
        Image img1 = Toolkit.getDefaultToolkit().createImage("images/done.png").getScaledInstance(25,25, WIDTH);
        Image img2 = Toolkit.getDefaultToolkit().createImage("images/skull.png").getScaledInstance(70,45, WIDTH);
        for(int i = 0; i<taille;i++)
        {
            for(int j = 0; j<taille;j++)
            {
                
                int nbb=Integer.valueOf(OceanArray[i][j].getText());
                if(nbb==incomming)
                {
                        OceanArray[i][j].setFont(new Font("Serif", Font.PLAIN, 0));
                        OceanArray[i][j].setIcon(new ImageIcon(img0));
                       // OceanArray[i][j].setIcon(new ImageIcon(img1));
                         //OceanArray[i][j].setIcon(new ImageIcon(img2));                               
                }
            } 
        }

//        for(JLabel element:OceanArray)
//        {
//            if(!element.equals(listbtn[incomming]))
//                missHit = true;
//        }
//
//        if(missHit)
//        {
//            Image img = Toolkit.getDefaultToolkit().createImage("images/explosion.gif").getScaledInstance(70,50, WIDTH);
//            
//            OceanArray[incomming].(setBackground(java.awt.Color.magenta)); 
//        } 
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        playbutton = new java.awt.Button();
        label1 = new java.awt.Label();
        portinput = new javax.swing.JTextField();
        label3 = new java.awt.Label();
        ipinput = new javax.swing.JTextField();
        label4 = new java.awt.Label();
        radioclient = new javax.swing.JRadioButton();
        radioserver = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        msglabel = new java.awt.Label();
        msglabel1 = new java.awt.Label();
        jPanel2 = new javax.swing.JPanel();
        buttongrid = new javax.swing.JPanel();
        oceangrid = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        playbutton.setBackground(new java.awt.Color(0, 153, 0));
        playbutton.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        playbutton.setLabel("PLAY");
        playbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playbuttonActionPerformed(evt);
            }
        });

        label1.setAlignment(java.awt.Label.RIGHT);
        label1.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        label1.setText("Port");

        portinput.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        portinput.setEnabled(false);

        label3.setAlignment(java.awt.Label.RIGHT);
        label3.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        label3.setText("IP");

        ipinput.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        ipinput.setEnabled(false);

        label4.setAlignment(java.awt.Label.CENTER);
        label4.setBackground(new java.awt.Color(255, 153, 51));
        label4.setFont(new java.awt.Font("Cooper Black", 1, 36)); // NOI18N
        label4.setText("BATTLE SHIP");

        buttonGroup1.add(radioclient);
        radioclient.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        radioclient.setText("Player2");
        radioclient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioclientActionPerformed(evt);
            }
        });

        buttonGroup1.add(radioserver);
        radioserver.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        radioserver.setText("Player1");
        radioserver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioserverActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        msglabel.setFont(new java.awt.Font("Franklin Gothic Heavy", 1, 12)); // NOI18N
        msglabel.setForeground(new java.awt.Color(102, 102, 102));

        msglabel1.setFont(new java.awt.Font("Franklin Gothic Heavy", 1, 12)); // NOI18N
        msglabel1.setForeground(new java.awt.Color(102, 102, 102));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(msglabel, javax.swing.GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE)
                    .addComponent(msglabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(msglabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(msglabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        msglabel.getAccessibleContext().setAccessibleName("msglabel");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(label4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ipinput, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(portinput, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(radioclient)
                    .addComponent(radioserver))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(playbutton, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(playbutton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(radioserver, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(radioclient, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(label4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 61, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(label1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(portinput, javax.swing.GroupLayout.Alignment.TRAILING))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(ipinput, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(label3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        buttongrid.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout buttongridLayout = new javax.swing.GroupLayout(buttongrid);
        buttongrid.setLayout(buttongridLayout);
        buttongridLayout.setHorizontalGroup(
            buttongridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        buttongridLayout.setVerticalGroup(
            buttongridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 316, Short.MAX_VALUE)
        );

        oceangrid.setBackground(new java.awt.Color(0, 153, 255));
        oceangrid.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout oceangridLayout = new javax.swing.GroupLayout(oceangrid);
        oceangrid.setLayout(oceangridLayout);
        oceangridLayout.setHorizontalGroup(
            oceangridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 441, Short.MAX_VALUE)
        );
        oceangridLayout.setVerticalGroup(
            oceangridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(oceangrid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttongrid, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(oceangrid, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttongrid, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void radioclientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioclientActionPerformed
        ipinput.setEnabled(true);
        portinput.setEnabled(true);
        player1=false;
        bk.modeserver=false;
    }//GEN-LAST:event_radioclientActionPerformed

    private void radioserverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioserverActionPerformed
            try {
                InetAddress ip = InetAddress.getLocalHost();
                ipinput.setText(ip.getHostAddress());
            } catch (UnknownHostException ex) {
                Logger.getLogger(battleview.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        ipinput.setEnabled(false);
        portinput.setEnabled(true);
        player1=true;
        bk.modeserver=true;
    }//GEN-LAST:event_radioserverActionPerformed

    private void playbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_playbuttonActionPerformed
        
        int port=Integer.parseInt(portinput.getText());
        String ip=ipinput.getText();
        backgroundThread bk1 = new backgroundThread();
        
        if (player1==true) {
            bk1.startServer(port);
        } else {
            bk1.connectToServer(ip, port);
        }
            
    }//GEN-LAST:event_playbuttonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(battleview.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(battleview.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(battleview.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(battleview.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new battleview().setVisible(true);
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    public static javax.swing.JPanel buttongrid;
    public static javax.swing.JTextField ipinput;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private java.awt.Label label1;
    private java.awt.Label label3;
    private java.awt.Label label4;
    public static java.awt.Label msglabel;
    public static java.awt.Label msglabel1;
    public static javax.swing.JPanel oceangrid;
    private java.awt.Button playbutton;
    public static javax.swing.JTextField portinput;
    private javax.swing.JRadioButton radioclient;
    private javax.swing.JRadioButton radioserver;
    // End of variables declaration//GEN-END:variables


}
