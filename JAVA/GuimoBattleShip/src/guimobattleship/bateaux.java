package guimobattleship;


import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tikol
 */
public class bateaux {
    
    public ArrayList<JLabel> BoatSections;
    int status;
    int nbhit;
    
    public bateaux(ArrayList<JLabel> BoatSection)
    {
        this.BoatSections = new ArrayList<JLabel>();
        this.BoatSections = BoatSection;
        this.status = 0;
        this.nbhit = 0;
        initShip();
    }
    
    
    public int getStatus()
    {
       return this.status;
    }

    public int getNbhit() {
        return this.nbhit;
    }

    

    public void initShip()
    {
       
        for(JLabel section:this.BoatSections)
        {
            section.setBackground(java.awt.Color.green);
        }       
    }


   public void checkHit(JButton cible)
   {
      
       for(JLabel section:this.BoatSections)
       {
           if (section.equals(cible))
           {
             
             this.nbhit++;
             cible.setBackground(java.awt.Color.red);
            
           }      
       }
   }

   


}
