/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guimobattleship;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Tikol
 */
public class backgroundThread implements Runnable {

    private bateaux B;
    private static ServerSocket ss;
    private static Socket s;
    private static Socket s1;
    private Scanner reader;
    private PrintStream writer;

    public int missileIncomming;
    public int missileOutgoing;
    public Boolean dataToSend;
    public Boolean modeserver;
    private InetAddress ips;

    //connexion pour le server
    public void startServer(int port) {

        try {
            ips = InetAddress.getLocalHost();

            //je demare mon server
            ss = new ServerSocket(port);
            JOptionPane.showMessageDialog(null, "IP: " + ips + "\n"+"Port: " + ss.getLocalPort());

            //j'attend une connexion...
            battleview.msglabel.setText("En attente de player 2...");
            s = ss.accept();
            JOptionPane.showMessageDialog(null, "Server accept connection from" + s.getInetAddress().getHostAddress());

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }

        battleview.msglabel.setText("En connexion avec player2...");
    }

    //connexion pour le client
    public void connectToServer(String ip, int port) {
        try {
            //demande connexion au serveur par ip et numero de port
            s1 = new Socket(ip, port);

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        battleview.msglabel.setText("En connexion avec player1...");

    }

    @Override
    public void run() {

        if (modeserver) {
            
            try {
               
                writer = new PrintStream(s.getOutputStream());

            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }

            try {

                reader = new Scanner(s.getInputStream());

                
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
            while (true) {

                synchronized (dataToSend) {
                                    
                    if (dataToSend) {
                        
                        writer.println(missileOutgoing);
                        battleview.msglabel.setText("Cible: " + String.valueOf(missileOutgoing) + " Retour: "+missileIncomming + " Statut: ");
                        dataToSend = false;
                    } else {
                        writer.println(101);
                    }
                }
                    missileIncomming = reader.nextInt();
                if (missileIncomming >= 1 && missileIncomming < 101) {
                    battleview.msglabel.setText("Cible: " + " Retour: " + missileIncomming + " Statut: ");
                    battleview.upgrageOceanGrid(missileIncomming);
                }
                synchronized (dataToSend) {
                                    
                    if (dataToSend) {
                        
                        writer.println(missileOutgoing);
                        battleview.msglabel.setText("Cible: " + missileOutgoing + " Retour: " + " Statut: ");
                        dataToSend = false;
                    } else {
                        writer.println(101);
                    }
                }
            }

        } else {

            try {
                
                reader = new Scanner(s1.getInputStream());

            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }

            try {
                
                writer = new PrintStream(s1.getOutputStream());

            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
            while (true) {
                
                    missileIncomming = reader.nextInt();
                    
                if (missileIncomming >= 1 && missileIncomming < 101) {
                    battleview.msglabel.setText("Cible: " + " Retour: " + missileIncomming + " Statut: ");
                    battleview.upgrageOceanGrid(missileIncomming);
                }

                synchronized (dataToSend) {
                    if (dataToSend) {
                        writer.println(missileOutgoing);
                        battleview.msglabel.setText("Cible: " + String.valueOf(missileOutgoing) + " Retour: " + " Statut: ");
                        dataToSend = false;
                    } else {
                        writer.println(101);
                    }
                }
            }
        }

    }

}
