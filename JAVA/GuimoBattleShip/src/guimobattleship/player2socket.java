/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guimobattleship;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tikol
 */
class player2socket {
    
int id;
int sendmissile;
int receivemissile;

    public player2socket() {
        
        int port=Integer.parseInt(battleview.portinput.getText());
        String ip=battleview.ipinput.getText().toString();
        try 
        {
          //demande connexion au serveur par ip et numero de port
          Socket s =new Socket(ip,port);
          battleview.msglabel.setText("En connexion avec player1...");
         
          InputStream is = s.getInputStream();
          OutputStream os = s.getOutputStream();
          //envois de message au serveur           
          Scanner scan=new Scanner(System.in);
          int nb=scan.nextInt();  
          os.write(nb);
          //j'attend le message du serveur
          int rep=is.read();
          battleview.msglabel.setText("Player1: "+rep);
        } 
        catch(IOException ex) 
        {
            Logger.getLogger(player2socket.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    
}
