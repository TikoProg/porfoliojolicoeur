/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epiceriestrategy;

/**
 *
 * @author Guimaev Jolicoeur
 */
public class Legumes extends Produits {
 
private String type;    

    public Legumes(String type, String nom, double prix) {
        super(nom, prix);
        this.type = type;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
}
