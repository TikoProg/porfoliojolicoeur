/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epiceriestrategy;

/**
 *
 * @author Guimaev Jolicoeur
 */
public interface IUpdateProduits {
    
    public void updateLegumes(Legumes L);
    public void updateViandes(Viandes V);
    public void updateLessives(Lessives LE);
}
