/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epiceriestrategy;

/**
 *
 * @author Guimaev Jolicoeur
 */
public class Viandes extends Produits implements Taxes {
  
    private String animal;

    public Viandes(String animal, String nom, double prix) {
        super(nom, prix);
        this.animal = animal;
    }

    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    @Override
    public double CalculTaxes() {
        double taxe;
        taxe=0.20;
        return taxe;
    }
    
    

}
