/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epiceriestrategy;

/**
 *
 * @author Guimaev Jolicoeur
 */

public class txtadder implements IAddProduits,IUpdateProduits,ISelectProduits,IDeleteProduits 
{
    
    String category= addFrame.category.getItemAt(0);
    String nom = addFrame.nomField.getText();
    String type=addFrame.abtField.getText();  
    String animal=addFrame.abtField.getText();
    String marque=addFrame.abtField.getText();
    double prix = Double.parseDouble(addFrame.priceField.getText());
    
    Legumes L = new Legumes(nom,type,prix);
    Viandes V = new Viandes(nom,animal,prix);
    Lessives LE = new Lessives(nom,marque,prix);

    @Override
    public void insertLegumes(Legumes L) {
        this.L=L;
    }

    @Override
    public void insertViandes(Viandes V) {
        this.V=V;    
    }

    @Override
    public void insertLessives(Lessives LE) {
        this.LE=LE;
    }

    @Override
    public void updateLegumes(Legumes L) {
        this.L=L;
    }

    @Override
    public void updateViandes(Viandes V) {
        this.V=V;     
    }

    @Override
    public void updateLessives(Lessives LE) {
        this.LE=LE;    
    }

    @Override
    public void selectLegumes(Legumes L) {

    }

    @Override
    public void selectViandes(Viandes V) {

    }

    @Override
    public void selectLessives(Lessives LE) {
  
    }

    @Override
    public void deleteLegumes(Legumes L) {
        this.L=L;
    }

    @Override
    public void deleteViandes(Viandes V) {
        this.V=V; 
    }

    @Override
    public void deleteLessives(Lessives LE) {
        this.LE=LE;    
    }
     
}
