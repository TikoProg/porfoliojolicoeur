/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epiceriestrategy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Tikol
 */
public class Epicerie_datagateway 
{
    
    private Connection conn;
    private PreparedStatement 
            select_legumes,select_viandes,select_lessives,
            insert_legumes,insert_viandes,insert_lessives,
            update_legumes,update_viandes,update_lessives,            
            delete_prod;
    private ResultSet result;
    private String url,user,pwd;

    public Epicerie_datagateway(String url, String user, String pwd) {
        this.url = url;
        this.user = user;
        this.pwd = pwd;
        
        try
        {
            conn = DriverManager.getConnection(this.url, this.user, this.pwd);
            
            update_legumes= conn.prepareStatement("update legumes set nom=?, type=?, prix=? where nom=?");
            update_viandes= conn.prepareStatement("update viandes set nom=?, type=?, prix=? where nom=?");
            update_lessives= conn.prepareStatement("update lessives set nom=?, type=?, prix=? where nom=?");
            
            select_legumes=conn.prepareStatement("select nom,type,prix from legumes where nom=? ");
            select_viandes=conn.prepareStatement("select nom,animal,prix from viandes where nom=? ");
            select_lessives=conn.prepareStatement("select nom,marque,prix from lessives where nom=? ");
            
            insert_legumes= conn.prepareStatement("insert into legumes(nom,type,prix)"+"values(?,?,?);");
            insert_viandes= conn.prepareStatement("insert into viandes(nom,animal,prix)"+"values(?,?,?);");
            insert_lessives= conn.prepareStatement("insert into lessives(nom,marque,prix)"+"values(?,?,?);");
             
            delete_prod= conn.prepareStatement("delete * from ? where id=?"); 
             
        }
        catch(SQLException ex)
        {
            
            Logger.getLogger(Epicerie_datagateway.class.getName()).log(Level.SEVERE, null, ex);        
        }
        finally
        {
            JOptionPane.showMessageDialog(null,"Connected to DataBase Successfully ");
        }
        
    }
    
    //mes selecters............................................................... 
    public ArrayList<Legumes> select_legumes(String nom)
    {
      
     ArrayList<Legumes> ProdList= new ArrayList<>();
     try
      {
        select_legumes.setString(1,nom);
        
        result = select_legumes.executeQuery();
        while(result.next())
        {
                 
//            int i = result.getInt("id");
            String n = result.getString("nom");
            String t = result.getString("type");
            double p = result.getDouble("prix");

                 
            ProdList.add(new Legumes(n,t,p)); 
                   
        }
                 
      }
      catch(SQLException ex)
      {
       Logger.getLogger(Epicerie_datagateway.class.getName()).log(Level.SEVERE, null, ex);        
      }

       return ProdList;
    }

    public ArrayList<Viandes> select_viandes(String nom)
    {
      
     ArrayList<Viandes> ProdList= new ArrayList<>();
     try
      {
        select_viandes.setString(1,nom);
        result = select_viandes.executeQuery();
        while(result.next())
        {
                 
//            int i = result.getInt("id");
            String n = result.getString("nom");
            String a = result.getString("animal");
            double p = result.getDouble("prix");

                 
            ProdList.add(new Viandes(n,a,p)); 
                   
        }
                 
      }
      catch(SQLException ex)
      {
       Logger.getLogger(Epicerie_datagateway.class.getName()).log(Level.SEVERE, null, ex);        
      }

       return ProdList;
    } 
    
    public ArrayList<Lessives> select_lessives(String nom)
    {
      
     ArrayList<Lessives> ProdList= new ArrayList<>();
     try
      {
        select_lessives.setString(1,nom);
        result = select_lessives.executeQuery();
        while(result.next())
        {
                 
//            int i = result.getInt("id");
            String n = result.getString("nom");
            String m = result.getString("marque");
            double p = result.getDouble("prix");

                 
            ProdList.add(new Lessives(n,m,p)); 
                   
        }
                 
      }
      catch(SQLException ex)
      {
       Logger.getLogger(Epicerie_datagateway.class.getName()).log(Level.SEVERE, null, ex);        
      }

       return ProdList;
    }
//.................................................................................................
           
           
    //mes inserters..................................................................   
    public int insert_legumes(String nom,String type,Double prix)
    {
      int nb_rows=0;
      
        try 
        {
            insert_legumes.setString(1, nom);
            insert_legumes.setString(2, type);
            insert_legumes.setDouble(3, prix);
            insert_legumes.executeUpdate();

        } 
        catch (SQLException e) 
        {
            Logger.getLogger(Epicerie_datagateway.class.getName()).log(Level.SEVERE, null, e); 
        }
        return nb_rows;
 
    }

        public int insert_viandes(String nom,String animal,Double prix)
    {
      int nb_rows=0;
        try 
        {
            insert_viandes.setString(1, nom);
            insert_viandes.setString(2, animal);
            insert_viandes.setDouble(3, prix);

            insert_viandes.executeUpdate();
            nb_rows++;
        } 
        catch (SQLException e) 
        {
            Logger.getLogger(Epicerie_datagateway.class.getName()).log(Level.SEVERE, null, e); 
        }
        return nb_rows;
 
    }
        
        public int insert_lessives(String nom,String marque,Double prix)
    {
      int nb_rows=0;
        try 
        {
            insert_lessives.setString(1, nom);
            insert_lessives.setString(2, marque);
            insert_lessives.setDouble(3, prix);

            insert_lessives.executeUpdate();
            nb_rows++;
        } 
        catch (SQLException e) 
        {
            Logger.getLogger(Epicerie_datagateway.class.getName()).log(Level.SEVERE, null, e); 
        }
        return nb_rows;
 
    }
    //................................................................................
        
        
    //mes updaters....................................................................
        
    public int update_legumes(String nom,String type,Double prix)
    {
      int nb_rows=0;
        try 
        {
            
            update_legumes.setString(1, nom);
            update_legumes.setString(2, type);
            update_legumes.setDouble(3, prix);
            update_legumes.setString(4,nom);
            
            update_legumes.executeUpdate();
            nb_rows++;
        } 
        catch (SQLException e) 
        {
            Logger.getLogger(Epicerie_datagateway.class.getName()).log(Level.SEVERE, null, e); 
        }
        return nb_rows;
 
    }

    public int update_viandes(String nom,String animal,Double prix)
    {
      int nb_rows=0;
        try 
        {
            update_viandes.setString(1, nom);
            update_viandes.setString(2, animal);
            update_viandes.setDouble(3, prix);
            update_viandes.setString(4, nom);
            
            update_viandes.executeUpdate();
            nb_rows++;
        } 
        catch (SQLException e) 
        {
            Logger.getLogger(Epicerie_datagateway.class.getName()).log(Level.SEVERE, null, e); 
        }
        return nb_rows;
 
    }
        
    public int update_lessives(String nom,String marque,Double prix)
    {
      int nb_rows=0;
        try 
        {
            
            update_lessives.setString(1, nom);
            update_lessives.setString(2, marque);
            update_lessives.setDouble(3, prix);
            update_viandes.setString(4, nom);
            
            update_lessives.executeUpdate();
            nb_rows++;
        } 
        catch (SQLException e) 
        {
            Logger.getLogger(Epicerie_datagateway.class.getName()).log(Level.SEVERE, null, e); 
        }
        return nb_rows;
 
    }
    //.......................................................................................
    
    //ma delete fonction
    public int delete_prod(String nom)
    {
        int nb_rows=0;
        try 
        {
            
            delete_prod.setString(1, nom);
            
            delete_prod.executeQuery();
            nb_rows++;
        } 
        catch (SQLException e) 
        {
            Logger.getLogger(Epicerie_datagateway.class.getName()).log(Level.SEVERE, null, e); 
        }
        return nb_rows;
   
    }
}
