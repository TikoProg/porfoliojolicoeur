/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epiceriestrategy;

/**
 *
 * @author Guimaev Jolicoeur
 */
public class Lessives extends Produits implements Taxes {
    
    private String marque;


    public Lessives(String marque, String nom, double prix) {
        super(nom, prix);
        this.marque = marque;
    }
        
    public String getMarque() 
    {
        return marque;
    }

    public void setMarque(String marque) 
    {
        this.marque = marque;
    }

    @Override
    public double CalculTaxes() {
        double taxe;
        taxe=0.10;
        return taxe;
    }    
    
}
