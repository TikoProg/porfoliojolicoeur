 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package epiceriestrategy;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Tikol
 */
public class mysqlAdder implements IAddProduits,IUpdateProduits,ISelectProduits,IDeleteProduits 
{
    
    private ArrayList<Lessives> lessiveList= new ArrayList<>();
    private ArrayList<Viandes> viandeList= new ArrayList<>();
    
    private String catego= addFrame.category.getItemAt(0);
    private String nom = addFrame.nomField.getText();
    private String type=addFrame.abtField.getText();  
    private String animal=addFrame.abtField.getText();
    private String marque=addFrame.abtField.getText();
    private double prix = Double.parseDouble(addFrame.priceField.getText());
    
    private String url ="jdbc:mysql://localhost:3306/epiceriedb";
    private String user ="root";
    private String pwd="";
    
    Legumes L = new Legumes(nom,type,prix);
    Viandes V = new Viandes(nom,animal,prix);
    Lessives LE = new Lessives(nom,marque,prix);
    
    public Epicerie_datagateway dgw = new Epicerie_datagateway(url, user, pwd);

    @Override
    public void insertLegumes(Legumes L) 
    {
    
        dgw.insert_legumes(nom, type, prix);
       
        if (dgw.equals(0))
        {
            JOptionPane.showMessageDialog(null, "Ooops!", "Error",JOptionPane.ERROR_MESSAGE);
        } 
        else
        {
           JOptionPane.showMessageDialog(null, "New item added to the database! :-)", "Success",JOptionPane.PLAIN_MESSAGE); 
        }    
    
    }

    @Override
    public void insertViandes(Viandes V) 
    {
        dgw.insert_viandes(nom, animal, prix);
        if (dgw.equals(0))
        {
            JOptionPane.showMessageDialog(null, "Ooops!", "Error",JOptionPane.ERROR_MESSAGE);
        } 
        else
        {
           JOptionPane.showMessageDialog(null, "New item added to Carnet database! :-)", "Success",JOptionPane.PLAIN_MESSAGE); 
        }  
    }

    @Override
    public void insertLessives(Lessives LE) {
        
        dgw.insert_lessives(nom, marque, prix);
        
        if (dgw.equals(0))
        {
            JOptionPane.showMessageDialog(null, "Ooops!", "Error",JOptionPane.ERROR_MESSAGE);
        } 
        else
        {
           JOptionPane.showMessageDialog(null, "New item added to Carnet database! :-)", "Success",JOptionPane.PLAIN_MESSAGE); 
        }  
    }

//.....................................SELECTERS..................................    
    @Override
    public void selectLegumes(Legumes L) {
        
        ArrayList<Legumes> legumeList= new ArrayList<>();
        legumeList=dgw.select_legumes(showFrame.name_inpt.getText());
        
    }

    @Override
    public void selectViandes(Viandes V) {
        dgw.select_viandes(nom);
    }

    @Override
    public void selectLessives(Lessives LE) {
        dgw.select_lessives(nom);
    }

    @Override
    public void updateLegumes(Legumes L) {
        dgw.update_legumes(nom, type, prix);
    }

    @Override
    public void updateViandes(Viandes V) {
        dgw.update_viandes(nom, animal, prix);
    }

    @Override
    public void updateLessives(Lessives LE) {
        dgw.update_lessives(nom, marque, prix);
    }

    @Override
    public void deleteLegumes(Legumes L) {
       dgw.delete_prod(nom);
    }

    @Override
    public void deleteViandes(Viandes V) {
        dgw.delete_prod(nom);
    }

    @Override
    public void deleteLessives(Lessives LE) {
        dgw.delete_prod(nom);
    }

    
}
