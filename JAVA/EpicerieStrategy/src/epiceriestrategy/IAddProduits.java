/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epiceriestrategy;

/**
 *
 * @author Guimaev Jolicoeur
 */
public interface IAddProduits {
    
    public void insertLegumes(Legumes L);
    public void insertViandes(Viandes V);
    public void insertLessives(Lessives LE);
}
