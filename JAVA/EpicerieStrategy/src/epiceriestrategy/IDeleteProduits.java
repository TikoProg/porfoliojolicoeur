/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epiceriestrategy;

/**
 *
 * @author Tikol
 */
public interface IDeleteProduits {
    
    public void deleteLegumes(Legumes L);
    public void deleteViandes(Viandes V);
    public void deleteLessives(Lessives LE); 
}
