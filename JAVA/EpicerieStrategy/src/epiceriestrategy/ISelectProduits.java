/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epiceriestrategy;

/**
 *
 * @author Guimaev Jolicoeur
 */
public interface ISelectProduits {
    
    public void selectLegumes(Legumes L);
    public void selectLessives(Lessives LE);
    public void selectViandes(Viandes V);

}
