/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epiceriestrategy;

/**
 *
 * @author Guimaev Jolicoeur
 */
public abstract class Produits{

    private String nom;
    private double prix;
    //private String photo;
    
    protected Produits(String nom, double prix) {
        this.nom = nom;
        this.prix = prix;
    }
        
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }
    
    
  
}
