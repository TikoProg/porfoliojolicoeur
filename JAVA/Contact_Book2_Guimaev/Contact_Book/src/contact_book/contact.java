/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contact_book;

/**
 *
 * @author Tikol
 */
public class contact {
    
        
    private int id;
    private String nom;
    private String prenom;
    private String email;
    private String phone;    

    //constructor vide
    public contact()
    {
        this.id=0;
        this.nom="";
        this.prenom="";
        this.email="";
        this.phone="";         
    }
    
    //constructor
    public contact(int id,String nom,String prenom,String email,String phone)
    {
        this.id=id;
        this.nom=nom;
        this.prenom=prenom;
        this.email=email;
        this.phone=phone;         
    }

    //getters 
    public int get_id()
    {
        return this.id;
    }
    public String get_nom()
    {
        return this.nom;
    }
    public String get_prenom()
    {
        return this.prenom;
    }        
    public String get_email()
    {
        return this.email;
    }      
    public String get_phone()
    {
        return this.phone;
    }
    
    // seters 
    public void set_id(int id)
    {
        this.id = id;
    }
        public void set_nom(String nom)
    {
        this.nom = nom;
    }
    public void set_prenom(String prenom)
    {
        this.prenom = prenom;
    }

    public void set_email(String email)
    {
        this.email = email;
    }
    
    public void set_phone(String phone)
    {
        this.phone = phone;
    }
}
