/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contact_book;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Tikol
 */
public class Contact_datagateway 
{
    private Connection conn;
    private PreparedStatement insert_contact,search,brows;
    private ResultSet result;
    private String url,user,pwd;
    
   public Contact_datagateway(String url, String user, String pwd)
    {
       this.url = url;
       this.user = user;
       this.pwd = pwd;
         
      try
      {
        conn = DriverManager.getConnection(this.url, this.user, this.pwd);
        brows= conn.prepareStatement("select * from contact");
        insert_contact= conn.prepareStatement("insert into contact(nom,prenom,email,phone)"+"values(?,?,?,?);");
        search=conn.prepareStatement("select * from contact where nom=? ");
             
      }
      catch(SQLException ex)
      {
        JOptionPane.showMessageDialog(null,"Connected to DataBase Successfully ");
        Logger.getLogger(Contact_datagateway.class.getName()).log(Level.SEVERE, null, ex);        
      }
               
    }
   
    public ArrayList<contact> brows()
    {
      
     ArrayList<contact> ContactList= new ArrayList<contact>();
     try
      {
        
        result = brows.executeQuery();
        while(result.next())
        {
                 
            int i = result.getInt("id");
            String n = result.getString("nom");
            String pr = result.getString("prenom");
            String e = result.getString("email");
            String ph = result.getString("phone");
                 
            ContactList.add(new contact(i,n,pr,e,ph)); 
                   
        }
                 
      }
      catch(SQLException ex)
      {
       Logger.getLogger(Contact_datagateway.class.getName()).log(Level.SEVERE, null, ex);        
      }

       return ContactList;
    }

    
    public ArrayList<contact> search(String nom)
    {
        ArrayList<contact> ContactList= new ArrayList<contact>();

        try 
        {
            search.setString(1,nom);
            result = search.executeQuery();
                        
            while(result.next())
            {
                 
                int i = result.getInt("id");
                String n = result.getString("nom");
                String pr = result.getString("prenom");
                String e = result.getString("email");
                String ph = result.getString("phone");
                  
                ContactList.add(new contact(i,n,pr,e,ph)); 
                   
            } 
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(Contact_datagateway.class.getName()).log(Level.SEVERE, null, ex);   
        }
        return ContactList;
        
    }
    
    public int insert_contact(String nom,String prenom,String email, String phone)
    {
      int nb_rows=0;
        try 
        {
            insert_contact.setString(1, nom);
            insert_contact.setString(2, prenom);
            insert_contact.setString(3, email);
            insert_contact.setString(4, phone);
            insert_contact.executeUpdate();

        } 
        catch (SQLException e) 
        {
            Logger.getLogger(Contact_datagateway.class.getName()).log(Level.SEVERE, null, e); 
        }
        return nb_rows;
 
    }

}
