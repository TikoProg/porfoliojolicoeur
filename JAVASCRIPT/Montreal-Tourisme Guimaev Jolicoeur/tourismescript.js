function mySubmit()
{
	
	var n= document.getElementById("nom");
	var ht= document.getElementById("hotel");
	var rm = document.getElementById("roomtype");
	var nbpers= document.getElementById("nbp");
	var day = document.getElementById("duration");
	var ext= document.getElementsByClassName("extras");
	
	var selectedHotel = ht.options[ht.selectedIndex].value;

	var rep1="";
	var rep2="";
	var rep3="";
	
	for(var i=0; i<ht.length && i<rm.length && i<ext.length; i++)
	{
		if( ht[i].selected || rm[i].selected || ext[i].checked)
		{
			rep1= ht[i].value;
			rep2= rm[i].value;
			rep3+= ext[i].value + "";
			
		}

	}
	
	if(n.value=="") 
	{
		document.getElementById("d_nom").innerHTML +="<font color='red'>	Missing name!</font>";
	}
	else if(nbpers.value=="")
	{
		document.getElementById("d_pers").innerHTML +="<font color='red'>	Missing number!</font>";
	}
	else if(nbpers.value>=2)
	{
		document.getElementById("d_pers").innerHTML +="<font color='red'>	More than 2 not allowed!</font>";
	}
	else if(day.value=="")
	{
		document.getElementById("d_days").innerHTML +="<font color='red'>	Missing days!</font>";
	}
	else if(selectedHotel=="Select Type") 
	{
		document.getElementById("d_hotel").innerHTML +="<font color='red'>	Missing hotel type!</font>";
	}
	else if(rm.value==false)
	{
		document.getElementById("d_room").innerHTML +="<font color='red'>Missing room type!</font>";
	}
	else if(rep3!=="")
	{	
		var soustotal = 0.0,total = 0.0, tps =0.0 , tvq =0.0 ;
        var sum=0.0;
		
		if(ext[0].checked)
		{
			sum+=20;
		}
		else if(ext[1].checked)
		{
			sum+=80;
		}
		else if(ext[2].checked)
		{
			sum+=200;
		}
		soustotal=sum;
		tvq=soustotal*0.09975;
		tps=soustotal*0.05;
		total = tps+ tvq + soustotal;
		document.getElementById("div1").innerHTML +="<p id='p1' align='center'>Mr./Mrs. "+ n.value +"</br> You reserved a "+ rep2 + " for  "+ nbpers.value  +" persons in a "+rep1+" hotel for "+day.value+" days.</br> Extras: "+ rep3 +"<p><br/>";
		document.getElementById("p1").innerHTML +="<p id='p1' align='center'>Sous-total: "+ soustotal +"$</br> Tps: "+ tps + "$</br> Tvq: "+ tvq  +"$</br> Total: "+total+"$<p><br/>";
	}
	else 
	{
		document.getElementById("div1").innerHTML +="<p id='p1' align='center'>Mr./Mrs. "+ n.value +"</br> You reserved a "+ rep2 + " for  "+ nbpers.value  +" persons in a "+rep1+" hotel for "+day.value+" days.</br> Extras: "+ rep3 +"<p><br/>";
	}
	
	
}

function myReset()
{
	document.getElementById("p1").innerHTML = "";
	document.getElementById("myForm").reset();
}