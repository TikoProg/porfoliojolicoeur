$(document).ready(function(){

    var cnt = 2;

	$("#addButton").click(function () {

		if(cnt>10)
		{
			alert("Seulement 10 textboxes est permis");
			return false;
		}

		var nouvelle_ligne = $(document.createElement('div')).attr("id", 'input_div' + cnt);

		nouvelle_ligne.after().html('<label>Textbox #'+ cnt + ' : </label>' +'<input type="text" name="textbox' + cnt +'" id="textbox' + cnt + '" value="" >');

		nouvelle_ligne.appendTo("#ligne");
		cnt++;
	});

	$("#removeButton").click(function () {
		if(cnt==1)
		{
			alert("Il ne reste plus de textbox");
			return false;
		}
		cnt--;

		$("#input_div" + cnt).remove();

	});

 $("#getButtonValue").click(function () {

	var reponse = '';
	for(var i=1; i<cnt; i++)
		{
			reponse += "\n Textbox #" + i + " : " + $('#textbox' + i).val();
		}
    	  alert(reponse);
    });
});