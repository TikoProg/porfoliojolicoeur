function showImage(obj)
{
	var name = document.getElementById("im1").src;
	document.getElementById("im1").src = obj.src;
	obj.src = name;
}
function rotateRight(param)
{
	param.style.transform=" rotate(360deg)";
	param.style.opacity=0.3;
}
function rotateBack(param)
{
	param.style.transform=" rotate(0deg)";
	param.style.opacity=1;
}

var TableauImage = new Array("./Images/bebe.jpg","./Images/bebe2.jpg","./Images/bebe3.jpg","./Images/bebe4.jpg");
var nextImage = 0;
function showNextImageInTab()
{
	if(nextImage <4)
	{
		document.getElementById("im1").src = TableauImage[nextImage];
		nextImage++;
	}
	else
	{
		nextImage = 0;
	}
}