$(document).ready(function (){
   
$("#mySubmit").click(function(){
var name= $("#nom").val();    
var hot = $("#hotel").val();
var rom = $("#roomtype").val();
var pers= $("#nbp").val();
var jours=$("#duration").val();
var ext=$(".extras").val();
    
var x = [];     
$(".extras:checked").each(function(){
    x.push($(this).val());
    }); 
    
  if($(x).length!==0)
	{	
		var soustotal = 0.0;
        var total = 0.0; 
        var tps =0.0 ; 
        var tvq =0.0 ;
        var sum=0.0;
		
		if($(ext[0]).checked==true)
		{
			sum+=20;
		}
		if($(ext[1]).checked==true)
		{
			sum+=80;
		}
		if($(ext[2]).checked==true)
		{
			sum+=200;
		}
		soustotal=sum;
		tvq=soustotal*0.09975;
		tps=soustotal*0.05;
		total = tps+ tvq + soustotal;
        
        $("#div2").html("<p id='p1' align='center'>Mr./Mrs. "+ name +"</br> You reserved a "+ rom.join(", ") + " for  "+ pers  +" persons in a "+hot+" hotel for "+jours+" days.</br> Extras: "+ x.join(", ") +"<p><br/>");
 
        $("#div3").html("<p id='p1' align='center'>Sous-total: "+ soustotal.toFixed(2) +"$</br> Tps: "+ tps.toFixed(2) + "$</br> Tvq: "+ tvq.toFixed(2)  +"$</br> Total: "+total.toFixed(2)+"$<p><br/>");
    }
    else if($(x).length==0 )
    {
        $("#div2").html("<p id='p1' align='center'>Mr./Mrs. "+ name +"</br> You reserved a "+ rom.join(", ") + " for  "+ pers  +" persons in a "+hot+" hotel for "+jours+" days.</br> No Extras has been selected! "+"<p><br/>");
    
    }
});  

    
$("#myReset").click(function(){
   $("p").html("");
	$("myForm").reset();
});

});
	
	