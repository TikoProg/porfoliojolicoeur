<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="../css/bouffestyle.css" />

<div id="entete">
<p align="center">Cuisine Internationale<br/>
12 rue Bonne Bouffe<br/>
Montréal</p>
</div>
<div align="center" id="menubars">
<ol>
	<li><a href="index.php?lien=acceuil">Acceuil</a></li>
	<li><a href="index.php?lien=login">Login</a></li>
	<li><a href="index.php?lien=recettes">Recettes</a></li>
	<li><a href="index.php?lien=contacts">Contacts</a></li>
	<li><a href="index.php?lien=references">Références</a></li>
</ol>
</div>

<!--details-->
<div align="center" >
<?php

if(isset($_GET["lien"]))
{
	$lien=$_GET["lien"];
	switch($lien)
	{
		case"acceuil":
			include("acceuil.php");
		break;
		case"login":
			include("login.php");
		break;
		case"recettes":
			include("recettes.php");
		break;
		case"inventaire":
			include("inventaire.php");
		break;
		case"contacts":
			include("contacts.php");
		break;
		case"references":
			include("references.php");
		break;
	
	}
}

?>
</div>