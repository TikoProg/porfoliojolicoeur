<meta charset="utf-8">
<title>ECOLE</title>
<link rel="stylesheet" type="text/css" href="../css/courstyle.css" />

<?php
//1) Demarre la session en HAUT DE LA PAGE
session_start();

//Tester si le code de session existe
if(!$_SESSION['Code'])
{
	header("location:../index.php");
}
//2) Recuperation des variables de SESSION
$code=$_SESSION["Code"];
$nom=$_SESSION["Nom"];
$prenom=$_SESSION["Prenom"];
$telephone=$_SESSION["Telephone"];
$photo=$_SESSION["Photo"];
$password=$_SESSION["Password"];

?>

<div id="d1" align="center">
<h1>ECOLE</h1>
<div id="welcom">
<?php 
echo "Bienvenue, ".$nom." ".$prenom."!";
?>
</div>

<form method="post">
<ul id="menu_bars" >
	<li><a href="indexetudiants.php?lien=accueil"><h1>Accueil</h1></a></li>
	<li><a href="indexetudiants.php?lien=inscription"><h1>Inscription</h1></a></li>
	<li><a href="indexetudiants.php?lien=recherche"><h1>Recherche</h1></a></li>
	<li><a href="indexetudiants.php?lien=login"><h1>Login</h1></a></li>
	<li><a href="indexetudiants.php?lien=administration"><h1>Administration</h1></a></li>
</ul>
</form>
</div>


<div align="center">
<?php
if(isset($_GET["lien"]))
{
	$lien=$_GET["lien"];
	switch($lien)
	{
		case"accueil":
			include("accueil.php");
		break;
		case"inscription":
			include("inscription.php");
		break;
		case"login":
			include("login.php");
		break;
		case"recherche":
			include("recherche.php");
		break;
		case"administration":
			include("administration.php");
		break;

	}
	
}

?>
</div>
