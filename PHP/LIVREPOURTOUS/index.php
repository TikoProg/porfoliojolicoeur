<!DOCTYPE HTML>
<html>

 <head>
  <title>Livres Pour tous</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="./css/style.css" />
  <script type="text/javascript" src="./monscript/jquery-3.2.0.min.js"></script>
  <script type="text/javascript" src="./monscript/scriptpourtous.js"></script>
 </head>
 

 
 <body>
	<header>
		<form style="float:left" id="search_bar" class="active">Recherche : <input id="search" name="search" value="" /><input type='submit' name="ok" value="OK"/></form>
		<div align="center">
			<form method="post">
				<ul id="menu_bar" >
				<li class="li2"><a href="index.php?lien=inscription">Inscription</a></li>
				<li class="li2"><a href="index.php?lien=login">Login</a></li>
				<li class="li2"><a href="index.php?lien=accueil">Accueil</a></li>
				</ul>
			</form>
		</div>
	</header>
 
	<div id="main" align="center">
	
		<div id="details"></div>
		<?php
	
		if(isset($_GET["lien"]))
		{
			$lien=$_GET["lien"];
			switch($lien)
			{
				case"accueil":
					include("accueil.php");
				break;
				case"inscription":
					include("inscription.php");
				break;
				case"login":
					include("login.php");
				break;
				case"recherche":
					include("recherche.php");
				break;
		
			}
			
		}
		else if(isset($_GET["ok"])) //recherche des recherches
		{
			
			include("recherche.php");
		
		}
		else
		{
			include("accueil.php");
		}
		?>
	</div>
	
	
	<footer>
		<p id="p_foot">About us!<br>Guimo&reg;<br>Email: guimoinc@hotmail.ca<br>Tel: 514 222-3333</p>
	</footer>
	
</body>
</html>