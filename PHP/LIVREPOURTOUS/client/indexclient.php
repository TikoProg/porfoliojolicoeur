<?php
//1) Demarre la session en HAUT DE LA PAGE
session_start();
?>
<!DOCTYPE HTML>
<html>
 <head>
  <title>Livres Pour Tous</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../css/style.css" />
  <script type="text/javascript" src="../monscript/jquery-3.2.0.min.js"></script>
  <script type="text/javascript" src="../monscript/scriptpourtous.js"></script>
 </head>
 <body>
 	<header>
		<div >
			<form style="float:left" id="search_bar" class="active">Recherche : <input id="search" name="search" value="" /><input type='submit' name="ok" value="OK"/></form>
		</div>
		
		<div align="center">
			<form method="post">
				<ul id="menu_bar" >
				<li><a href="indexclient.php?lien=usager">Usager</a></li>
				<li><a href="indexclient.php?lien=logout">Logout</a></li>
				<li><a href="indexclient.php?lien=accueil">Accueil</a></li>
				</ul>
			</form>
		</div>
	</header>
	
	<div id="main" align="center">
	
		<?php
		if(isset($_GET["lien"]))
		{
			$lien=$_GET["lien"];
			switch($lien)
			{
				case"accueil":
					include("accueil.php");
				break;
				case"inscription":
					include("inscription.php");
				break;
				case"logout":
					session_destroy();
					header("location:../index.php");
				break;
				case"recherche":
					include("recherche.php");
				break;
				case"usager":
					include("usager.php");
				break;
				case"enter":
					include("livres.php");
				break;
		
			}
			
		}
		else if(isset($_GET["ok"])) //recherche des recherches
		{
			
			include("recherche.php");
		
		}
		else
		{
			include("accueil.php");
		}
		?>
	</div>

	<footer>
		<p id="p_foot">Administrateur: Guimo&reg;<br>Email: guimoinc@hotmail.ca<br>Tel: 514 222-3333</p>
	</footer>
<?php 
$ipserver=$_SERVER["SERVER_ADDR"];
print<<<eof3
<script>
function impression(idvente)
{
 options = "menubar=no,status=no,location=no,directories=no,width=1024,height=600";
 window.open("http://$ipserver:8000/LIVREPOURTOUS/client/impression.php?idvente="+idvente);
}
function impression2()
{
 options = "menubar=no,status=no,location=no,directories=no,width=1024,height=600";
 window.open("http://$ipserver:8000/LIVREPOURTOUS/client/impression2.php",options);
}
</script>
eof3;

?>
</body>
</html>