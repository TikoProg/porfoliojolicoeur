<?php
//1) Demarre la session en HAUT DE LA PAGE
session_start();
?>
<meta charset="utf-8">
<title>Ma clinique</title>
<link rel="stylesheet" type="text/css" href="../css/cliniquestyle.css" />

<div id="entete" align="center">
<h1>MA CLINIQUE</h1>
<form method="post">
<ul id="menu_bars" >
	<li><a href="indexpatient.php?lien=accueil"><h1>Accueil</h1></a></li>
	<li><a href="indexpatient.php?lien=profile"><h1>Profile</h1></a></li>
	<li><a href="indexpatient.php?lien=rendezvous"><h1>Rendez-vous</h1></a></li>
	<li><a href="indexpatient.php?lien=logout"><h1>Logout</h1></a></li>
</ul>
</form>
</div>


<div id="corp" class="box">
<?php

if(isset($_GET["lien"]))
{
	$lien=$_GET["lien"];
	switch($lien)
	{
		case"accueil":
			include("accueil.php");
		break;
		case"profile":
			include("profile.php");
		break;
		case"rendezvous":
			include("rendezvous.php");
		break;
		case"logout":
		    session_destroy();
			header("location:../index.php");
		break;
	}
}
else
{
	include("accueil.php");	
}
?>
</div>