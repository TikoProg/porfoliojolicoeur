﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Le_Safari.Contact" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="css/StyleSheet1.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <script src="scripts/jquery-3.3.1.min.js"></script>
    <script src="scripts/validation.js"></script>
</head>
<body>

    <form class="contact" runat="server">
        <header>
            <nav>               
                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn1" PostBackUrl="~/Home.aspx">Home<i class="fa fa-angle-down"></i></asp:LinkButton>
                <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn1" PostBackUrl="~/About Us.aspx">About</asp:LinkButton>
                <asp:LinkButton ID="LinkButton4" runat="server" CssClass="btn1">Pages<i class="fa fa-angle-down"></i></asp:LinkButton>
                <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn1" PostBackUrl="~/Gallery.aspx">Gallery</asp:LinkButton>
                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn1">Blog</asp:LinkButton>
                <asp:LinkButton ID="LinkButton6" runat="server" CssClass="btn1" PostBackUrl="~/Contact.aspx">Contact</asp:LinkButton>        
            </nav>
            <div id="div1">
                <asp:Image ID="Image1" runat="server" ImageAlign="Left" ImageUrl="~/images/globe.png" Height="45px" Width="50px" CssClass="div1" />  <asp:Label ID="Label1" runat="server" Text="Safari Adventure" ForeColor="White" CssClass="div1"></asp:Label> <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Right" ImageUrl="~/images/loop.png" Height="45px" Width="50px" CssClass="div1" />
            </div>

        </header>
        
            <section class="contact_section1">

                <article class="contact_article1">
                    <p class="contact_p">
                        <asp:Label ID="Label2" runat="server" Text="ADDRESS:" CssClass="titre_contact"></asp:Label>
                    </p>
                    <p class="contact_p">
                        <i class="fa fa-home" style="font-size:26px;color:orangered;"> </i><asp:Label ID="Label6" runat="server" Text="38 Atlantis Ln Kongston Illinois 121164"></asp:Label>
                    </p>
                    <p class="contact_p">
                        <asp:Label ID="Label3" runat="server" Text="PHONES:" CssClass="titre_contact"></asp:Label>
                    </p>
                    <p class="contact_p">
                        <i class="fa fa-phone" style="font-size:32px;color:orangered;"></i><asp:Label ID="Label7" runat="server" Text="1800 858-4434"></asp:Label>
                    <br />
                        <i class="fa fa-fax" style="font-size:26px;color:orangered;"></i><asp:Label ID="Label9" runat="server" Text="1800 445-3676"></asp:Label>
                    </p>
                    <p class="contact_p">
                        <asp:Label ID="Label4" runat="server" Text="E-MAIL:" CssClass="titre_contact"></asp:Label>
                    </p>
                    <p class="contact_p">
                        <i class="fa fa-envelope" style="font-size:26px;color:orangered;"></i><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="https://www.tikolife@gmail.com" CssClass="email">tikolife@gmail.com</asp:HyperLink>
                    </p>
                    <p class="contact_p">
                        <asp:Label ID="Label10" runat="server" Text="Download information as. " ><asp:HyperLink ID="HyperLink2" runat="server"  NavigateUrl="#vcard" CssClass="vcard">vCard</asp:HyperLink></asp:Label>
                    </p>
                  </article>
                <article class="contact_article2">
                    <p class="contact_p">
                        <asp:Label ID="Label5" runat="server" Text="MISCELLANEOUS INFORMATION:" CssClass="titre_contact"></asp:Label>
                    </p>
                    <p class="contact_p">
                        <asp:Label ID="Label8" runat="server" Text="Email us with any questions or inquiries or use our contact data."></asp:Label>
                    </p>

                    <p class="contact_p">
                        <asp:TextBox ID="TextBox1" runat="server" CssClass="txt_box" AutoCompleteType="FirstName" Text="Name"></asp:TextBox>
                        <asp:TextBox ID="TextBox2" runat="server" CssClass="txt_box" AutoCompleteType="Email" Text="E-mail"></asp:TextBox>
                        <asp:TextBox ID="TextBox3" runat="server" CssClass="txt_box" AutoCompleteType="HomePhone" Text="Phone"></asp:TextBox>
                    </p>
                    <p class="contact_p"> 
                        <asp:TextBox ID="TextBox4" runat="server" CssClass="txt_box" AutoCompleteType="Notes" TextMode="MultiLine" Text="Message"></asp:TextBox>
                    </p>
                        <p class="contact_p">
                        <asp:Button ID="Button1" runat="server" Text="Send" CssClass="btn" OnClick="Button1_Click" />
                        <asp:Button ID="Button2" runat="server" Text="Clear" CssClass="btn" />
                    </p>
                </article>
            </section>

    </form>
  <div class="wide"></div>
</body>
</html>
