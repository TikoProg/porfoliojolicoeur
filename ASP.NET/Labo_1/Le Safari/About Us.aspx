﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="About Us.aspx.cs" Inherits="Le_Safari.About_Us" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="css/StyleSheet1.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
</head>
<body>

    <form class="about" runat="server">
        <header>
            <nav>               
                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn1" PostBackUrl="~/Home.aspx">Home<i class="fa fa-angle-down"></i></asp:LinkButton>
                <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn1" PostBackUrl="~/About Us.aspx">About</asp:LinkButton>
                <asp:LinkButton ID="LinkButton4" runat="server" CssClass="btn1">Pages<i class="fa fa-angle-down"></i></asp:LinkButton>
                <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn1" PostBackUrl="~/Gallery.aspx">Gallery</asp:LinkButton>
                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn1">Blog</asp:LinkButton>
                <asp:LinkButton ID="LinkButton6" runat="server" CssClass="btn1" PostBackUrl="~/Contact.aspx">Contact</asp:LinkButton>        
            </nav>
            <div id="div1">
                <asp:Image ID="Image1" runat="server" ImageAlign="Left" ImageUrl="~/images/globe.png" Height="45px" Width="50px" CssClass="div1" />  <asp:Label ID="Label1" runat="server" Text="Safari Adventure" ForeColor="White" CssClass="div1"></asp:Label> <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Right" ImageUrl="~/images/loop.png" Height="45px" Width="50px" CssClass="div1" />
            </div>

        </header>

            <section class="about_section1">

             <article class="about_article1">
                    <asp:Label ID="Label5" runat="server" Text="About us"  Font-Size="66px" Font-Bold="True"></asp:Label>
                    <hr/>
                    <asp:Label ID="Label6" runat="server" Text="We strive to be an exceptional team. We provide comprehensive care with a high level of customer service in a technically advanced environment." Font-Size="20px" Font-Bold="True"></asp:Label>
             </article>

             <asp:Button ID="Button4" runat="server" Text="More" CssClass="circle"/>

           </section>

        <section class="about_section2">
            <article>
                <asp:Label ID="Label7" runat="server" Text="Why Choose Us?" Font-Size="66px" Font-Bold="True" ForeColor="ghostwhite"></asp:Label>
            </article>
            
            <article class="homearticle">
                <header class="header2"> <asp:Label ID="Label2" runat="server" Text="Unusual Holidays" CssClass="titre"></asp:Label></header>
                <p class="texte">
                   Zoo Parc Safari, Un zoo à deux pas de Montréal · Animaux · Safari Aventure · Ferme des cinq continents · Terrasse Afrika · Plaine d'Afrique · Centre des guépards · Chimpanzés · Tunnel des lions · Cité des tigres · Sentier des daims · Bébé / Nouveaux arrivés · Attractions · Aquaparc Safari · Parc Nishati · Présentations ...
                </p>
                <footer> <asp:Button ID="Button2" runat="server" Text="More" BackColor="#FF3300" ForeColor="White" /></footer>
            </article>
            <article class="homearticle">
                <header class="header2"><asp:Label ID="Label4" runat="server" Text="Best Time to Visit" CssClass="titre"></asp:Label></header>
                <p class="texte">
                   Zoo Parc Safari, Un zoo à deux pas de Montréal · Animaux · Safari Aventure · Ferme des cinq continents · Terrasse Afrika · Plaine d'Afrique · Centre des guépards · Chimpanzés · Tunnel des lions · Cité des tigres · Sentier des daims · Bébé / Nouveaux arrivés · Attractions · Aquaparc Safari · Parc Nishati · Présentations ... 
                </p>
                <footer><asp:Button ID="Button1" runat="server" Text="More" BackColor="#FF3300" ForeColor="White" /></footer>
            </article>
            <article class="homearticle">
                <header class="header2"><asp:Label ID="Label3" runat="server" Text="Big Cat Safari" CssClass="titre"></asp:Label></header>
                <p class="texte">
                    Zoo Parc Safari, Un zoo à deux pas de Montréal · Animaux · Safari Aventure · Ferme des cinq continents · Terrasse Afrika · Plaine d'Afrique · Centre des guépards · Chimpanzés · Tunnel des lions · Cité des tigres · Sentier des daims · Bébé / Nouveaux arrivés · Attractions · Aquaparc Safari · Parc Nishati · Présentations ...   
                </p>
                <footer><asp:Button ID="Button3" runat="server" Text="More" BackColor="#FF3300" ForeColor="White" /></footer>
            </article>
          </section>

    </form>
  <div class="blury2"></div>
</body>
</html>
