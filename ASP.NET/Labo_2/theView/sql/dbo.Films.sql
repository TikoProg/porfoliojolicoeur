﻿CREATE TABLE [dbo].[films] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Titre]     NVARCHAR (50)  NULL,
    [Directeur] NVARCHAR (50)  NULL,
    [Date]      DATE           NULL,
    [Link]      NVARCHAR (MAX) NULL,
    [Image]     NVARCHAR (MAX) NULL,
    [Ensale]    BIT            NULL,
    [Categorie] INT            NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_films_Categorie] FOREIGN KEY ([Categorie]) REFERENCES [dbo].[Categorie] ([Id]) ON DELETE CASCADE
);

