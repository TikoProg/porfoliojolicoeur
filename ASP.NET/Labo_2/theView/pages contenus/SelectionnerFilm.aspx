﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages maitres/page_maitre.Master" AutoEventWireup="true" CodeBehind="SelectionnerFilm.aspx.cs" Inherits="theView.pages_contenus.SélectionnerFilm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid inserer">
        <div class="row">
            <div class="col-3">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" PageSize="3" AllowPaging="True">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="Titre" HeaderText="Titre" SortExpression="Titre" />
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <SortedAscendingCellStyle BackColor="#FEFCEB" />
                    <SortedAscendingHeaderStyle BackColor="#AF0101" />
                    <SortedDescendingCellStyle BackColor="#F6F0C0" />
                    <SortedDescendingHeaderStyle BackColor="#7E0000" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [Titre], [Id] FROM [films]"></asp:SqlDataSource>
            </div>
            <div class="col-9">
                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" PageSize="4">
                    <Columns>
                        <asp:BoundField DataField="Nom" HeaderText="Nom" SortExpression="Nom" />
                        <asp:BoundField DataField="Directeur" HeaderText="Directeur" SortExpression="Directeur" />
                        <asp:BoundField DataField="Date" HeaderText="Date de parution" SortExpression="Date" />
                        <asp:HyperLinkField DataNavigateUrlFields="Link" DataTextField="Link" HeaderText="Link" />
                        <asp:ImageField DataImageUrlField="Image" DataImageUrlFormatString="~/images/{0}" HeaderText="Image">
                        </asp:ImageField>
                        <asp:CheckBoxField DataField="Ensale" HeaderText="Ensale" SortExpression="Ensale" />
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <SortedAscendingCellStyle BackColor="#FEFCEB" />
                    <SortedAscendingHeaderStyle BackColor="#AF0101" />
                    <SortedDescendingCellStyle BackColor="#F6F0C0" />
                    <SortedDescendingHeaderStyle BackColor="#7E0000" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT films.Directeur, films.Date, films.Link, films.Image, films.Ensale, films.Categorie, Categorie.Nom FROM films INNER JOIN Categorie ON films.Categorie = Categorie.Id WHERE (films.Id = @Id)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GridView1" Name="Id" PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
        <div class="row">
            <h1 class="display-1">Meteo</h1>
            <div class="col-12">
                <asp:Label ID="Label1" runat="server" Text="Entez le nom d'un pays: " Font-Size="Larger" ForeColor="White"></asp:Label><asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><asp:Button CssClass="btn-primary" ID="Button1" runat="server" Text="Enter" OnClick="Button1_Click" />
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False">
                </asp:GridView>
            </div>
        </div>
    </div>

</asp:Content>
