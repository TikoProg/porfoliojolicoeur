﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using theView.net.webservicex.www;

namespace theView.pages_contenus
{
    public partial class SélectionnerFilm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string pays = TextBox1.Text;
            GlobalWeather service = new GlobalWeather();
            string Data = service.GetCitiesByCountry(pays);
            XmlReader xtr = new XmlTextReader(new System.IO.StringReader(Data));

            DataSet ds=new DataSet();
            ds.ReadXml(xtr);

            GridView3.DataSource = ds.Tables[0];

        }
    }
}