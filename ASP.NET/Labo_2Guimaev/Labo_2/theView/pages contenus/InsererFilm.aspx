﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages maitres/page_maitre.Master" AutoEventWireup="true" CodeBehind="InsererFilm.aspx.cs" Inherits="theView.pages_contenus.InsererFilm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid inserer">
        <div class="row">
            <h1>Upload to share with The View comunity!</h1>
        </div>
        <div class="row">
            <div class="col">
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [Titre], [Directeur], [Date], [Link], [Image], [Ensale], [Categorie] FROM [films]" InsertCommand="INSERT INTO films(Titre, Directeur, Date, Link, Image, Ensale, Categorie) VALUES (@Titre, @Directeur, @Date, @Link, @Image, @Ensale,(select Id from Categorie where nom= @Categorie))">
                    <InsertParameters>
                        <asp:ControlParameter ControlID="DetailsView1" Name="Titre" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DetailsView1" Name="Directeur" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DetailsView1" Name="Date" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DetailsView1" Name="Link" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DetailsView1" Name="Image" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DetailsView1" Name="Ensale" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DetailsView1" Name="Categorie" PropertyName="SelectedValue" />
                    </InsertParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Categorie]"></asp:SqlDataSource>

                <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataSourceID="SqlDataSource1" DefaultMode="Insert" Height="16px" Width="230px" OnPageIndexChanging="DetailsView1_PageIndexChanging">
                    <Fields>
                        <asp:BoundField DataField="Titre" HeaderText="Titre" SortExpression="Titre" />
                        <asp:BoundField DataField="Directeur" HeaderText="Directeur" SortExpression="Directeur" />
                        <asp:TemplateField HeaderText="Date" SortExpression="Date">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Date") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="Black" BorderStyle="Solid" CellSpacing="1" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="250px" NextPrevFormat="ShortMonth" Width="330px">
                                    <DayHeaderStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" Height="8pt" />
                                    <DayStyle BackColor="#CCCCCC" />
                                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                    <OtherMonthDayStyle ForeColor="#999999" />
                                    <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                                    <TitleStyle BackColor="#333399" BorderStyle="Solid" Font-Bold="True" Font-Size="12pt" ForeColor="White" Height="12pt" />
                                    <TodayDayStyle BackColor="#999999" ForeColor="White" />
                                </asp:Calendar>
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                            </ItemTemplate>
                            <ControlStyle BackColor="#CCCCCC" BorderColor="#006600" ForeColor="Black" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Link" HeaderText="Link" SortExpression="Link" />
                        <asp:TemplateField HeaderText="Image" SortExpression="Image">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Image") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                <asp:Button ID="ButUpload" runat="server" OnClick="ButUpload_Click" Text="Button" />
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Image") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CheckBoxField DataField="Ensale" HeaderText="Ensale" SortExpression="Ensale" />
                        <asp:TemplateField HeaderText="Categorie" SortExpression="Categorie">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Categorie") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource2" DataTextField="Nom" DataValueField="Id">
                                </asp:DropDownList>
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Categorie") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Button" ShowInsertButton="True" />
                    </Fields>
                </asp:DetailsView>
            &nbsp;&nbsp;&nbsp;
            </div>
        </div>
    </div>

</asp:Content>
