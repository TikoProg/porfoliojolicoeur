﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace theView.pages_contenus
{
    public partial class InsererFilm : System.Web.UI.Page
    {
         
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void DetailsView1_PageIndexChanging(object sender, DetailsViewPageEventArgs e)
        {

        }

        protected void ButUpload_Click(object sender, EventArgs e)
        {

            FileUpload fu1 = (FileUpload)FindControl(DetailsView1,"FileUpload1");
            fu1.SaveAs(Request.PhysicalApplicationPath + @"/images/" + fu1.FileName);
        }

        //control finder pour trouver le fileupload1 du detailsview
        private Control FindControl(Control parent, string id)
        {
            foreach (Control child in parent.Controls)
            {
                string childId = string.Empty;
                if (child.ID != null)
                {
                    childId = child.ID;
                }

                if (childId.ToLower() == id.ToLower())
                {
                    return child;
                }
                else
                {
                    if (child.HasControls())
                    {
                        Control response = FindControl(child, id);
                        if (response != null)
                            return response;
                    }
                }
            }

            return null;
        }
    }
}