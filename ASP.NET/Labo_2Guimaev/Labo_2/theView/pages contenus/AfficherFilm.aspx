﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages maitres/page_maitre.Master" AutoEventWireup="true" CodeBehind="AfficherFilm.aspx.cs" Inherits="theView.pages_contenus.AfficherFilm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid affiche">
        <div class="row ">
            <div class="col-md-5">
                <h3 class="display-6">Follow the links for free streaming....</h3>
                <p class="txt">The View</p>
            </div>
            <div class="col-md-7">
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id,Id1" DataSourceID="SqlDataSource1" PageSize="3" AutoGenerateEditButton="True">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                        <asp:ImageField DataImageUrlField="Image" DataImageUrlFormatString="~/images/{0}">
                        </asp:ImageField>
                        <asp:HyperLinkField DataNavigateUrlFields="Link" DataNavigateUrlFormatString="{0}" DataTextField="Titre" DataTextFormatString="{0}" HeaderText="Titre" SortExpression="Titre" />
                        <asp:TemplateField HeaderText="Categorie" SortExpression="Categorie">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlCat" DataTextField="Nom" DataValueField="Id" SelectedValue='<%# Bind("Categorie") %>'>
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Nom") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Directeur" HeaderText="Directeur" SortExpression="Directeur" />
                        <asp:BoundField DataField="Date" DataFormatString="{0: dddd dd MMMM yyyy }" HeaderText="Date" SortExpression="Date" />
                        <asp:CheckBoxField DataField="Ensale" HeaderText="Ensale" SortExpression="Ensale" />
                    </Columns>
                    <EmptyDataTemplate>
                        <a href="javascript:__doPostBack('GridView1','Sort$Id')">Id</a>
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Films] JOIN Categorie ON Films.Categorie=Categorie.Id" UpdateCommand="UPDATE Films SET Categorie = @Categorie WHERE ( Id = @Id)">
                    <UpdateParameters>
                        <asp:ControlParameter ControlID="GridView1" Name="Categorie" PropertyName="SelectedValue" />
                        <asp:Parameter Name="Id" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlCat" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Categorie]"></asp:SqlDataSource>
            </div>
        </div>
    </div>
</asp:Content>
