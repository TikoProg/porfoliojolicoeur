﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages maitres/pmTP3.Master" AutoEventWireup="true" CodeBehind="pageContenu3.aspx.cs" Inherits="exercice_1.pageContenu3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-sm-8 box">
            <h1>Inscription au serveur WEB</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 box">
            <asp:Label ID="Label1" runat="server" Text="Nom de l'élève:"></asp:Label>
        </div>
        <div class="col-sm-3 box">
            <input id="nom" type="text" runat="server" class="form-control" />
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 box">
            <asp:Label ID="Label2" runat="server" Text="Groupe:"></asp:Label>
        </div>
        <div class="col-sm-3 box">
            <input id="groupe" type="text" runat="server" class="form-control" />
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 box">
            <asp:Label ID="Label3" runat="server" Text="Programme inscrit:"></asp:Label>
        </div>
        <div class="col-sm-3 box">
            <input id="programme" type="text" runat="server" class="form-control" />
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 box">
            <asp:Label ID="Label4" runat="server" Text="mot de passe:"></asp:Label>
        </div>
        <div class="col-sm-3 box">
            <input id="pwd" type="password" runat="server" class="form-control" />
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7 box">
            <asp:Button ID="enter" runat="server" Text="Enter" OnClick="enter_Click" CssClass="btn btn-primary btn-lg" />
        </div>
    </div>
    <footer>
        <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
    </footer>
</asp:Content>
