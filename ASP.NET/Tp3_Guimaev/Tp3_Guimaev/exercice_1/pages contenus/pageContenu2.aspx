﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages maitres/pmTP3.Master" AutoEventWireup="true" CodeBehind="pageContenu2.aspx.cs" Inherits="exercice_1.pageContenu2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="row">
            <div class="col-sm-8 box">
                <h1>Stationnement</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 box">
                <asp:Label ID="Label1" runat="server" Text="Nom de l'élève:"  ></asp:Label>   
            </div>
            <div class="col-sm-3 box">
                <input id="nom" type="text" runat="server" class="form-control"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 box">
                <asp:Label ID="Label2" runat="server" Text="Marque de l'auto:"></asp:Label>   
            </div>
            <div class="col-sm-3 box">
                <input id="marque" type="text" runat="server" class="form-control"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 box">
                <asp:Label ID="Label3" runat="server" Text="Numéro de plaque:" ></asp:Label>
            </div>
            <div class="col-sm-3 box">
                <input id="plaque" type="text" runat="server" class="form-control" />
                <asp:CompareValidator ErrorMessage="Plaque non valide!" ControlToValidate="plaque" runat="server" ID="Validate_Plate" SetFocusOnError="True" ValueToCompare='@"^[A-Za-z][0-9][0-9][A-Za-z][A-Za-z][A-Za-z]$";' ForeColor="Red" Display="Dynamic" ValidationGroup="Validate" Font-Size="Small"></asp:CompareValidator>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-7 box">
                <asp:Button ID="enter" runat="server" Text="Enter"  OnClick="enter_Click" CssClass="btn btn-primary btn-lg" />
            </div>
        </div>
    <footer>
        <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
    </footer>
</asp:Content>
