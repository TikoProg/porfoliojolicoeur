﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="Le_Safari.Gallery" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="css/StyleSheet1.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <script src="scripts/jquery-3.3.1.min.js"></script>
    <script src="scripts/myscript.js"></script>
   
</head>
<body>

    <form class="gallery" runat="server">
        <header>
            <nav>               
                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn1" PostBackUrl="~/Home.aspx">Home<i class="fa fa-angle-down"></i></asp:LinkButton>
                <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn1" PostBackUrl="~/About Us.aspx">About</asp:LinkButton>
                <asp:LinkButton ID="LinkButton4" runat="server" CssClass="btn1">Pages<i class="fa fa-angle-down"></i></asp:LinkButton>
                <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn1" PostBackUrl="~/Gallery.aspx">Gallery</asp:LinkButton>
                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn1">Blog</asp:LinkButton>
                <asp:LinkButton ID="LinkButton6" runat="server" CssClass="btn1" PostBackUrl="~/Contact.aspx">Contact</asp:LinkButton>        
            </nav>
            <div id="div1">
                <asp:Image ID="Image1" runat="server" ImageAlign="Left" ImageUrl="~/images/globe.png" Height="45px" Width="50px" CssClass="div1" />  <asp:Label ID="Label1" runat="server" Text="Safari Adventure" ForeColor="White" CssClass="div1"></asp:Label> <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Right" ImageUrl="~/images/loop.png" Height="45px" Width="50px" CssClass="div1" />
            </div>

        </header>
        
            <section class="gallery_section1">

               <header class="titre">
                <asp:Label ID="Label2" runat="server" Text="Gallery" Font-Size="XX-Large"></asp:Label>
               </header>

                <article class="pic_box">
                <img src="images/zebra.jpg" class="gallery_pics" alt="Toto"/>
                <img src="images/tiger.jpg" class="gallery_pics" alt="Toto"/>
                <img src="images/parrot.jpg" class="gallery_pics" alt="Toto"/>
                <img src="images/monkey.jpg" class="gallery_pics" alt="Toto"/>
                <img src="images/leopard.jpg" class="gallery_pics" alt="Toto"/>
                <img src="images/hyena.jpg" class="gallery_pics" alt="Toto"/>
                <img src="images/deer.jpg" class="gallery_pics" alt="Toto"/>
                <img src="images/elephant.jpg" class="gallery_pics" alt="Toto"/>
                </article>

            </section>

    </form>
    <div id="myModal" class="modal">
         <span class="close">&times;</span>
        <img class="modal-content" id="img01" src="none"/>
        <div id="caption"></div>
    </div>
  <div class="wide"></div>
</body>
</html>
