﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Le_Safari.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="css/StyleSheet1.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <script src="scripts/jquery-3.3.1.min.js"></script>
    <script src="scripts/myscript.js"></script>
</head>
<body>

    <form class="home" runat="server">
        <header>
            <nav>               
                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn1" PostBackUrl="~/Home.aspx">Home<i class="fa fa-angle-down"></i></asp:LinkButton>
                <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn1" PostBackUrl="~/About Us.aspx">About</asp:LinkButton>
                <asp:LinkButton ID="LinkButton4" runat="server" CssClass="btn1">Pages<i class="fa fa-angle-down"></i></asp:LinkButton>
                <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn1" PostBackUrl="~/Gallery.aspx">Gallery</asp:LinkButton>
                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn1">Blog</asp:LinkButton>
                <asp:LinkButton ID="LinkButton6" runat="server" CssClass="btn1" PostBackUrl="~/Contact.aspx">Contact</asp:LinkButton>        
            </nav>
            <div id="div1">
                <asp:Image ID="Image1" runat="server" ImageAlign="Left" ImageUrl="~/images/globe.png" Height="45px" Width="50px" CssClass="div1" />  <asp:Label ID="Label1" runat="server" Text="Safari Adventure" ForeColor="White" CssClass="div1"></asp:Label> <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Right" ImageUrl="~/images/loop.png" Height="45px" Width="50px" CssClass="div1" />
            </div>

        </header>

            <section class="section1">

             <article class="bigpic">
                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/singe.jpg" BorderStyle="None" Width="100%"  Height="570px" />
                  <p class="bigpictxt">
                    <asp:Label ID="Label5" runat="server" Text="Explore Africa"  Font-Size="76px" Font-Bold="True"></asp:Label>
                    <br />
                    <asp:Label ID="Label6" runat="server" Text="During Amazing Safari Tours!" Font-Size="40px" Font-Bold="True"></asp:Label>
                  </p>
              </article>
            <div class="bigpicbottom">
                <div class="rectangle" id="rec1"></div>
                <div class="rectangle" id="rec2"></div>
                <div class="rectangle" id="rec3"></div>
             </div>    

            </section>

        <section class="section2">
            <article class="homearticle">
                <header class="header2"> <asp:Label ID="Label2" runat="server" Text="Unusual Holidays" CssClass="titre"></asp:Label></header>
                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/girafe.jpg" CssClass="imghome" />
                <h4 class="titre">Spend a week with us</h4>
                <p class="texte">
                   Zoo Parc Safari, Un zoo à deux pas de Montréal · Animaux · Safari Aventure · Ferme des cinq continents · Terrasse Afrika · Plaine d'Afrique · Centre des guépards · Chimpanzés · Tunnel des lions · Cité des tigres · Sentier des daims · Bébé / Nouveaux arrivés · Attractions · Aquaparc Safari · Parc Nishati · Présentations ...
                </p>
                <footer> <asp:Button ID="Button2" runat="server" Text="More" BackColor="#FF3300" ForeColor="White" /></footer>
            </article>
            <article class="homearticle">
                <header class="header2"><asp:Label ID="Label4" runat="server" Text="Best Time to Visit" CssClass="titre"></asp:Label></header>
                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/rhinos.jpg" CssClass="imghome" />
                <h4 class="titre">Plan your African Safari with us</h4>
                <p class="texte">
                   Zoo Parc Safari, Un zoo à deux pas de Montréal · Animaux · Safari Aventure · Ferme des cinq continents · Terrasse Afrika · Plaine d'Afrique · Centre des guépards · Chimpanzés · Tunnel des lions · Cité des tigres · Sentier des daims · Bébé / Nouveaux arrivés · Attractions · Aquaparc Safari · Parc Nishati · Présentations ... 
                </p>
                <footer><asp:Button ID="Button1" runat="server" Text="More" BackColor="#FF3300" ForeColor="White" /></footer>
            </article>
            <article class="homearticle">
                <header class="header2"><asp:Label ID="Label3" runat="server" Text="Big Cat Safari" CssClass="titre"></asp:Label></header>
                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/lion.jpg" CssClass="imghome" />
                <h4 class="titre">Explore Africa's unique Safari</h4>
                <p class="texte">
                    Zoo Parc Safari, Un zoo à deux pas de Montréal · Animaux · Safari Aventure · Ferme des cinq continents · Terrasse Afrika · Plaine d'Afrique · Centre des guépards · Chimpanzés · Tunnel des lions · Cité des tigres · Sentier des daims · Bébé / Nouveaux arrivés · Attractions · Aquaparc Safari · Parc Nishati · Présentations ...   
                </p>
                <footer><asp:Button ID="Button3" runat="server" Text="More" BackColor="#FF3300" ForeColor="White" /></footer>
            </article>
          </section>

    </form>
  <div class="blury"></div>
</body>
</html>
