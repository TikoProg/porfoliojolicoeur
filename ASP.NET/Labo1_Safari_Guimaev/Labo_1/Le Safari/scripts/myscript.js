﻿$(document).ready(function () {

    
    var pic1 = $("#Image5").attr('src');
    var pic2 = $("#Image2").attr('src');
    var pic3 = $("#Image3").attr('src');
    var pic4 = $("#Image4").attr('src');
    var cnt = 0;

    setInterval(function () {
        $("#Image5").fadeIn(500, function () {
            
            cnt++;
             switch (cnt)
            {
                case 1:
                    $("#Image5").attr('src', pic2);
                    $(".rectangle").css("background-color", "whitesmoke");
                    $("#rec1").css("background-color", "orangered");
                     break;
                case 2:
                    $("#Image5").attr('src', pic3);
                    $(".rectangle").css("background-color", "whitesmoke");
                    $("#rec2").css("background-color", "orangered");
                    break;
                case 3:
                    $("#Image5").attr('src', pic4);
                    $(".rectangle").css("background-color", "whitesmoke");
                    $("#rec3").css("background-color", "orangered");
                    break;
                case 4:
                    $("#Image5").attr('src', 'images/singe.jpg');
                    $(".rectangle").css("background-color", "whitesmoke");
                    cnt = 0;
                    break;
                default:
                    break;
            }


        });

    }, 5000);


    $("#rec1").click(function () {
        $("#Image5").attr('src', pic2);
        $(".rectangle").css("background-color", "whitesmoke");
        $("#rec1").css("background-color", "orangered");
    });
    $("#rec2").click(function () {
        $("#Image5").attr('src', pic3);
        $(".rectangle").css("background-color", "whitesmoke");
        $("#rec2").css("background-color", "orangered");
    });
    $("#rec3").click(function () {
        $("#Image5").attr('src', pic4);
        $(".rectangle").css("background-color", "whitesmoke");
        $("#rec3").css("background-color", "orangered");
    });


    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");

    $(".gallery_pics").click(function ()
    {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;

        setTimeout(function ()
        {
            modal.style.display = "none";
        }, 3000);
 
    });
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    };

});