﻿$(document).ready(function ()
{

    var name = $('#TextBox1').val();
    var email = $('#TextBox2').val();
    var phone = $('#TextBox3').val();
    var message = $('#TextBox4').val();


    //affiche les entrers dans le txt area
    $('#Button1').click(function ()
    {
        if (!ValidateName(name) || name === "Name" || name === "")
        {
            $('#TextBox4').val("***Le nom n'est pas valide!");
        }
        else if (!ValidateEmail(email) || email === "E-mail" || name === "")
        {
            $('#TextBox4').val("***L'e-mail n'est pas valide!");
        }
        else if (!ValidatePhone(phone) || phone === "Phone" || name === "")
        {
            $('#TextBox4').val("***Le numero de telephone n'est pas valide");
        }
        else
        {
            $('#TextBox4').val(name+" "+email+" "+phone+"\n");
        }

    });
    //reset form
    $('#Button2').click(function () {

        $(this).closest('form').find("input[type=text], textarea").val("");

    });


    function ValidateName(name) {
        var expr1 = /^([a-zA-Z]{3,16})$/;
        return expr1.test(name);
    }
    function ValidateEmail(email) {
        var expr2 = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr2.test(email);
    }
    function ValidatePhone(phone) {
        var expr3 = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
        return expr3.test(phone);
    }


});