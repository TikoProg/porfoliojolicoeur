#include<iostream>
#include<string>
using namespace std;

//mes prototypes 
void can(double);
void us(double x);

int main()
{
	cout.setf(ios::fixed);
	cout.precision(2);
	setlocale(LC_ALL, "");

	//declaraton et initialisation de mes variable
	double nb=0;
	string pays = "";
	string rep = "";

	do
	{
		cout << "De quel pays provenez-vous? Canada(c) ou Etats-Unis(e): ";
		cin >> pays;
		cout << endl;

		cout << "Entrez le montant de votre achat: ";
		cin >> nb;
		cout << endl;

		if (nb > 100|| nb<0)
		{
			cout << "Le montant doit etre entre 0 $ et 100 $ recommencer";
			cout << "Un autre calcul? (o/n): ";
			cin >> rep;
			cout << endl;
		}

		//struc de decision pour la devise et les taxes
		if (pays == "c")
		{
			can(nb);

		}
		else
		{
			us(nb);
		}
		cout << endl;

		cout << "Un autre calcul? (o/n): ";
		cin >> rep;
		cout << endl;

	} while (rep == "o");

	system("pause");
}

void can(double x)
{//function calcul tax canada
	
	const double tps = 5.0 / 100 * x;
	const double tvq = 9.975 / 100 * x;

	cout << "Votre achat vaut: " << x + tps + tvq << "$" << "\n";

}

void us(double x)
{//function calcul tax etats-unis

	const double etat = 7.5 / 100 * x;
	const double fed = 4.5 / 100 * x;
	
	cout << "Votre achat vaut: " << x + etat + fed << "$" << "\n";

}