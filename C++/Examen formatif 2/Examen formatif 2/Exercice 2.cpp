#include <iostream>

using namespace std;



int main()
{
	//definie les nombres apres la virgule
	cout.setf(ios::fixed);
	cout.precision(2);

	setlocale(LC_ALL, "");

	//declaation de la variable

	double hrs = 00.00;
	double sum = 0.00;
	char perm;

	cout << "Indiquer si vous �tes un employ� permanant ou non: " <<"(o/n)" << endl;
	cin >> perm;

	cout << endl;

	cout << "Entrez le nombre d'heures travaill�es durant la semaine: " << endl;

	cin >> hrs;


	if (perm == 'o')
	{
		if (hrs < 35)
		{
			sum = hrs*18.00;
			cout << "Votre ch�que sera de: " << sum << " $" << endl;
		}
		else if (hrs > 35 || hrs < 40)
		{
			sum = hrs*23.75;
			cout << "Votre ch�que sera de: " << sum << " $" << endl;
		}
		else
		{
			sum = hrs*26.00;
			cout << "Votre ch�que sera de: " << sum << " $" << endl;
		}
	} 

	else if (perm == 'n')
	{
		sum = hrs*18.00;
		cout << "Votre ch�que sera de: " << sum << " $" << endl;
	}


	cout << endl;

	system("pause");

	return 0;
}