#include <iostream>
#include <iomanip>
#include <string>
#include <ctime>
#include <cmath>
#include <time.h> 
#include <stdlib.h>

using namespace std;

//Mes prototypes
void AjouterFiguresAuDeck(string[]);
void AjouterValeursAuDeck(int[]);
void TourDeTable(int[], int[], int[], int[]);
void WinOrNot(int[], int[], int[], int[], int[]);
void AfficheMain(int[], int[], int[], int[], string[]);
void VerifierHand(int Player1hand[], int Player2hand[], int Player3hand[], int Vous[]);
int SommeDeLaMain(int[]);



int main()
{
	//declaration et initialisation de mes tabs
	const string joueurs[4] = { "Player1","Player2","Player3","Vous" };
	int Player1hand[10] = { 0 }, Player2hand[10] = { 0 }, Player3hand[10] = { 0 }, Vous[10] = { 0 };


	//initialisation de mes Decks par appel de fontion pour les valeurs et les figures
	int DeckValeurs[52];
	AjouterValeursAuDeck(DeckValeurs);
	string DeckFigures[52];
	AjouterFiguresAuDeck(DeckFigures);


	
	string REJOUER = "";

	do
	{


		//affiche un message de bienvenue
		cout << "\t\t\t\t\tBienvenue au jeu de Grey Jack" << "\n\n" << endl;

		//J'affiche les joueurs
		cout << setw(20) << joueurs[0] << setw(20) << joueurs[1] << setw(20) << joueurs[2] << setw(20) << joueurs[3] << endl;


		//appel de la fonction qui distribut les cartes 
		TourDeTable(Player1hand, Player2hand, Player3hand, Vous); //ajout 1
		

		//j'affiche la premiere main
		cout << setw(20) << "" << setw(20) << "" << setw(20) << "" << setw(20) << DeckFigures[Vous[0]] << endl;

		//appel de la fonction qui distribut les cartes
		TourDeTable(Player1hand, Player2hand, Player3hand, Vous); //ajout 2

		//j'affiche la deuxieme main
		cout << setw(20) << DeckFigures[Player1hand[1]] << setw(20) << DeckFigures[Player2hand[1]] << setw(20) << DeckFigures[Player3hand[1]] << setw(20) << DeckFigures[Vous[1]] << endl;

		// fonction qui verifie verifi les mains
		VerifierHand(Player1hand,Player2hand,Player3hand,Vous);

		string ENCORE = "";

		cout << setw(100) << "Encore (o/n)";
		cin >> ENCORE;
		cout << endl;
			
		if (ENCORE == "o")
		{
			do
			{
				//appel de la fonction qui distribut les cartes
				TourDeTable(Player1hand, Player2hand, Player3hand, Vous);

				//fontion affiahge des cartes
				AfficheMain(Player1hand, Player2hand, Player3hand, Vous, DeckFigures);

				//verifie verifi les mains
				VerifierHand(Player1hand, Player2hand, Player3hand, Vous);

				cout << setw(100) << "Encore (o/n)";
				cin >> ENCORE;
				cout << endl;

			} while (ENCORE == "o");

		} 
		
			//fonction qui affiche le resultat final
			WinOrNot(Player1hand, Player2hand, Player3hand, Vous, DeckValeurs);

			cout << endl;
			cout << "************************************* Un autre jeux ? *****************************************" << " (o / n) ";
			cin >> REJOUER;
			cout << endl;
		

	} while (REJOUER == "o");

	system("pause");
	return 0;
}




//mes fonction///////////////////////////////////////////////

void AjouterFiguresAuDeck(string deck[])
{
	string couleurs[4] = { "T","K","C","P" };
	string figures[13] = { "2","3","4","5","6","7","8","9","10","V","D","R","A" };
	

	//Je remplis un nouveau tab value pour chaque figures de mon deck

	//trefle
	for (int j = 0; j < 13; j++)
	{
			
		deck[j] = figures[j] + couleurs[0];
	}

	//kareau
	int x = 0;
	for (int j = 13; j < 26; j++)
	{
		
		deck[j] = figures[x] + couleurs[1];
		x++;
	}

	//coeur
	x = 0;
	for (int j = 26; j < 39; j++)
	{
		
		deck[j] = figures[x] + couleurs[2];
		x++;
	}

	//pique
	x = 0;
	for (int j = 39; j < 52; j++)
	{
	
		deck[j] = figures[x] + couleurs[3];
		x++;
	}
	
}

void AjouterValeursAuDeck(int valeurs[])
{
	int value[] = { 2,3,4,5,6,7,8,9,10,10,10,10,11 };
	


	//Je remplis un nouveau tab value pour chaque figures de mon dec

	//trefle
	for (int j = 0; j < 13; j++)
	{
		valeurs[j] = value[j];			
		
	}

	//kareau
	int x = 0;
	for (int j = 13; j < 26; j++)
	{
		valeurs[j] = value[x];
		
		x++;
	}

	//coeur
	x = 0;
	for (int j = 26; j < 39; j++)
	{
		valeurs[j] = value[x];
		
		x++;
	}

	//pique
	x = 0;
	for (int j = 39; j < 52; j++)
	{
		valeurs[j] = value[x];
		
		x++;
	}

}

void TourDeTable(int Player1hand[], int Player2hand[], int Player3hand[], int Vous[])
{
	//je brasse les cartes
	srand(time(NULL));

	int i = 0;

	for (i = 0; i < 10; i++)
	{
		

		//je remplie les mains en m assurant de ne pas partager les memes cartes
		// j'ai parametré les joueurs 1, 2 et 3 
		if ((Player1hand[i] == 0 && Player1hand[i] < 18) || (Player2hand[i] == 0 && Player2hand[i] < 16) || (Player3hand[i] == 0 && Player3hand[i] < 17) || Vous[i] == 0)
		{


			Player1hand[i] = rand() % 52;

			Player2hand[i] = rand() % 52;

			Player3hand[i] = rand() % 52;

			Vous[i] = rand() % 52;

			break; //Pour arreter la boucle
		}

	}

}


void VerifierHand(int Player1hand[], int Player2hand[], int Player3hand[], int Vous[])
{

	//delaration et initialisation de variable pour les valeurs de chaque mains
	int s1 = SommeDeLaMain(Player1hand);
	int s2 = SommeDeLaMain(Player2hand);
	int s3 = SommeDeLaMain(Player3hand);
	int s4 = SommeDeLaMain(Vous);

	//Verifie la somme des mains

	if (s1 > 21)//Si y'a 21 et plus, <OUT> s'affiche pour le perdant
	{
		cout << setw(20) << "OUT" << setw(20) << "" << setw(20) << "" << setw(20) << "" << endl;

	}
	if (s2 > 21)
	{
		cout << setw(20) << "" << setw(20) << "OUT" << setw(20) << "" << setw(20) << "" << endl;

	}
	if (s3 > 21)
	{
		cout << setw(20) << "" << setw(20) << "" << setw(20) << "OUT" << setw(20) << "" << endl;

	}
	if (s4 > 21)
	{
		cout << setw(20) << "" << setw(20) << "" << setw(20) << "" << setw(20) << "OUT" << endl;
	}


}

int SommeDeLaMain(int mains[])
{
	int DeckValeurs[52];
	//initialisation de mes Decks par appel de fontion pour les valeurs et les figures
	AjouterValeursAuDeck(DeckValeurs);

	int i = 0, somme = 0;

		for (i = 0; i < 10; i++)
		{
			if (mains[i] != 0) //ne prendra pas en charge les 0
			{
				somme += DeckValeurs[mains[i]];
			}
			
		}
		
	return somme;
}

void WinOrNot(int Player1hand[], int Player2hand[], int Player3hand[], int Vous[], int DeckValeurs[])
{
	//declaration et initialisation de mes Decks 
	const string joueurs[4] = { "Player1","Player2","Player3","Vous" };
	string DeckFigures[52];
	int argent[] = { 100,100,100,100 };

	//initialisation de mes Decks par appel de fontion
	AjouterFiguresAuDeck(DeckFigures);

	//delaration et initialisation de variable pour les valeurs de chaque mains
	int s1 = SommeDeLaMain(Player1hand);
	int s2 = SommeDeLaMain(Player2hand);
	int s3 = SommeDeLaMain(Player3hand);
	int s4 = SommeDeLaMain(Vous);


	//declare une variable pour trouver le plus grand nombre
	int nb = 0;

	if (s1 > nb)
	{
		nb = s1;
	}
	else if (s2 > nb)
	{
		nb = s2;
	}
	else if (s3 > nb)
	{
		nb = s3;
	}
	else if (s4 > nb)
	{
		nb = s4;
	}


	//Si y'a GrayJack, <Win!> s'affiche pour le gagnant 
	//+10$ pour le gagnant et -10$ pour les autres
	if (s1 == 21 || s1 > nb && s1 < 21)
	{
		cout << setw(20) << "WIN!" << setw(20) << "" << setw(20) << "" << setw(20) << "" << endl;
	
		argent[0] += 30;
		argent[1] -= 10;
		argent[2] -= 10;
		argent[3] -= 10;

	}
	else if (s2 == 21 || s2 > nb && s2 < 21)
	{
		cout << setw(20) << "" << setw(20) << "WIN!" << setw(20) << "" << setw(20) << "" << endl;
	
		argent[1] += 30;
		argent[2] -= 10;
		argent[3] -= 10;
		argent[0] -= 10;
	}
	else if (s3 == 21 || s3 > nb && s3 < 21)
	{
		cout << setw(20) << "" << setw(20) << "" << setw(20) << "WIN!" << setw(20) << "" << endl;
	
		argent[2] += 30;
		argent[3] -= 10;
		argent[0] -= 10;
		argent[1] -= 10;
	}
	else if (s4 == 21 || s4 > nb && s4 < 21)
	{
		cout << setw(20) << "" << setw(20) << "" << setw(20) << "" << setw(20) << "WIN!" << endl;

		argent[3] += 30;
		argent[0] -= 10;
		argent[1] -= 10;
		argent[2] -= 10;
	}
	else
	{
		cout << setw(20) << "" << setw(20) << "" << setw(20) << "" << setw(20) << "" << setw(20) << "DRAW" << endl;
	}



	cout << endl;

	//affichage des resultas final
	cout << "Argent restant " << setw(5) << argent[0] << setw(20) << argent[1] << setw(20) << argent[2] << setw(20) << argent[3] << "\n\n";

	cout << setw(40) << "La valeur de la main de " << joueurs[0] << " dont la care cachee est: " << DeckFigures[Player1hand[0]] << " vaut " << s1 << endl;
	cout << setw(40) << "La valeur de la main de " << joueurs[1] << " dont la care cachee est: " << DeckFigures[Player2hand[0]] << " vaut " << s2 << endl;
	cout << setw(40) << "La valeur de la main de " << joueurs[2] << " dont la care cachee est: " << DeckFigures[Player3hand[0]] << " vaut " << s3 << endl;
	cout << setw(60) << "La valeur de votre main est " << s4 << endl;



}

void AfficheMain(int Player1hand[], int Player2hand[], int Player3hand[], int Vous[], string DeckFigures[])
{
	int i = 2; //affiche les mains a partire de la 3em distribution
	
	cout << setw(20) << DeckFigures[Player1hand[i]] << setw(20) << DeckFigures[Player2hand[i]] << setw(20) << DeckFigures[Player3hand[i]] << setw(20) << DeckFigures[Vous[i]] << endl;
	
	i++;
	
}
