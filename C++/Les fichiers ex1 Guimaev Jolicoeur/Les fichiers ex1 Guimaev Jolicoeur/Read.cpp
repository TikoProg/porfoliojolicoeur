#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

int main()
{
	// cr�ation du fichier
	ifstream fichierMescontacts("mescontacts.dat", ios::in);

	// message si la cr�ation du fichier a �chou�e
	if (!fichierMescontacts)
	{
		cerr << "Ouverture de fichier impossible";
		exit(1);
	}

	// d�claration des variables
	char email[50];
	char nom[20];
	int age;

	// on entre les valeurs dans le fichier
	cout << setiosflags(ios::left) << setw(20) << "Nom" << setw(20) << "E-mail" << setw(0) << "Age\n\n";
	while (fichierMescontacts >> nom >> email >> age)
	{
		cout << setiosflags(ios::left) << setw(20) << nom << setw(20) << email<< setw(0) << age << endl;
	}

	fichierMescontacts.close();


	system("pause");
	return 0;
}