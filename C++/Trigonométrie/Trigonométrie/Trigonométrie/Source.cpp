#include<iostream>

using namespace std;
int main() 
{
	cout.setf(ios::fixed);
	cout.precision(6);
	setlocale(LC_ALL, "");

	double deg;
	const float pi = 3.14;

	cout << "Entrez le degr� de l'angle d�sir�: " << "\n";
	cin >> deg;

	double rad = deg*pi;
	cout << "La valeur de " << deg << " en radian est: " << rad << "\n";

	double sinus = sin(deg);
	cout << "Son sinus est: " << sinus << "\n";

	double cosinus = cos(deg);
	cout << "Son cosinus est: " << cosinus << "\n";

	double tangente = tan(deg);
	cout << "Sa tangente est: " << tangente << "\n";
	
	system("pause");
}